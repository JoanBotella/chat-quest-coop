# Chat Quest COOP

An engine written in C for Interactive Fiction games, or, in castillian, Aventuras Conversacionales. Uses an Object Oriented Programming aproach.

## License

GNU GPL v3 or later <https://www.gnu.org/licenses/gpl-3.0.en.html>

## Dependencies

- GLib 2.0

## Author

Joan Botella Vinaches <https://joanbotella.com>
