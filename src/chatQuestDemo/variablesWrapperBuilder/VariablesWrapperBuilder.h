#ifndef CHATQUEST_VARIABLESWRAPPERBUILDER_HEADER
#define CHATQUEST_VARIABLESWRAPPERBUILDER_HEADER

#include "chatQuestDemo/variablesWrapperBuilder/VariablesWrapperBuilderItf.h"

struct VariablesWrapperBuilderSta;

struct VariablesWrapperBuilder
{
	struct VariablesWrapperBuilderSta *sta;
	struct VariablesWrapperBuilderItf *VariablesWrapperBuilderItf;
};

struct VariablesWrapperBuilder *VariablesWrapperBuilder_new();

#endif
