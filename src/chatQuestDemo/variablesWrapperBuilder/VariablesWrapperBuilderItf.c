#include "chatQuest/globals/globals.h"
#include "chatQuestDemo/variablesWrapperBuilder/VariablesWrapperBuilderItf.h"
#include "chatQuestDemo/variablesWrapper/VariablesWrapperItf.h"
#include "chatQuest/stringToStringMap/StringToStringMapItf.h"

struct VariablesWrapperBuilderItf *VariablesWrapperBuilderItf_new(
	void *imp,
	void (*destroy)(struct VariablesWrapperBuilderItf *itf),

	void (*build)(struct VariablesWrapperBuilderItf *itf, struct StringToStringMapItf *variables),
	bool (*hasBuilt)(struct VariablesWrapperBuilderItf *itf),
	struct VariablesWrapperItf *(*ownBuiltAfterHas)(struct VariablesWrapperBuilderItf *itf)
)
{
	struct VariablesWrapperBuilderItf *itf = chatQuest_malloc(sizeof(struct VariablesWrapperBuilderItf));
	*itf = (struct VariablesWrapperBuilderItf) {
		.imp = imp,
		.destroy = destroy,

		.build = build,
		.hasBuilt = hasBuilt,
		.ownBuiltAfterHas = ownBuiltAfterHas
	};
	return itf;
}

void VariablesWrapperBuilderItf_destroy(struct VariablesWrapperBuilderItf *itf)
{
	chatQuest_free(itf);
}