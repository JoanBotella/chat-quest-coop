#ifndef CHATQUEST_VARIABLESWRAPPERBUILDERITF_HEADER
#define CHATQUEST_VARIABLESWRAPPERBUILDERITF_HEADER

#include "chatQuestDemo/variablesWrapper/VariablesWrapperItf.h"
#include "chatQuest/stringToStringMap/StringToStringMapItf.h"

struct VariablesWrapperBuilderItf
{
	void *imp;
	void (*destroy)(struct VariablesWrapperBuilderItf *itf);

	void (*build)(struct VariablesWrapperBuilderItf *itf, struct StringToStringMapItf *variables);
	bool (*hasBuilt)(struct VariablesWrapperBuilderItf *itf);
	struct VariablesWrapperItf *(*ownBuiltAfterHas)(struct VariablesWrapperBuilderItf *itf);
};

struct VariablesWrapperBuilderItf *VariablesWrapperBuilderItf_new(
	void *imp,
	void (*destroy)(struct VariablesWrapperBuilderItf *itf),

	void (*build)(struct VariablesWrapperBuilderItf *itf, struct StringToStringMapItf *variables),
	bool (*hasBuilt)(struct VariablesWrapperBuilderItf *itf),
	struct VariablesWrapperItf *(*ownBuiltAfterHas)(struct VariablesWrapperBuilderItf *itf)
);

void VariablesWrapperBuilderItf_destroy(struct VariablesWrapperBuilderItf *itf);

#endif