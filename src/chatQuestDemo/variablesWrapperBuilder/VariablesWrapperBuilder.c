#include "chatQuest/globals/globals.h"
#include "chatQuestDemo/variablesWrapperBuilder/VariablesWrapperBuilder.h"
#include "chatQuestDemo/variablesWrapperBuilder/VariablesWrapperBuilderItf.h"
#include "chatQuestDemo/variablesWrapper/VariablesWrapper.h"
#include "chatQuestDemo/variablesWrapper/VariablesWrapperItf.h"
#include "chatQuest/stringToStringMap/StringToStringMapItf.h"

struct VariablesWrapperBuilderSta
{
	struct VariablesWrapperItf *built;
};

static struct VariablesWrapperBuilderSta *VariablesWrapperBuilderSta_new()
{
	struct VariablesWrapperBuilderSta *sta = chatQuest_malloc(sizeof(struct VariablesWrapperBuilderSta));
	*sta = (struct VariablesWrapperBuilderSta) {
		.built = NULL
	};
	return sta;
}

static void VariablesWrapperBuilderSta_destroy(struct VariablesWrapperBuilderSta *sta)
{
	if (sta->built != NULL)
	{
		sta->built->destroy(sta->built);
	}
	chatQuest_free(sta);
}

static void VariablesWrapperBuilder_destroy(struct VariablesWrapperBuilderItf *itf)
{
	struct VariablesWrapperBuilder *imp = (struct VariablesWrapperBuilder *)itf->imp;

	VariablesWrapperBuilderSta_destroy(imp->sta);
	VariablesWrapperBuilderItf_destroy(imp->VariablesWrapperBuilderItf);
	chatQuest_free(imp);
}

static void VariablesWrapperBuilder_build(struct VariablesWrapperBuilderItf *itf, struct StringToStringMapItf *variables)
{
	struct VariablesWrapperBuilderSta *sta = ((struct VariablesWrapperBuilder *)itf->imp)->sta;
	sta->built = VariablesWrapper_new(variables)->VariablesWrapperItf;
}

static bool VariablesWrapperBuilder_hasBuilt(struct VariablesWrapperBuilderItf *itf)
{
	struct VariablesWrapperBuilderSta *sta = ((struct VariablesWrapperBuilder *)itf->imp)->sta;
	return sta->built != NULL;
}

static struct VariablesWrapperItf *VariablesWrapperBuilder_ownBuiltAfterHas(struct VariablesWrapperBuilderItf *itf)
{
	struct VariablesWrapperBuilderSta *sta = ((struct VariablesWrapperBuilder *)itf->imp)->sta;
	struct VariablesWrapperItf *r = sta->built;
	sta->built = NULL;
	return r;
}

struct VariablesWrapperBuilder *VariablesWrapperBuilder_new()
{
	struct VariablesWrapperBuilder *imp = chatQuest_malloc(sizeof(struct VariablesWrapperBuilder));
	*imp = (struct VariablesWrapperBuilder) {
		.sta = VariablesWrapperBuilderSta_new(
		),
		.VariablesWrapperBuilderItf = VariablesWrapperBuilderItf_new(
			imp,
			&VariablesWrapperBuilder_destroy,

			&VariablesWrapperBuilder_build,
			&VariablesWrapperBuilder_hasBuilt,
			&VariablesWrapperBuilder_ownBuiltAfterHas
		)
	};
	return imp;
}
