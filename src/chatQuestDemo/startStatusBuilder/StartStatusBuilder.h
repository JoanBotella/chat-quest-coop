#ifndef CHATQUESTDEMO_STARTSTATUSBUILDER_HEADER
#define CHATQUESTDEMO_STARTSTATUSBUILDER_HEADER

#include "chatQuest/startStatusBuilder/StartStatusBuilderItf.h"

struct StartStatusBuilderSta;

struct StartStatusBuilder
{
	struct StartStatusBuilderSta *sta;
	struct StartStatusBuilderItf *StartStatusBuilderItf;
};

struct StartStatusBuilder *StartStatusBuilder_new();

#endif
