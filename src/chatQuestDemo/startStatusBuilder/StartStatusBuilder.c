#include "chatQuest/globals/globals.h"
#include "chatQuestDemo/startStatusBuilder/StartStatusBuilder.h"
#include "chatQuest/startStatusBuilder/StartStatusBuilderItf.h"
#include "chatQuest/status/Status.h"
#include "chatQuest/status/StatusItf.h"
#include "chatQuestDemo/node/bedroom/BedroomNode.h"
#include "chatQuest/stringArray/StringArray.h"
#include "chatQuest/stringArray/StringArrayItf.h"
#include "chatQuest/stringToStringMap/StringToStringMap.h"

struct StartStatusBuilderSta
{
};

static struct StartStatusBuilderSta *StartStatusBuilderSta_new()
{
	struct StartStatusBuilderSta *sta = chatQuest_malloc(sizeof(struct StartStatusBuilderSta));
	*sta = (struct StartStatusBuilderSta) {
	};
	return sta;
}

static void StartStatusBuilderSta_destroy(struct StartStatusBuilderSta *sta)
{
	chatQuest_free(sta);
}

static void StartStatusBuilder_destroy(struct StartStatusBuilderItf *itf)
{
	struct StartStatusBuilder *imp = (struct StartStatusBuilder *)itf->imp;

	StartStatusBuilderSta_destroy(imp->sta);
	StartStatusBuilderItf_destroy(imp->StartStatusBuilderItf);
	chatQuest_free(imp);
}

	static struct StringArrayItf *StartStatusBuilder_buildNodeIds(struct StartStatusBuilder *imp)
	{
		struct StringArrayItf *nodeIds = StringArray_new()->StringArrayItf;
		nodeIds->append(nodeIds, CHATQUESTDEMO_BEDROOMNODE_ID);
		return nodeIds;
	}

	static struct StringToStringMapItf *StartStatusBuilder_buildVariables(struct StartStatusBuilder *imp)
	{
		struct StringToStringMapItf *variables = StringToStringMap_new()->StringToStringMapItf;
		return variables;
	}

static struct StatusItf *StartStatusBuilder_build(struct StartStatusBuilderItf *itf)
{
	struct StartStatusBuilder *imp = (struct StartStatusBuilder *)itf->imp;

	return Status_new(
		StartStatusBuilder_buildNodeIds(imp),
		StartStatusBuilder_buildVariables(imp)
	)->StatusItf;
}

struct StartStatusBuilder *StartStatusBuilder_new()
{
	struct StartStatusBuilder *imp = chatQuest_malloc(sizeof(struct StartStatusBuilder));
	*imp = (struct StartStatusBuilder) {
		.sta = StartStatusBuilderSta_new(),
		.StartStatusBuilderItf = StartStatusBuilderItf_new(
			imp,
			&StartStatusBuilder_destroy,

			&StartStatusBuilder_build
		)
	};
	return imp;
}