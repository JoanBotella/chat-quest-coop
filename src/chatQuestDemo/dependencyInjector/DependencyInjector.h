#ifndef CHATQUESTDEMO_DEPENDENCYINJECTOR_HEADER
#define CHATQUESTDEMO_DEPENDENCYINJECTOR_HEADER

#include "chatQuestDemo/dependencyInjector/DependencyInjectorItf.h"

struct DependencyInjectorSta;

struct DependencyInjector
{
	struct DependencyInjectorSta *sta;
	struct DependencyInjectorItf *DependencyInjectorItf;
};

struct DependencyInjector *DependencyInjector_new();

#endif
