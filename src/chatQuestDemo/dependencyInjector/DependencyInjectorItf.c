#include "chatQuest/globals/globals.h"
#include "chatQuestDemo/dependencyInjector/DependencyInjectorItf.h"
#include "chatQuest/app/AppItf.h"
#include "chatQuest/requestBuilder/RequestBuilderItf.h"
#include "chatQuest/stateMachine/StateMachineItf.h"
#include "chatQuest/responseDispatcher/ResponseDispatcherItf.h"
#include "chatQuest/prompter/PrompterItf.h"
#include "chatQuest/queryStringNormalizer/QueryStringNormalizerItf.h"
#include "chatQuest/queryBuilder/QueryBuilderItf.h"
#include "chatQuest/startStatusBuilder/StartStatusBuilderItf.h"
#include "chatQuest/nodeBuilder/NodeBuilderItf.h"
#include "chatQuest/errorDispatcher/ErrorDispatcherItf.h"
#include "chatQuestDemo/variablesWrapperBuilder/VariablesWrapperBuilderItf.h"

#include "chatQuest/node/NodeItf.h"

struct DependencyInjectorItf *DependencyInjectorItf_new(
	void *imp,
	void (*destroy)(struct DependencyInjectorItf *itf),

	struct AppItf *(*buildApp)(struct DependencyInjectorItf *itf),
	struct RequestBuilderItf *(*buildRequestBuilder)(struct DependencyInjectorItf *itf),
	struct StateMachineItf *(*buildStateMachine)(struct DependencyInjectorItf *itf),
	struct ResponseDispatcherItf *(*buildResponseDispatcher)(struct DependencyInjectorItf *itf),
	struct PrompterItf *(*buildPrompter)(struct DependencyInjectorItf *itf),
	struct QueryStringNormalizerItf *(*buildQueryStringNormalizer)(struct DependencyInjectorItf *itf),
	struct QueryBuilderItf *(*buildQueryBuilder)(struct DependencyInjectorItf *itf),
	struct StartStatusBuilderItf *(*buildStartStatusBuilder)(struct DependencyInjectorItf *itf),
	struct NodeBuilderItf *(*buildNodeBuilder)(struct DependencyInjectorItf *itf),
	struct ErrorDispatcherItf *(*buildErrorDispatcher)(struct DependencyInjectorItf *itf),
	struct VariablesWrapperBuilderItf *(*buildVariablesWrapperBuilder)(struct DependencyInjectorItf *itf),

	struct NodeItf *(*buildBedroomNode)(struct DependencyInjectorItf *itf),
	struct NodeItf *(*buildCorridorNode)(struct DependencyInjectorItf *itf)
)
{
	struct DependencyInjectorItf *itf = chatQuest_malloc(sizeof(struct DependencyInjectorItf));
	*itf = (struct DependencyInjectorItf) {
		.imp = imp,
		.destroy = destroy,

		.buildApp = buildApp,
		.buildRequestBuilder = buildRequestBuilder,
		.buildStateMachine = buildStateMachine,
		.buildResponseDispatcher = buildResponseDispatcher,
		.buildPrompter = buildPrompter,
		.buildQueryStringNormalizer = buildQueryStringNormalizer,
		.buildQueryBuilder = buildQueryBuilder,
		.buildStartStatusBuilder = buildStartStatusBuilder,
		.buildNodeBuilder = buildNodeBuilder,
		.buildErrorDispatcher = buildErrorDispatcher,
		.buildVariablesWrapperBuilder = buildVariablesWrapperBuilder,

		.buildBedroomNode = buildBedroomNode,
		.buildCorridorNode = buildCorridorNode
	};
	return itf;
}

void DependencyInjectorItf_destroy(struct DependencyInjectorItf *itf)
{
	chatQuest_free(itf);
}