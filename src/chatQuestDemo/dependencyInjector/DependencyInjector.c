#include "chatQuest/globals/globals.h"
#include "chatQuestDemo/dependencyInjector/DependencyInjector.h"
#include "chatQuestDemo/dependencyInjector/DependencyInjectorItf.h"
#include "chatQuest/app/App.h"
#include "chatQuest/app/AppItf.h"
#include "chatQuest/requestBuilder/RequestBuilder.h"
#include "chatQuest/requestBuilder/RequestBuilderItf.h"
#include "chatQuest/stateMachine/StateMachine.h"
#include "chatQuest/stateMachine/StateMachineItf.h"
#include "chatQuest/responseDispatcher/ResponseDispatcher.h"
#include "chatQuest/responseDispatcher/ResponseDispatcherItf.h"
#include "chatQuest/prompter/Prompter.h"
#include "chatQuest/prompter/PrompterItf.h"
#include "chatQuest/queryStringNormalizer/QueryStringNormalizer.h"
#include "chatQuest/queryStringNormalizer/QueryStringNormalizerItf.h"
#include "chatQuest/queryBuilder/QueryBuilder.h"
#include "chatQuest/queryBuilder/QueryBuilderItf.h"
#include "chatQuestDemo/startStatusBuilder/StartStatusBuilder.h"
#include "chatQuest/startStatusBuilder/StartStatusBuilderItf.h"
#include "chatQuestDemo/nodeBuilder/NodeBuilder.h"
#include "chatQuest/nodeBuilder/NodeBuilderItf.h"
#include "chatQuest/errorDispatcher/ErrorDispatcher.h"
#include "chatQuest/errorDispatcher/ErrorDispatcherItf.h"
#include "chatQuestDemo/variablesWrapperBuilder/VariablesWrapperBuilder.h"
#include "chatQuestDemo/variablesWrapperBuilder/VariablesWrapperBuilderItf.h"

#include "chatQuest/node/NodeItf.h"
#include "chatQuestDemo/node/bedroom/BedroomNode.h"
#include "chatQuestDemo/node/corridor/CorridorNode.h"

struct DependencyInjectorSta
{
};

static struct DependencyInjectorSta *DependencyInjectorSta_new()
{
	struct DependencyInjectorSta *sta = chatQuest_malloc(sizeof(struct DependencyInjectorSta));
	*sta = (struct DependencyInjectorSta) {
	};
	return sta;
}

static void DependencyInjectorSta_destroy(struct DependencyInjectorSta *sta)
{
	chatQuest_free(sta);
}

static void DependencyInjector_destroy(struct DependencyInjectorItf *itf)
{
	struct DependencyInjector *imp = (struct DependencyInjector *)itf->imp;

	DependencyInjectorSta_destroy(imp->sta);
	DependencyInjectorItf_destroy(imp->DependencyInjectorItf);
	chatQuest_free(imp);
}

static struct AppItf *DependencyInjector_buildApp(struct DependencyInjectorItf *itf)
{
	return App_new(
		itf->buildRequestBuilder(itf),
		itf->buildStateMachine(itf),
		itf->buildResponseDispatcher(itf),
		itf->buildErrorDispatcher(itf)
	)->AppItf;
}

static struct RequestBuilderItf *DependencyInjector_buildRequestBuilder(struct DependencyInjectorItf *itf)
{
	return RequestBuilder_new(
		itf->buildStartStatusBuilder(itf),
		itf->buildQueryBuilder(itf)
	)->RequestBuilderItf;
}

static struct StateMachineItf *DependencyInjector_buildStateMachine(struct DependencyInjectorItf *itf)
{
	return StateMachine_new(
		itf->buildNodeBuilder(itf),
		itf->buildErrorDispatcher(itf)
	)->StateMachineItf;
}

static struct ResponseDispatcherItf *DependencyInjector_buildResponseDispatcher(struct DependencyInjectorItf *itf)
{
	return ResponseDispatcher_new(
	)->ResponseDispatcherItf;
}

static struct PrompterItf *DependencyInjector_buildPrompter(struct DependencyInjectorItf *itf)
{
	return Prompter_new(
	)->PrompterItf;
}

static struct QueryStringNormalizerItf *DependencyInjector_buildQueryStringNormalizer(struct DependencyInjectorItf *itf)
{
	return QueryStringNormalizer_new(
	)->QueryStringNormalizerItf;
}

static struct QueryBuilderItf *DependencyInjector_buildQueryBuilder(struct DependencyInjectorItf *itf)
{
	return QueryBuilder_new(
		itf->buildPrompter(itf),
		itf->buildQueryStringNormalizer(itf)
	)->QueryBuilderItf;
}

static struct StartStatusBuilderItf *DependencyInjector_buildStartStatusBuilder(struct DependencyInjectorItf *itf)
{
	return StartStatusBuilder_new(
	)->StartStatusBuilderItf;
}

static struct NodeBuilderItf *DependencyInjector_buildNodeBuilder(struct DependencyInjectorItf *itf)
{
	return NodeBuilder_new(
		itf
	)->NodeBuilderItf;
}

static struct ErrorDispatcherItf *DependencyInjector_buildErrorDispatcher(struct DependencyInjectorItf *itf)
{
	return ErrorDispatcher_new(
		itf
	)->ErrorDispatcherItf;
}

static struct VariablesWrapperBuilderItf *DependencyInjector_buildVariablesWrapperBuilder(struct DependencyInjectorItf *itf)
{
	return VariablesWrapperBuilder_new(
	)->VariablesWrapperBuilderItf;
}

static struct NodeItf *DependencyInjector_buildBedroomNode(struct DependencyInjectorItf *itf)
{
	return BedroomNode_new(
		itf->buildErrorDispatcher(itf),
		itf->buildVariablesWrapperBuilder(itf)
	)->NodeItf;
}

static struct NodeItf *DependencyInjector_buildCorridorNode(struct DependencyInjectorItf *itf)
{
	return CorridorNode_new(
		itf->buildErrorDispatcher(itf),
		itf->buildVariablesWrapperBuilder(itf)
	)->NodeItf;
}

struct DependencyInjector *DependencyInjector_new()
{
	struct DependencyInjector *imp = chatQuest_malloc(sizeof(struct DependencyInjector));
	*imp = (struct DependencyInjector) {
		.sta = DependencyInjectorSta_new(),
		.DependencyInjectorItf = DependencyInjectorItf_new(
			imp,
			&DependencyInjector_destroy,

			&DependencyInjector_buildApp,
			&DependencyInjector_buildRequestBuilder,
			&DependencyInjector_buildStateMachine,
			&DependencyInjector_buildResponseDispatcher,
			&DependencyInjector_buildPrompter,
			&DependencyInjector_buildQueryStringNormalizer,
			&DependencyInjector_buildQueryBuilder,
			&DependencyInjector_buildStartStatusBuilder,
			&DependencyInjector_buildNodeBuilder,
			&DependencyInjector_buildErrorDispatcher,
			&DependencyInjector_buildVariablesWrapperBuilder,

			&DependencyInjector_buildBedroomNode,
			&DependencyInjector_buildCorridorNode
		)
	};
	return imp;
}