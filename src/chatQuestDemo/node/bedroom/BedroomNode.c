#include "chatQuest/globals/globals.h"
#include "chatQuestDemo/node/bedroom/BedroomNode.h"
#include "chatQuest/node/NodeAbs.h"
#include "chatQuest/node/NodeItf.h"
#include "chatQuest/request/RequestItf.h"
#include "chatQuest/response/ResponseItf.h"
#include "chatQuest/response/ResponseAction.h"
#include "chatQuestDemo/node/corridor/CorridorNode.h"
#include "chatQuest/slide/SlideItf.h"
#include "chatQuest/stringArray/StringArrayItf.h"
#include "chatQuest/slideArray/SlideArrayItf.h"
#include "chatQuest/slide/TransitionIn.h"
#include "chatQuest/slide/TransitionOut.h"
#include "chatQuest/errorDispatcher/ErrorDispatcherItf.h"
#include "chatQuest/query/QueryItf.h"
#include "chatQuestDemo/variablesWrapper/VariablesWrapperItf.h"
#include "chatQuestDemo/variablesWrapperBuilder/VariablesWrapperBuilderItf.h"
#include "chatQuest/node/NodeError.h"
#include "chatQuest/error/Error.h"
#include "chatQuest/error/ErrorItf.h"

struct BedroomNodeSta
{
	struct VariablesWrapperBuilderItf *variablesWrapperBuilder;
};

static struct BedroomNodeSta *BedroomNodeSta_new(
	struct BedroomNode *imp,

	struct VariablesWrapperBuilderItf *variablesWrapperBuilder
)
{
	struct BedroomNodeSta *sta = chatQuest_malloc(sizeof(struct BedroomNodeSta));
	*sta = (struct BedroomNodeSta) {
		.variablesWrapperBuilder = variablesWrapperBuilder
	};
	return sta;
}

static void BedroomNodeSta_destroy(struct BedroomNodeSta *sta)
{
	chatQuest_free(sta);
}

static void BedroomNode_destroy(struct NodeItf *itf)
{
	struct BedroomNode *imp = (struct BedroomNode *)itf->imp;

	BedroomNodeSta_destroy(imp->sta);

	NodeItf_destroy(imp->NodeItf);

	struct NodeAbs *abs = imp->NodeAbs;
	abs->destroy(abs);

	chatQuest_free(imp);
}

static char *BedroomNode_getId(struct NodeItf *itf)
{
	struct NodeAbs *abs = ((struct BedroomNode *)itf->imp)->NodeAbs;
	return abs->getId(abs);
}

static void BedroomNode_runByQueryAndResponse(struct NodeAbs *abs, struct QueryItf *query, struct ResponseItf *response)
{
	struct BedroomNodeSta *sta = ((struct BedroomNode *)abs->imp)->sta;
	struct StatusItf *status = response->getStatus(response);
	struct VariablesWrapperBuilderItf *variablesWrapperBuilder = sta->variablesWrapperBuilder;

	variablesWrapperBuilder->build(variablesWrapperBuilder, status->getVariables(status));
	if (!variablesWrapperBuilder->hasBuilt(variablesWrapperBuilder))
	{
		struct ErrorDispatcherItf *errorDispatcher = abs->getErrorDispatcher(abs);
		struct ErrorItf *error = Error_new(
			CHATQUEST_NODEERROR_VARIABLESWRAPPER_NOT_BUILT,
			__FILE__,
			__LINE__,
			"The VariablesWrapper could not be built."
		)->ErrorItf;
		errorDispatcher->dispatchAndDestroy(errorDispatcher, error);
	}
	struct VariablesWrapperItf *variablesWrapper = variablesWrapperBuilder->ownBuiltAfterHas(variablesWrapperBuilder);

	char *verb = 
		query->hasVerb(query)
			? query->getVerbAfterHas(query)
			: NULL
	;

	char *direct =
		query->hasDirect(query)
			? query->getDirectAfterHas(query)
			: NULL
	;

	if (
		verb == NULL
		|| (
			chatQuest_areStringsEqual(verb, "look")
			&& direct == NULL
		)
	)
	{
		struct SlideArrayItf *slides = abs->newSlideArray(abs);

		struct SlideItf *slide = abs->newSlide(abs);
		slide->setTitle(slide, "Bedroom");
		slide->setBody(slide, "I am in the bedroom.\n\nI can see the <exit>corridor</exit> from here.");
		slides->append(slides, slide);

		response->setSlides(response, slides);
	}
	else if (chatQuest_areStringsEqual(verb, "greet"))
	{
		struct SlideArrayItf *slides = abs->newSlideArray(abs);

		struct SlideItf *slide = abs->newSlide(abs);
		slide->setBody(slide, "hello");
		slide->setTransitionOut(slide, CHATQUEST_TRANSITION_OUT_PROMPT);
		slides->append(slides, slide);

		slide = abs->newSlide(abs);
		slide->setBody(slide, "friend");
		slides->append(slides, slide);

		response->setSlides(response, slides);
	}
	else if (
		chatQuest_areStringsEqual(verb, "go")
		&& direct != NULL
		&& chatQuest_areStringsEqual(direct, "corridor")
	)
	{
		struct StringArrayItf *nodeIds = status->getNodeIds(status);
		nodeIds->pop(nodeIds);
		nodeIds->append(nodeIds, CHATQUESTDEMO_CORRIDORNODE_ID);
		response->setNextAction(response, CHATQUEST_RESPONSEACTION_ENTER);
	}
	else
	{
		response = abs->globalRunByQueryAndResponse(abs, query, response);
	}

	abs->setResponse(abs, response);
	variablesWrapper->destroy(variablesWrapper);
}

static void BedroomNode_run(struct NodeItf *itf, struct RequestItf *request)
{
	struct NodeAbs *abs = ((struct BedroomNode *)itf->imp)->NodeAbs;
	return abs->run(abs, request);
}

static bool Bedroom_hasResponse(struct NodeItf *itf)
{
	struct NodeAbs *abs = ((struct BedroomNode *)itf->imp)->NodeAbs;
	return abs->hasResponse(abs);
}

static struct ResponseItf *Bedroom_ownResponseAfterHas(struct NodeItf *itf)
{
	struct NodeAbs *abs = ((struct BedroomNode *)itf->imp)->NodeAbs;
	return abs->ownResponseAfterHas(abs);
}

static bool BedroomNode_hasResponse(struct NodeItf *itf)
{
	struct NodeAbs *abs = ((struct BedroomNode *)itf->imp)->NodeAbs;
	return abs->hasResponse(abs);
}

static struct ResponseItf *BedroomNode_ownResponseAfterHas(struct NodeItf *itf)
{
	struct NodeAbs *abs = ((struct BedroomNode *)itf->imp)->NodeAbs;
	return abs->ownResponseAfterHas(abs);
}

struct BedroomNode *BedroomNode_new(
	struct ErrorDispatcherItf *errorDispatcher,
	struct VariablesWrapperBuilderItf *variablesWrapperBuilder
)
{
	struct BedroomNode *imp = chatQuest_malloc(sizeof(struct BedroomNode));
	*imp = (struct BedroomNode) {
		.sta = BedroomNodeSta_new(
			imp,

			variablesWrapperBuilder
		),
		.NodeItf = NodeItf_new(
			imp,
			&BedroomNode_destroy,

			&BedroomNode_getId,

			&BedroomNode_run,

			&BedroomNode_hasResponse,
			&BedroomNode_ownResponseAfterHas
		),
		.NodeAbs = NodeAbs_new(
			imp,
			CHATQUESTDEMO_BEDROOMNODE_ID,

			errorDispatcher,

			&BedroomNode_runByQueryAndResponse
		)
	};
	return imp;
}