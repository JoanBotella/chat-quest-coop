#ifndef CHATQUESTDEMO_BEDROOMNODE_HEADER
#define CHATQUESTDEMO_BEDROOMNODE_HEADER

#define CHATQUESTDEMO_BEDROOMNODE_ID "chatQuestDemo_bedroomNode_id"

#include "chatQuest/node/NodeItf.h"
#include "chatQuest/node/NodeAbs.h"
#include "chatQuest/errorDispatcher/ErrorDispatcherItf.h"
#include "chatQuestDemo/variablesWrapperBuilder/VariablesWrapperBuilderItf.h"

struct BedroomNodeSta;

struct BedroomNode
{
	struct BedroomNodeSta *sta;
	struct NodeItf *NodeItf;
	struct NodeAbs *NodeAbs;
};

struct BedroomNode *BedroomNode_new(
	struct ErrorDispatcherItf *errorDispatcher,
	struct VariablesWrapperBuilderItf *variablesWrapperBuilder
);

#endif
