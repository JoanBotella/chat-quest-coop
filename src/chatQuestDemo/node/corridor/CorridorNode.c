#include "chatQuest/globals/globals.h"
#include "chatQuestDemo/node/corridor/CorridorNode.h"
#include "chatQuest/node/NodeAbs.h"
#include "chatQuest/node/NodeItf.h"
#include "chatQuest/request/RequestItf.h"
#include "chatQuest/response/ResponseItf.h"
#include "chatQuest/response/ResponseAction.h"
#include "chatQuestDemo/node/bedroom/BedroomNode.h"
#include "chatQuest/slide/SlideItf.h"
#include "chatQuest/stringArray/StringArrayItf.h"
#include "chatQuest/slideArray/SlideArrayItf.h"
#include "chatQuest/errorDispatcher/ErrorDispatcherItf.h"
#include "chatQuest/query/QueryItf.h"
#include "chatQuestDemo/variablesWrapper/VariablesWrapperItf.h"
#include "chatQuestDemo/variablesWrapperBuilder/VariablesWrapperBuilderItf.h"
#include "chatQuest/node/NodeError.h"
#include "chatQuest/error/Error.h"
#include "chatQuest/error/ErrorItf.h"

struct CorridorNodeSta
{
	struct VariablesWrapperBuilderItf *variablesWrapperBuilder;
};

static struct CorridorNodeSta *CorridorNodeSta_new(
	struct CorridorNode *imp,

	struct VariablesWrapperBuilderItf *variablesWrapperBuilder
)
{
	struct CorridorNodeSta *sta = chatQuest_malloc(sizeof(struct CorridorNodeSta));
	*sta = (struct CorridorNodeSta) {
		.variablesWrapperBuilder = variablesWrapperBuilder
	};
	return sta;
}

static void CorridorNodeSta_destroy(struct CorridorNodeSta *sta)
{
	chatQuest_free(sta);
}

static void CorridorNode_destroy(struct NodeItf *itf)
{
	struct CorridorNode *imp = (struct CorridorNode *)itf->imp;

	CorridorNodeSta_destroy(imp->sta);

	NodeItf_destroy(imp->NodeItf);

	struct NodeAbs *abs = imp->NodeAbs;
	abs->destroy(abs);

	chatQuest_free(imp);
}

static char *CorridorNode_getId(struct NodeItf *itf)
{
	struct NodeAbs *abs = ((struct CorridorNode *)itf->imp)->NodeAbs;
	return abs->getId(abs);
}

static void CorridorNode_runByQueryAndResponse(struct NodeAbs *abs, struct QueryItf *query, struct ResponseItf *response)
{
	struct CorridorNodeSta *sta = ((struct CorridorNode *)abs->imp)->sta;
	struct StatusItf *status = response->getStatus(response);
	struct VariablesWrapperBuilderItf *variablesWrapperBuilder = sta->variablesWrapperBuilder;

	variablesWrapperBuilder->build(variablesWrapperBuilder, status->getVariables(status));
	if (!variablesWrapperBuilder->hasBuilt(variablesWrapperBuilder))
	{
		struct ErrorDispatcherItf *errorDispatcher = abs->getErrorDispatcher(abs);
		struct ErrorItf *error = Error_new(
			CHATQUEST_NODEERROR_VARIABLESWRAPPER_NOT_BUILT,
			__FILE__,
			__LINE__,
			"The VariablesWrapper could not be built."
		)->ErrorItf;
		errorDispatcher->dispatchAndDestroy(errorDispatcher, error);
	}
	struct VariablesWrapperItf *variablesWrapper = variablesWrapperBuilder->ownBuiltAfterHas(variablesWrapperBuilder);

	char *verb = 
		query->hasVerb(query)
			? query->getVerbAfterHas(query)
			: NULL
	;

	char *direct =
		query->hasDirect(query)
			? query->getDirectAfterHas(query)
			: NULL
	;

	if (
		verb == NULL
		|| (
			chatQuest_areStringsEqual(verb, "look")
			&& direct == NULL
		)
	)
	{
		struct SlideArrayItf *slides = abs->newSlideArray(abs);

		struct SlideItf *slide = abs->newSlide(abs);
		slide->setTitle(slide, "Corridor");

		int corridorTimes =
			variablesWrapper->hasCorridorTimes(variablesWrapper)
				? variablesWrapper->getCorridorTimesAfterHas(variablesWrapper)
				: 0
		;
		char *body = chatQuest_buildString("I am in the corridor.\n\nI've been here %d times before.\n\nI can go to the <exit>bedroom</exit>.", corridorTimes);
		slide->setBody(slide, body);
		chatQuest_free(body);
		variablesWrapper->setCorridorTimes(variablesWrapper, corridorTimes + 1);

		slides->append(slides, slide);

		response->setSlides(response, slides);
	}
	else if (chatQuest_areStringsEqual(verb, "greet"))
	{
		struct SlideArrayItf *slides = abs->newSlideArray(abs);

		struct SlideItf *slide = abs->newSlide(abs);
		slide->setBody(slide, "hello");
		slides->append(slides, slide);

		response->setSlides(response, slides);
	}
	else if (chatQuest_areStringsEqual(verb, "quit"))
	{
		struct SlideArrayItf *slides = abs->newSlideArray(abs);

		struct SlideItf *slide = abs->newSlide(abs);
		slide->setBody(slide, "Sorry! I can't quit while in the corridor.");
		slides->append(slides, slide);

		response->setSlides(response, slides);
	}
	else if (
		chatQuest_areStringsEqual(verb, "go")
		&& direct != NULL
		&& chatQuest_areStringsEqual(direct, "bedroom")
	)
	{
		struct StringArrayItf *nodeIds = status->getNodeIds(status);
		nodeIds->pop(nodeIds);
		nodeIds->append(nodeIds, CHATQUESTDEMO_BEDROOMNODE_ID);
		response->setNextAction(response, CHATQUEST_RESPONSEACTION_ENTER);
	}
	else
	{
		response = abs->globalRunByQueryAndResponse(abs, query, response);
	}

	abs->setResponse(abs, response);
	variablesWrapper->destroy(variablesWrapper);
}

static void CorridorNode_run(struct NodeItf *itf, struct RequestItf *request)
{
	struct NodeAbs *abs = ((struct CorridorNode *)itf->imp)->NodeAbs;
	return abs->run(abs, request);
}

static bool CorridorNode_hasResponse(struct NodeItf *itf)
{
	struct NodeAbs *abs = ((struct CorridorNode *)itf->imp)->NodeAbs;
	return abs->hasResponse(abs);
}

static struct ResponseItf *CorridorNode_ownResponseAfterHas(struct NodeItf *itf)
{
	struct NodeAbs *abs = ((struct CorridorNode *)itf->imp)->NodeAbs;
	return abs->ownResponseAfterHas(abs);
}

struct CorridorNode *CorridorNode_new(
	struct ErrorDispatcherItf *errorDispatcher,
	struct VariablesWrapperBuilderItf *variablesWrapperBuilder
)
{
	struct CorridorNode *imp = chatQuest_malloc(sizeof(struct CorridorNode));
	*imp = (struct CorridorNode) {
		.sta = CorridorNodeSta_new(
			imp,

			variablesWrapperBuilder
		),
		.NodeItf = NodeItf_new(
			imp,
			&CorridorNode_destroy,

			&CorridorNode_getId,

			&CorridorNode_run,

			&CorridorNode_hasResponse,
			&CorridorNode_ownResponseAfterHas
		),
		.NodeAbs = NodeAbs_new(
			imp,
			CHATQUESTDEMO_CORRIDORNODE_ID,

			errorDispatcher,

			&CorridorNode_runByQueryAndResponse
		)
	};
	return imp;
}