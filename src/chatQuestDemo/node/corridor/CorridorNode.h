#ifndef CHATQUESTDEMO_CORRIDORNODE_HEADER
#define CHATQUESTDEMO_CORRIDORNODE_HEADER

#define CHATQUESTDEMO_CORRIDORNODE_ID "chatQuestDemo_corridorNode_id"

#include "chatQuest/node/NodeItf.h"
#include "chatQuest/node/NodeAbs.h"
#include "chatQuest/errorDispatcher/ErrorDispatcherItf.h"
#include "chatQuestDemo/variablesWrapperBuilder/VariablesWrapperBuilderItf.h"

struct CorridorNodeSta;

struct CorridorNode
{
	struct CorridorNodeSta *sta;
	struct NodeItf *NodeItf;
	struct NodeAbs *NodeAbs;
};

struct CorridorNode *CorridorNode_new(
	struct ErrorDispatcherItf *errorDispatcher,
	struct VariablesWrapperBuilderItf *variablesWrapperBuilder
);

#endif
