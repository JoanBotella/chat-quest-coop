#ifndef CHATQUESTDEMO_NODEBUILDER_HEADER
#define CHATQUESTDEMO_NODEBUILDER_HEADER

#include "chatQuest/nodeBuilder/NodeBuilderItf.h"
#include "chatQuestDemo/dependencyInjector/DependencyInjectorItf.h"

struct NodeBuilderSta;

struct NodeBuilder
{
	struct NodeBuilderSta *sta;
	struct NodeBuilderItf *NodeBuilderItf;
};

struct NodeBuilder *NodeBuilder_new(
	struct DependencyInjectorItf *dependencyInjector
);

#endif
