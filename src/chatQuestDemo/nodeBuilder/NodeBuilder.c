#include "chatQuest/globals/globals.h"
#include "chatQuestDemo/nodeBuilder/NodeBuilder.h"
#include "chatQuest/nodeBuilder/NodeBuilderItf.h"
#include "chatQuestDemo/dependencyInjector/DependencyInjectorItf.h"

#include "chatQuestDemo/node/bedroom/BedroomNode.h"
#include "chatQuestDemo/node/corridor/CorridorNode.h"

struct NodeBuilderSta
{
	struct DependencyInjectorItf *dependencyInjector;
	struct NodeItf *node;
};

static struct NodeBuilderSta *NodeBuilderSta_new(
	struct DependencyInjectorItf *dependencyInjector
)
{
	struct NodeBuilderSta *sta = chatQuest_malloc(sizeof(struct NodeBuilderSta));
	*sta = (struct NodeBuilderSta) {
		.dependencyInjector = dependencyInjector,
		.node = NULL
	};
	return sta;
}

static void NodeBuilderSta_destroy(struct NodeBuilderSta *sta)
{
	// DependencyInjector is not destroyed here
	// The node is not destroyed here
	chatQuest_free(sta);
}

static void NodeBuilder_destroy(struct NodeBuilderItf *itf)
{
	struct NodeBuilder *imp = (struct NodeBuilder *)itf->imp;

	NodeBuilderSta_destroy(imp->sta);
	NodeBuilderItf_destroy(imp->NodeBuilderItf);
	chatQuest_free(imp);
}

static bool NodeBuilder_hasNode(struct NodeBuilderItf *itf)
{
	struct NodeBuilderSta *sta = ((struct NodeBuilder *)itf->imp)->sta;
	return sta->node != NULL;
}

static struct NodeItf *NodeBuilder_ownNodeAfterHas(struct NodeBuilderItf *itf)
{
	struct NodeBuilderSta *sta = ((struct NodeBuilder *)itf->imp)->sta;
	struct NodeItf *r = sta->node;
	sta->node = NULL;
	return r;
}

static void *NodeBuilder_setNode(struct NodeBuilder *imp, struct NodeItf *node)
{
	struct NodeBuilderSta *sta = imp->sta;
	sta->node = node;
}

static void *NodeBuilder_unsetNode(struct NodeBuilder *imp)
{
	struct NodeBuilderSta *sta = imp->sta;
	if (sta->node != NULL)
	{
		sta->node->destroy(sta->node);
		sta->node = NULL;
	}
}

static void NodeBuilder_build(struct NodeBuilderItf *itf, char *nodeId)
{
	struct NodeBuilder *imp = (struct NodeBuilder *)itf->imp;
	struct NodeBuilderSta *sta = imp->sta;

	NodeBuilder_unsetNode(imp);

	struct DependencyInjectorItf *dependencyInjector = sta->dependencyInjector;

	if (chatQuest_areStringsEqual(nodeId, CHATQUESTDEMO_BEDROOMNODE_ID))
	{
		NodeBuilder_setNode(
			imp,
			dependencyInjector->buildBedroomNode(dependencyInjector)
		);
	}
	else if (chatQuest_areStringsEqual(nodeId, CHATQUESTDEMO_CORRIDORNODE_ID))
	{
		NodeBuilder_setNode(
			imp,
			dependencyInjector->buildCorridorNode(dependencyInjector)
		);
	}
}

struct NodeBuilder *NodeBuilder_new(
	struct DependencyInjectorItf *dependencyInjector
)
{
	struct NodeBuilder *imp = chatQuest_malloc(sizeof(struct NodeBuilder));
	*imp = (struct NodeBuilder) {
		.sta = NodeBuilderSta_new(
			dependencyInjector
		),
		.NodeBuilderItf = NodeBuilderItf_new(
			imp,
			&NodeBuilder_destroy,

			&NodeBuilder_build,

			&NodeBuilder_hasNode,
			&NodeBuilder_ownNodeAfterHas
		)
	};
	return imp;
}