#ifndef CHATQUEST_VARIABLESWRAPPER_HEADER
#define CHATQUEST_VARIABLESWRAPPER_HEADER

#include "chatQuestDemo/variablesWrapper/VariablesWrapperItf.h"
#include "chatQuest/variablesWrapper/VariablesWrapperAbs.h"
#include "chatQuest/stringToStringMap/StringToStringMapItf.h"

struct VariablesWrapperSta;

struct VariablesWrapper
{
	struct VariablesWrapperSta *sta;
	struct VariablesWrapperItf *VariablesWrapperItf;
	struct VariablesWrapperAbs *VariablesWrapperAbs;
};

struct VariablesWrapper *VariablesWrapper_new(
	struct StringToStringMapItf *variables
);

#endif
