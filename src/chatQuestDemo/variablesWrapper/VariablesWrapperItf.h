#ifndef CHATQUEST_VARIABLESWRAPPERITF_HEADER
#define CHATQUEST_VARIABLESWRAPPERITF_HEADER

#include "chatQuest/globals/globals.h"

struct VariablesWrapperItf
{
	void *imp;
	void (*destroy)(struct VariablesWrapperItf *itf);

	bool (*hasCorridorTimes)(struct VariablesWrapperItf *itf);
	int (*getCorridorTimesAfterHas)(struct VariablesWrapperItf *itf);
	void (*setCorridorTimes)(struct VariablesWrapperItf *itf, int value);
	void (*unsetCorridorTimes)(struct VariablesWrapperItf *itf);
};

struct VariablesWrapperItf *VariablesWrapperItf_new(
	void *imp,
	void (*destroy)(struct VariablesWrapperItf *itf),

	bool (*hasCorridorTimes)(struct VariablesWrapperItf *itf),
	int (*getCorridorTimesAfterHas)(struct VariablesWrapperItf *itf),
	void (*setCorridorTimes)(struct VariablesWrapperItf *itf, int value),
	void (*unsetCorridorTimes)(struct VariablesWrapperItf *itf)
);

void VariablesWrapperItf_destroy(struct VariablesWrapperItf *itf);

#endif