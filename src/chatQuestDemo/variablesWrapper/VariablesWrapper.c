#include "chatQuest/globals/globals.h"
#include "chatQuestDemo/variablesWrapper/VariablesWrapper.h"
#include "chatQuestDemo/variablesWrapper/VariablesWrapperItf.h"
#include "chatQuest/variablesWrapper/VariablesWrapperAbs.h"

#define VARIABLE_NAME_CORRIDOR_TIMES "corridorTimes"

struct VariablesWrapperSta
{
};

static struct VariablesWrapperSta *VariablesWrapperSta_new()
{
	struct VariablesWrapperSta *sta = chatQuest_malloc(sizeof(struct VariablesWrapperSta));
	*sta = (struct VariablesWrapperSta) {
	};
	return sta;
}

static void VariablesWrapperSta_destroy(struct VariablesWrapperSta *sta)
{
	chatQuest_free(sta);
}

static void VariablesWrapper_destroy(struct VariablesWrapperItf *itf)
{
	struct VariablesWrapper *imp = (struct VariablesWrapper *)itf->imp;
	struct VariablesWrapperAbs *abs = imp->VariablesWrapperAbs;

	VariablesWrapperSta_destroy(imp->sta);
	VariablesWrapperItf_destroy(imp->VariablesWrapperItf);
	abs->destroy(abs);
	chatQuest_free(imp);
}

static bool VariablesWrapper_hasCorridorTimes(struct VariablesWrapperItf *itf)
{
	struct VariablesWrapperAbs *abs = ((struct VariablesWrapper *)itf->imp)->VariablesWrapperAbs;
	return abs->hasVariable(abs, VARIABLE_NAME_CORRIDOR_TIMES);
}

static int VariablesWrapper_getCorridorTimesAfterHas(struct VariablesWrapperItf *itf)
{
	struct VariablesWrapperAbs *abs = ((struct VariablesWrapper *)itf->imp)->VariablesWrapperAbs;
	return abs->getIntVariableAfterHas(abs, VARIABLE_NAME_CORRIDOR_TIMES);
}

static void VariablesWrapper_setCorridorTimes(struct VariablesWrapperItf *itf, int value)
{
	struct VariablesWrapperAbs *abs = ((struct VariablesWrapper *)itf->imp)->VariablesWrapperAbs;
	abs->setIntVariable(abs, VARIABLE_NAME_CORRIDOR_TIMES, value);
}

static void VariablesWrapper_unsetCorridorTimes(struct VariablesWrapperItf *itf)
{
	struct VariablesWrapperAbs *abs = ((struct VariablesWrapper *)itf->imp)->VariablesWrapperAbs;
	abs->unsetVariable(abs, VARIABLE_NAME_CORRIDOR_TIMES);
}

struct VariablesWrapper *VariablesWrapper_new(
	struct StringToStringMapItf *variables
)
{
	struct VariablesWrapper *imp = chatQuest_malloc(sizeof(struct VariablesWrapper));
	*imp = (struct VariablesWrapper) {
		.sta = VariablesWrapperSta_new(
		),
		.VariablesWrapperItf = VariablesWrapperItf_new(
			imp,
			&VariablesWrapper_destroy,

			&VariablesWrapper_hasCorridorTimes,
			&VariablesWrapper_getCorridorTimesAfterHas,
			&VariablesWrapper_setCorridorTimes,
			&VariablesWrapper_unsetCorridorTimes
		),
		.VariablesWrapperAbs = VariablesWrapperAbs_new(
			imp,

			variables
		)
	};
	return imp;
}
