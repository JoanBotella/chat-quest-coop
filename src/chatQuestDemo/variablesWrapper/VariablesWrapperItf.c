#include "chatQuest/globals/globals.h"
#include "chatQuestDemo/variablesWrapper/VariablesWrapperItf.h"

struct VariablesWrapperItf *VariablesWrapperItf_new(
	void *imp,
	void (*destroy)(struct VariablesWrapperItf *itf),

	bool (*hasCorridorTimes)(struct VariablesWrapperItf *itf),
	int (*getCorridorTimesAfterHas)(struct VariablesWrapperItf *itf),
	void (*setCorridorTimes)(struct VariablesWrapperItf *itf, int value),
	void (*unsetCorridorTimes)(struct VariablesWrapperItf *itf)
)
{
	struct VariablesWrapperItf *itf = chatQuest_malloc(sizeof(struct VariablesWrapperItf));
	*itf = (struct VariablesWrapperItf) {
		.imp = imp,
		.destroy = destroy,

		.hasCorridorTimes = hasCorridorTimes,
		.getCorridorTimesAfterHas = getCorridorTimesAfterHas,
		.setCorridorTimes = setCorridorTimes,
		.unsetCorridorTimes = unsetCorridorTimes
	};
	return itf;
}

void VariablesWrapperItf_destroy(struct VariablesWrapperItf *itf)
{
	chatQuest_free(itf);
}