#include "chatQuestDemo/dependencyInjector/DependencyInjector.h"
#include "chatQuestDemo/dependencyInjector/DependencyInjectorItf.h"
#include "chatQuest/app/AppItf.h"

int main(void)
{
	struct DependencyInjectorItf *dependencyInjector = DependencyInjector_new()->DependencyInjectorItf;

	struct AppItf *app = dependencyInjector->buildApp(dependencyInjector);
	app->run(app);

	app->destroy(app);
	dependencyInjector->destroy(dependencyInjector);

	return 0;
}