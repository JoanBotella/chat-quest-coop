#ifndef CHATQUEST_RESPONSE_HEADER
#define CHATQUEST_RESPONSE_HEADER

#include "chatQuest/response/ResponseItf.h"
#include "chatQuest/status/StatusItf.h"

struct ResponseSta;

struct Response
{
	struct ResponseSta *sta;
	struct ResponseItf *ResponseItf;
};

struct Response *Response_new(
	struct StatusItf *status
);

#endif
