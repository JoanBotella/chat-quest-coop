#include "chatQuest/globals/globals.h"
#include "chatQuest/response/ResponseItf.h"
#include "chatQuest/status/StatusItf.h"
#include "chatQuest/response/ResponseAction.h"
#include "chatQuest/slideArray/SlideArrayItf.h"

struct ResponseItf *ResponseItf_new(
	void *imp,
	void (*destroy)(struct ResponseItf *itf),

	bool (*hasSlides)(struct ResponseItf *itf),
	struct SlideArrayItf *(*getSlidesAfterHas)(struct ResponseItf *itf),
	void (*setSlides)(struct ResponseItf *itf, struct SlideArrayItf *slides),
	void (*unsetSlides)(struct ResponseItf *itf),

	struct StatusItf *(*getStatus)(struct ResponseItf *itf),
	void (*setStatus)(struct ResponseItf *itf, struct StatusItf *status),

	enum ResponseAction (*getNextAction)(struct ResponseItf *itf),
	void (*setNextAction)(struct ResponseItf *itf, enum ResponseAction nextAction)
)
{
	struct ResponseItf *itf = chatQuest_malloc(sizeof(struct ResponseItf));
	*itf = (struct ResponseItf) {
		.imp = imp,
		.destroy = destroy,

		.hasSlides = hasSlides,
		.getSlidesAfterHas = getSlidesAfterHas,
		.setSlides = setSlides,
		.unsetSlides = unsetSlides,

		.getStatus = getStatus,
		.setStatus = setStatus,

		.getNextAction = getNextAction,
		.setNextAction = setNextAction
	};
	return itf;
}

void ResponseItf_destroy(struct ResponseItf *itf)
{
	chatQuest_free(itf);
}