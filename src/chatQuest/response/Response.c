#include "chatQuest/globals/globals.h"
#include "chatQuest/response/Response.h"
#include "chatQuest/response/ResponseItf.h"
#include "chatQuest/status/StatusItf.h"
#include "chatQuest/response/ResponseAction.h"
#include "chatQuest/slideArray/SlideArrayItf.h"

struct ResponseSta
{
	struct SlideArrayItf *slides;
	struct StatusItf *status;
	enum ResponseAction nextAction;
};

static struct ResponseSta *ResponseSta_new(
	struct StatusItf *status
)
{
	struct ResponseSta *sta = chatQuest_malloc(sizeof(struct ResponseSta));
	*sta = (struct ResponseSta) {
		.slides = NULL,
		.status = status,
		.nextAction = CHATQUEST_RESPONSEACTION_RUN
	};
	return sta;
}

static void ResponseSta_destroy(struct ResponseSta *sta)
{
	struct SlideArrayItf *slides = sta->slides;
	if (slides != NULL)
	{
		slides->destroy(slides);
	}
	// Do not destroy the Status
	chatQuest_free(sta);
}

static void Response_destroy(struct ResponseItf *itf)
{
	struct Response *imp = (struct Response *)itf->imp;

	ResponseSta_destroy(imp->sta);
	ResponseItf_destroy(imp->ResponseItf);
	chatQuest_free(imp);
}

static bool Response_hasSlides(struct ResponseItf *itf)
{
	struct ResponseSta *sta = ((struct Response *)itf->imp)->sta;
	return sta->slides != NULL;
}

static struct SlideArrayItf *Response_getSlidesAfterHas(struct ResponseItf *itf)
{
	struct ResponseSta *sta = ((struct Response *)itf->imp)->sta;
	return sta->slides;
}

static void Response_setSlides(struct ResponseItf *itf, struct SlideArrayItf *slides)
{
	struct ResponseSta *sta = ((struct Response *)itf->imp)->sta;
	sta->slides = slides;
}

static void Response_unsetSlides(struct ResponseItf *itf)
{
	struct ResponseSta *sta = ((struct Response *)itf->imp)->sta;
	chatQuest_free(sta->slides);
	sta->slides = NULL;
}

static struct StatusItf *Response_getStatus(struct ResponseItf *itf)
{
	struct ResponseSta *sta = ((struct Response *)itf->imp)->sta;
	return sta->status;
}

static void Response_setStatus(struct ResponseItf *itf, struct StatusItf *status)
{
	struct ResponseSta *sta = ((struct Response *)itf->imp)->sta;
	status = status;
}

static enum ResponseAction Response_getNextAction(struct ResponseItf *itf)
{
	struct ResponseSta *sta = ((struct Response *)itf->imp)->sta;
	return sta->nextAction;
}

static void Response_setNextAction(struct ResponseItf *itf, enum ResponseAction nextAction)
{
	struct ResponseSta *sta = ((struct Response *)itf->imp)->sta;
	sta->nextAction = nextAction;
}

struct Response *Response_new(
	struct StatusItf *status
)
{
	struct Response *imp = chatQuest_malloc(sizeof(struct Response));
	*imp = (struct Response) {
		.sta = ResponseSta_new(
			status
		),
		.ResponseItf = ResponseItf_new(
			imp,
			&Response_destroy,

			&Response_hasSlides,
			&Response_getSlidesAfterHas,
			&Response_setSlides,
			&Response_unsetSlides,

			&Response_getStatus,
			&Response_setStatus,

			&Response_getNextAction,
			&Response_setNextAction
		)
	};
	return imp;
}