#ifndef CHATQUEST_RESPONSEITF_HEADER
#define CHATQUEST_RESPONSEITF_HEADER

#include "chatQuest/globals/globals.h"
#include "chatQuest/status/StatusItf.h"
#include "chatQuest/response/ResponseAction.h"
#include "chatQuest/slide/SlideItf.h"
#include "chatQuest/slideArray/SlideArrayItf.h"

struct ResponseItf
{
	void *imp;
	void (*destroy)(struct ResponseItf *itf);

	bool (*hasSlides)(struct ResponseItf *itf);
	struct SlideArrayItf *(*getSlidesAfterHas)(struct ResponseItf *itf);
	void (*setSlides)(struct ResponseItf *itf, struct SlideArrayItf *slides);
	void (*unsetSlides)(struct ResponseItf *itf);

	struct StatusItf *(*getStatus)(struct ResponseItf *itf);
	void (*setStatus)(struct ResponseItf *itf, struct StatusItf *status);

	enum ResponseAction (*getNextAction)(struct ResponseItf *itf);
	void (*setNextAction)(struct ResponseItf *itf, enum ResponseAction nextAction);
};

struct ResponseItf *ResponseItf_new(
	void *imp,
	void (*destroy)(struct ResponseItf *itf),	// Does not destroy the Status

	bool (*hasSlides)(struct ResponseItf *itf),
	struct SlideArrayItf *(*getSlidesAfterHas)(struct ResponseItf *itf),
	void (*setSlides)(struct ResponseItf *itf, struct SlideArrayItf *slides),
	void (*unsetSlides)(struct ResponseItf *itf),

	struct StatusItf *(*getStatus)(struct ResponseItf *itf),
	void (*setStatus)(struct ResponseItf *itf, struct StatusItf *status),

	enum ResponseAction (*getNextAction)(struct ResponseItf *itf),
	void (*setNextAction)(struct ResponseItf *itf, enum ResponseAction nextAction)
);

void ResponseItf_destroy(struct ResponseItf *itf);

#endif