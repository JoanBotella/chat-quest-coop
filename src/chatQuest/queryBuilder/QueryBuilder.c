#include "chatQuest/globals/globals.h"
#include "chatQuest/queryBuilder/QueryBuilder.h"
#include "chatQuest/queryBuilder/QueryBuilderItf.h"
#include "chatQuest/prompter/PrompterItf.h"
#include "chatQuest/queryStringNormalizer/QueryStringNormalizerItf.h"
#include "chatQuest/query/Query.h"
#include "chatQuest/query/QueryItf.h"
#include "chatQuest/response/ResponseAction.h"

struct QueryBuilderSta
{
	struct PrompterItf *prompter;
	struct QueryStringNormalizerItf *queryStringNormalizer;
};

static struct QueryBuilderSta *QueryBuilderSta_new(
	struct PrompterItf *prompter,
	struct QueryStringNormalizerItf *queryStringNormalizer
)
{
	struct QueryBuilderSta *sta = chatQuest_malloc(sizeof(struct QueryBuilderSta));
	*sta = (struct QueryBuilderSta) {
		.prompter = prompter,
		.queryStringNormalizer = queryStringNormalizer
	};
	return sta;
}

static void QueryBuilderSta_destroy(struct QueryBuilderSta *sta)
{
	sta->prompter->destroy(sta->prompter);
	sta->queryStringNormalizer->destroy(sta->queryStringNormalizer);
	chatQuest_free(sta);
}

static void QueryBuilder_destroy(struct QueryBuilderItf *itf)
{
	struct QueryBuilder *imp = (struct QueryBuilder *)itf->imp;

	QueryBuilderSta_destroy(imp->sta);
	QueryBuilderItf_destroy(imp->QueryBuilderItf);
	chatQuest_free(imp);
}

	static struct QueryItf *QueryBuilder_setQueryWords(struct QueryBuilder *imp, struct QueryItf *query, char *normalized)
	{
		char **words = chatQuest_splitString(normalized, " ", 3);

		if (words[0] != NULL)
		{
			query->setVerb(query, words[0]);

			if (words[1] != NULL)
			{
				query->setDirect(query, words[1]);

				if (words[2] != NULL)
				{
					query->setIndirect(query, words[2]);
				}
			}
		}

		chatQuest_freeStringArray(words);

		return query;
	}

static struct QueryItf *QueryBuilder_build(struct QueryBuilderItf *itf, enum ResponseAction action)
{
	struct QueryBuilder *imp = (struct QueryBuilder *)itf->imp;

	struct QueryItf *query = Query_new()->QueryItf;

	if (
		action == CHATQUEST_RESPONSEACTION_START
		|| action == CHATQUEST_RESPONSEACTION_ENTER
	)
	{
		return query;
	}

	struct QueryBuilderSta *sta = ((struct QueryBuilder *)itf->imp)->sta;

	char *raw = sta->prompter->prompt(sta->prompter, "> ");
	char *normalized = sta->queryStringNormalizer->normalize(sta->queryStringNormalizer, raw);
	chatQuest_free(raw);

	query = QueryBuilder_setQueryWords(imp, query, normalized);
	chatQuest_free(normalized);

	return query;
}

struct QueryBuilder *QueryBuilder_new(
	struct PrompterItf *prompter,
	struct QueryStringNormalizerItf *queryStringNormalizer
)
{
	struct QueryBuilder *imp = chatQuest_malloc(sizeof(struct QueryBuilder));
	*imp = (struct QueryBuilder) {
		.sta = QueryBuilderSta_new(
			prompter,
			queryStringNormalizer
		),
		.QueryBuilderItf = QueryBuilderItf_new(
			imp,
			&QueryBuilder_destroy,

			&QueryBuilder_build
		)
	};
	return imp;
}