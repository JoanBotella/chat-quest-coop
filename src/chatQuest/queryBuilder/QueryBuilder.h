#ifndef CHATQUEST_QUERYBUILDER_HEADER
#define CHATQUEST_QUERYBUILDER_HEADER

#include "chatQuest/queryBuilder/QueryBuilderItf.h"
#include "chatQuest/prompter/PrompterItf.h"
#include "chatQuest/queryStringNormalizer/QueryStringNormalizerItf.h"

struct QueryBuilderSta;

struct QueryBuilder
{
	struct QueryBuilderSta *sta;
	struct QueryBuilderItf *QueryBuilderItf;
};

struct QueryBuilder *QueryBuilder_new(
	struct PrompterItf *prompter,
	struct QueryStringNormalizerItf *queryStringNormalizer
);

#endif
