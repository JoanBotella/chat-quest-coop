#include "chatQuest/globals/globals.h"
#include "chatQuest/queryBuilder/QueryBuilderItf.h"
#include "chatQuest/query/QueryItf.h"
#include "chatQuest/response/ResponseAction.h"

struct QueryBuilderItf *QueryBuilderItf_new(
	void *imp,
	void (*destroy)(struct QueryBuilderItf *itf),

	struct QueryItf *(*build)(struct QueryBuilderItf *itf, enum ResponseAction action)
)
{
	struct QueryBuilderItf *itf = chatQuest_malloc(sizeof(struct QueryBuilderItf));
	*itf = (struct QueryBuilderItf) {
		.imp = imp,
		.destroy = destroy,

		.build = build
	};
	return itf;
}

void QueryBuilderItf_destroy(struct QueryBuilderItf *itf)
{
	chatQuest_free(itf);
}