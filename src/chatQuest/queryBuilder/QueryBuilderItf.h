#ifndef CHATQUEST_QUERYBUILDERITF_HEADER
#define CHATQUEST_QUERYBUILDERITF_HEADER

#include "chatQuest/query/QueryItf.h"
#include "chatQuest/response/ResponseAction.h"

struct QueryBuilderItf
{
	void *imp;
	void (*destroy)(struct QueryBuilderItf *itf);

	struct QueryItf *(*build)(struct QueryBuilderItf *itf, enum ResponseAction action);
};

struct QueryBuilderItf *QueryBuilderItf_new(
	void *imp,
	void (*destroy)(struct QueryBuilderItf *itf),

	struct QueryItf *(*build)(struct QueryBuilderItf *itf, enum ResponseAction action)
);

void QueryBuilderItf_destroy(struct QueryBuilderItf *itf);

#endif