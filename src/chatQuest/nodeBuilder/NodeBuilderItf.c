#include "chatQuest/globals/globals.h"
#include "chatQuest/nodeBuilder/NodeBuilderItf.h"
#include "chatQuest/node/NodeItf.h"

struct NodeBuilderItf *NodeBuilderItf_new(
	void *imp,
	void (*destroy)(struct NodeBuilderItf *itf),

	void (*build)(struct NodeBuilderItf *itf, char *nodeId),

	bool (*hasNode)(struct NodeBuilderItf *itf),
	struct NodeItf *(*ownNodeAfterHas)(struct NodeBuilderItf *itf)
)
{
	struct NodeBuilderItf *itf = chatQuest_malloc(sizeof(struct NodeBuilderItf));
	*itf = (struct NodeBuilderItf) {
		.imp = imp,
		.destroy = destroy,

		.build = build,

		.hasNode = hasNode,
		.ownNodeAfterHas = ownNodeAfterHas
	};
	return itf;
}

void NodeBuilderItf_destroy(struct NodeBuilderItf *itf)
{
	chatQuest_free(itf);
}