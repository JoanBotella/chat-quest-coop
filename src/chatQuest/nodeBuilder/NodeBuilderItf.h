#ifndef CHATQUEST_NODEBUILDERITF_HEADER
#define CHATQUEST_NODEBUILDERITF_HEADER

#include "chatQuest/globals/globals.h"
#include "chatQuest/node/NodeItf.h"

struct NodeBuilderItf
{
	void *imp;
	void (*destroy)(struct NodeBuilderItf *itf);

	void (*build)(struct NodeBuilderItf *itf, char *nodeId);

	bool (*hasNode)(struct NodeBuilderItf *itf);
	struct NodeItf *(*ownNodeAfterHas)(struct NodeBuilderItf *itf);
};

struct NodeBuilderItf *NodeBuilderItf_new(
	void *imp,
	void (*destroy)(struct NodeBuilderItf *itf),

	void (*build)(struct NodeBuilderItf *itf, char *nodeId),

	bool (*hasNode)(struct NodeBuilderItf *itf),
	struct NodeItf *(*ownNodeAfterHas)(struct NodeBuilderItf *itf)
);

void NodeBuilderItf_destroy(struct NodeBuilderItf *itf);

#endif