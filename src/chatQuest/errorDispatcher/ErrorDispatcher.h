#ifndef CHATQUEST_ERRORDISPATCHER_HEADER
#define CHATQUEST_ERRORDISPATCHER_HEADER

#include "chatQuest/errorDispatcher/ErrorDispatcherItf.h"

struct ErrorDispatcherSta;

struct ErrorDispatcher
{
	struct ErrorDispatcherSta *sta;
	struct ErrorDispatcherItf *ErrorDispatcherItf;
};

struct ErrorDispatcher *ErrorDispatcher_new();

#endif
