#ifndef CHATQUEST_ERRORDISPATCHERITF_HEADER
#define CHATQUEST_ERRORDISPATCHERITF_HEADER

#include "chatQuest/error/ErrorItf.h"

struct ErrorDispatcherItf
{
	void *imp;
	void (*destroy)(struct ErrorDispatcherItf *itf);

	void (*dispatchAndDestroy)(struct ErrorDispatcherItf *itf, struct ErrorItf *error);
};

struct ErrorDispatcherItf *ErrorDispatcherItf_new(
	void *imp,
	void (*destroy)(struct ErrorDispatcherItf *itf),

	void (*dispatchAndDestroy)(struct ErrorDispatcherItf *itf, struct ErrorItf *error)
);

void ErrorDispatcherItf_destroy(struct ErrorDispatcherItf *itf);

#endif