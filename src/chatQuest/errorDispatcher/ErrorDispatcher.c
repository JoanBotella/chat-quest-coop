#include "chatQuest/globals/globals.h"
#include "chatQuest/errorDispatcher/ErrorDispatcher.h"
#include "chatQuest/errorDispatcher/ErrorDispatcherItf.h"
#include "chatQuest/error/ErrorItf.h"
#include "chatQuest/error/ErrorItf.h"

struct ErrorDispatcherSta
{
};

static struct ErrorDispatcherSta *ErrorDispatcherSta_new()
{
	struct ErrorDispatcherSta *sta = chatQuest_malloc(sizeof(struct ErrorDispatcherSta));
	*sta = (struct ErrorDispatcherSta) {
	};
	return sta;
}

static void ErrorDispatcherSta_destroy(struct ErrorDispatcherSta *sta)
{
	chatQuest_free(sta);
}

static void ErrorDispatcher_destroy(struct ErrorDispatcherItf *itf)
{
	struct ErrorDispatcher *imp = (struct ErrorDispatcher *)itf->imp;

	ErrorDispatcherSta_destroy(imp->sta);
	ErrorDispatcherItf_destroy(imp->ErrorDispatcherItf);
	chatQuest_free(imp);
}

static void ErrorDispatcher_dispatchAndDestroy(struct ErrorDispatcherItf *itf, struct ErrorItf *error)
{
	chatQuest_printError(
		"ERROR [%s:%d(%d)] %s\n",
		error->getFile(error),
		error->getLine(error),
		error->getCode(error),
		error->getMessage(error)
	);
	error->destroy(error);
}

struct ErrorDispatcher *ErrorDispatcher_new()
{
	struct ErrorDispatcher *imp = chatQuest_malloc(sizeof(struct ErrorDispatcher));
	*imp = (struct ErrorDispatcher) {
		.sta = ErrorDispatcherSta_new(),
		.ErrorDispatcherItf = ErrorDispatcherItf_new(
			imp,
			&ErrorDispatcher_destroy,

			&ErrorDispatcher_dispatchAndDestroy
		)
	};
	return imp;
}