#include "chatQuest/globals/globals.h"
#include "chatQuest/errorDispatcher/ErrorDispatcherItf.h"
#include "chatQuest/error/ErrorItf.h"

struct ErrorDispatcherItf *ErrorDispatcherItf_new(
	void *imp,
	void (*destroy)(struct ErrorDispatcherItf *itf),

	void (*dispatchAndDestroy)(struct ErrorDispatcherItf *itf, struct ErrorItf *error)
)
{
	struct ErrorDispatcherItf *itf = chatQuest_malloc(sizeof(struct ErrorDispatcherItf));
	*itf = (struct ErrorDispatcherItf) {
		.imp = imp,
		.destroy = destroy,

		.dispatchAndDestroy = dispatchAndDestroy
	};
	return itf;
}

void ErrorDispatcherItf_destroy(struct ErrorDispatcherItf *itf)
{
	chatQuest_free(itf);
}