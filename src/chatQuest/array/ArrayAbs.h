#ifndef CHATQUEST_ARRAYABS_HEADER
#define CHATQUEST_ARRAYABS_HEADER

#include "chatQuest/globals/globals.h"

struct ArrayAbsSta;

struct ArrayAbs
{
	void *imp;
	struct ArrayAbsSta *sta;
	void (*destroy)(struct ArrayAbs *abs);

	void (*append)(struct ArrayAbs *abs, void *item);
	void (*prepend)(struct ArrayAbs *abs, void *item);
	int (*getLength)(struct ArrayAbs *abs);
	bool (*hasAt)(struct ArrayAbs *abs, int index);
	void *(*getAtAfterHas)(struct ArrayAbs *abs, int index);
	bool (*isEmpty)(struct ArrayAbs *abs);
	void *(*getLastAfterIsNotEmpty)(struct ArrayAbs *abs);
	void (*pop)(struct ArrayAbs *abs);
	void (*shift)(struct ArrayAbs *abs);
	void (*removeAtAfterHas)(struct ArrayAbs *abs, int index);
};

struct ArrayAbs *ArrayAbs_new(
	void *imp,
	void (*destroyItem)(struct ArrayAbs *abs, void *item)
);

#endif
