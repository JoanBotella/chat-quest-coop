#include <glib.h>
#include "chatQuest/globals/globals.h"
#include "chatQuest/array/ArrayAbs.h"

struct ArrayAbsSta
{
	GPtrArray *gPtrArray;
	void (*destroyItem)(struct ArrayAbs *abs, void *item);
};

static struct ArrayAbsSta *ArrayAbsSta_new(
	void (*destroyItem)(struct ArrayAbs *abs, void *item)
)
{
	struct ArrayAbsSta *sta = chatQuest_malloc(sizeof(struct ArrayAbsSta));
	*sta = (struct ArrayAbsSta) {
		.gPtrArray = g_ptr_array_new(),
		.destroyItem = destroyItem
	};
	return sta;
}

static void ArrayAbsSta_destroy(struct ArrayAbsSta *sta)
{
	g_ptr_array_free(sta->gPtrArray, TRUE);
	chatQuest_free(sta);
}

	static void ArrayAbs_destroyItems(struct ArrayAbs *abs)
	{
		struct ArrayAbsSta *sta = abs->sta;
		GPtrArray *gPtrArray = sta->gPtrArray;

		for (int i = 0; i < gPtrArray->len; i++)
		{
			sta->destroyItem(
				abs,
				g_ptr_array_index(gPtrArray, i)
			);
		}
	}

static void ArrayAbs_destroy(struct ArrayAbs *abs)
{
	ArrayAbs_destroyItems(abs);
	ArrayAbsSta_destroy(abs->sta);
	chatQuest_free(abs);
}

static void ArrayAbs_append(struct ArrayAbs *abs, void *item)
{
	g_ptr_array_add(abs->sta->gPtrArray, item);
}

static void ArrayAbs_prepend(struct ArrayAbs *abs, void *item)
{
	GPtrArray *gPtrArray = abs->sta->gPtrArray;
	if (gPtrArray->len == 0)
	{
		g_ptr_array_add(gPtrArray, item);
		return;
	}
	g_ptr_array_insert(gPtrArray, 0, item);
}

static int ArrayAbs_getLength(struct ArrayAbs *abs)
{
	return abs->sta->gPtrArray->len;
}

static bool ArrayAbs_hasAt(struct ArrayAbs *abs, int index)
{
	return
		index < abs->sta->gPtrArray->len
		&& index > -1
	;
}

static void *ArrayAbs_getAtAfterHas(struct ArrayAbs *abs, int index)
{
	return g_ptr_array_index(abs->sta->gPtrArray, index);
}

static bool ArrayAbs_isEmpty(struct ArrayAbs *abs)
{
	return abs->sta->gPtrArray->len == 0;
}

static void *ArrayAbs_getLastAfterIsNotEmpty(struct ArrayAbs *abs)
{
	GPtrArray *gPtrArray = abs->sta->gPtrArray;
	return g_ptr_array_index(gPtrArray, gPtrArray->len - 1);
}

static void ArrayAbs_removeAtAfterHas(struct ArrayAbs *abs, int index)
{
	struct ArrayAbsSta *sta = abs->sta;
	GPtrArray *gPtrArray = sta->gPtrArray;
	void *item = g_ptr_array_index(gPtrArray, index);
	g_ptr_array_remove_index(gPtrArray, index);
	sta->destroyItem(abs, item);
}

static void ArrayAbs_pop(struct ArrayAbs *abs)
{
	int size = abs->sta->gPtrArray->len;
	if (size > 0)
	{
		abs->removeAtAfterHas(abs, size - 1);
	}
}

static void ArrayAbs_shift(struct ArrayAbs *abs)
{
	if (abs->sta->gPtrArray->len > 0)
	{
		abs->removeAtAfterHas(abs, 0);
	}
}

struct ArrayAbs *ArrayAbs_new(
	void *imp,
	void (*destroyItem)(struct ArrayAbs *abs, void *item)
)
{
	struct ArrayAbs *abs = chatQuest_malloc(sizeof(struct ArrayAbs));
	*abs = (struct ArrayAbs) {
		.imp = imp,
		.sta = ArrayAbsSta_new(
			destroyItem
		),
		.destroy = &ArrayAbs_destroy,
		
		.append = &ArrayAbs_append,
		.prepend = &ArrayAbs_prepend,
		.getLength = &ArrayAbs_getLength,
		.hasAt = &ArrayAbs_hasAt,
		.getAtAfterHas = &ArrayAbs_getAtAfterHas,
		.isEmpty = &ArrayAbs_isEmpty,
		.getLastAfterIsNotEmpty = &ArrayAbs_getLastAfterIsNotEmpty,
		.removeAtAfterHas = &ArrayAbs_removeAtAfterHas,
		.pop = &ArrayAbs_pop,
		.shift = &ArrayAbs_shift
	};
	return abs;
}
