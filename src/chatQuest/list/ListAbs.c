#include "chatQuest/globals/globals.h"
#include "chatQuest/list/ListAbs.h"

struct ListNode
{
	void *item;
	struct ListNode *next;
};

static struct ListNode *ListNode_new(
	void *item
)
{
	struct ListNode *node = chatQuest_malloc(sizeof(struct ListNode));
	*node = (struct ListNode) {
		.item = item,
		.next = NULL
	};
	return node;
}

static void ListNode_destroy(struct ListNode *node)
{
	chatQuest_free(node);
}

struct ListAbsSta
{
	struct ListNode *firstNode;
	struct ListNode *lastNode;
	struct ListNode *pointedNode;
	void (*destroyItem)(struct ListAbs *abs, void *item);
};

static struct ListAbsSta *ListAbsSta_new(
	void (*destroyItem)(struct ListAbs *abs, void *item)
)
{
	struct ListAbsSta *sta = chatQuest_malloc(sizeof(struct ListAbsSta));
	*sta = (struct ListAbsSta) {
		.firstNode = NULL,
		.lastNode = NULL,
		.pointedNode = NULL,
		.destroyItem = destroyItem
	};
	sta->pointedNode = sta->firstNode;
	return sta;
}

static void ListAbsSta_destroy(struct ListAbsSta *sta)
{
	chatQuest_free(sta);
}

static void ListAbs_destroyNodes(struct ListAbs *abs)
{
	struct ListAbsSta *sta = abs->sta;
	struct ListNode *node = sta->firstNode;
	struct ListNode *next;
	while (node != NULL)
	{
		next = node->next;
		sta->destroyItem(abs, node->item);
		ListNode_destroy(node);
		node = next;
	}
}

static void ListAbs_destroy(struct ListAbs *abs)
{
	ListAbs_destroyNodes(abs);
	ListAbsSta_destroy(abs->sta);
	chatQuest_free(abs);
}

static void ListAbs_add(struct ListAbs *abs, void *item)
{
	struct ListAbsSta *sta = abs->sta;
	struct ListNode *node = ListNode_new(item);

	if (sta->lastNode == NULL)
	{
		sta->firstNode = node;
		sta->lastNode = node;
		sta->pointedNode = node;
		return;
	}

	sta->lastNode->next = node;
	sta->lastNode = node;
}

static void ListAbs_rewindPointer(struct ListAbs *abs)
{
	struct ListAbsSta *sta = abs->sta;
	sta->pointedNode = sta->firstNode;
}

static bool ListAbs_pointedExists(struct ListAbs *abs)
{
	struct ListAbsSta *sta = abs->sta;
	return sta->pointedNode != NULL;
}

static void *ListAbs_getPointedAfterExists(struct ListAbs *abs)
{
	struct ListAbsSta *sta = abs->sta;
	return sta->pointedNode->item;
}

static void ListAbs_pointNext(struct ListAbs *abs)
{
	struct ListAbsSta *sta = abs->sta;
	if (sta->pointedNode != NULL)
	{
		sta->pointedNode = sta->pointedNode->next;
	}
}

struct ListAbs *ListAbs_new(
	void *imp,
	void (*destroyItem)(struct ListAbs *abs, void *item)
)
{
	struct ListAbs *abs = chatQuest_malloc(sizeof(struct ListAbs));
	*abs = (struct ListAbs) {
		.imp = imp,
		.sta = ListAbsSta_new(
			destroyItem
		),
		.destroy = &ListAbs_destroy,

		.add = &ListAbs_add,
		.rewindPointer = &ListAbs_rewindPointer,
		.pointedExists = &ListAbs_pointedExists,
		.getPointedAfterExists = &ListAbs_getPointedAfterExists,
		.pointNext = &ListAbs_pointNext
	};
	return abs;
}