#ifndef CHATQUEST_SIMPLELISTABS_HEADER
#define CHATQUEST_SIMPLELISTABS_HEADER

#include "chatQuest/globals/globals.h"

struct ListAbsSta;

struct ListAbs
{
	void *imp;
	struct ListAbsSta *sta;
	void (*destroy)(struct ListAbs *abs);

	void (*add)(struct ListAbs *abs, void *item);
	void (*rewindPointer)(struct ListAbs *abs);
	bool (*pointedExists)(struct ListAbs *abs);
	void *(*getPointedAfterExists)(struct ListAbs *abs);
	void (*pointNext)(struct ListAbs *abs);
};

struct ListAbs *ListAbs_new(
	void *imp,
	void (*destroyItem)(struct ListAbs *abs, void *item)
);


#endif