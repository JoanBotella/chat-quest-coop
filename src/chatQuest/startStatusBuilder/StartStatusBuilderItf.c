#include "chatQuest/globals/globals.h"
#include "chatQuest/startStatusBuilder/StartStatusBuilderItf.h"
#include "chatQuest/status/StatusItf.h"

struct StartStatusBuilderItf *StartStatusBuilderItf_new(
	void *imp,
	void (*destroy)(struct StartStatusBuilderItf *itf),

	struct StatusItf *(*build)(struct StartStatusBuilderItf *itf)
)
{
	struct StartStatusBuilderItf *itf = chatQuest_malloc(sizeof(struct StartStatusBuilderItf));
	*itf = (struct StartStatusBuilderItf) {
		.imp = imp,
		.destroy = destroy,

		.build = build
	};
	return itf;
}

void StartStatusBuilderItf_destroy(struct StartStatusBuilderItf *itf)
{
	chatQuest_free(itf);
}