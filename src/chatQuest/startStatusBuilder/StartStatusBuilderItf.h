#ifndef CHATQUEST_STARTSTATUSBUILDERITF_HEADER
#define CHATQUEST_STARTSTATUSBUILDERITF_HEADER

#include "chatQuest/status/StatusItf.h"

struct StartStatusBuilderItf
{
	void *imp;
	void (*destroy)(struct StartStatusBuilderItf *itf);

	struct StatusItf *(*build)(struct StartStatusBuilderItf *itf);
};

struct StartStatusBuilderItf *StartStatusBuilderItf_new(
	void *imp,
	void (*destroy)(struct StartStatusBuilderItf *itf),

	struct StatusItf *(*build)(struct StartStatusBuilderItf *itf)
);

void StartStatusBuilderItf_destroy(struct StartStatusBuilderItf *itf);

#endif