#ifndef CHATQUEST_STATEMACHINE_HEADER
#define CHATQUEST_STATEMACHINE_HEADER

#include "chatQuest/stateMachine/StateMachineItf.h"
#include "chatQuest/nodeBuilder/NodeBuilderItf.h"
#include "chatQuest/errorDispatcher/ErrorDispatcherItf.h"

struct StateMachineSta;

struct StateMachine
{
	struct StateMachineSta *sta;
	struct StateMachineItf *StateMachineItf;
};

struct StateMachine *StateMachine_new(
	struct NodeBuilderItf *nodeBuilder,
	struct ErrorDispatcherItf *errorDispatcher
);

#endif