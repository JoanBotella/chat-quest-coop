#ifndef CHATQUEST_STATEMACHINEERROR_HEADER
#define CHATQUEST_STATEMACHINEERROR_HEADER

enum NodeError
{
	CHATQUEST_STATEMACHINEERROR_NODE_NOT_BUILT = 0,
	CHATQUEST_STATEMACHINEERROR_NODE_RESPONSE_NOT_BUILT = 1,
	CHATQUEST_STATEMACHINEERROR_EMPTY_NODE_IDS = 2
};

#endif