#include "chatQuest/globals/globals.h"
#include "chatQuest/stateMachine/StateMachine.h"
#include "chatQuest/stateMachine/StateMachineItf.h"
#include "chatQuest/request/RequestItf.h"
#include "chatQuest/response/ResponseItf.h"
#include "chatQuest/status/StatusItf.h"
#include "chatQuest/nodeBuilder/NodeBuilderItf.h"
#include "chatQuest/node/NodeItf.h"
#include "chatQuest/errorDispatcher/ErrorDispatcherItf.h"
#include "chatQuest/stateMachine/StateMachineError.h"
#include "chatQuest/error/Error.h"
#include "chatQuest/error/ErrorItf.h"
#include "chatQuest/stringArray/StringArrayItf.h"

struct StateMachineSta
{
	struct NodeBuilderItf *nodeBuilder;
	struct ErrorDispatcherItf *errorDispatcher;

	struct NodeItf *node;
	struct ResponseItf *response;
};

static struct StateMachineSta *StateMachineSta_new(
	struct NodeBuilderItf *nodeBuilder,
	struct ErrorDispatcherItf *errorDispatcher
)
{
	struct StateMachineSta *sta = chatQuest_malloc(sizeof(struct StateMachineSta));
	*sta = (struct StateMachineSta) {
		.nodeBuilder = nodeBuilder,
		.errorDispatcher = errorDispatcher,

		.node = NULL,
		.response = NULL
	};
	return sta;
}

static void StateMachineSta_destroy(struct StateMachineSta *sta)
{
	sta->nodeBuilder->destroy(sta->nodeBuilder);
	sta->errorDispatcher->destroy(sta->errorDispatcher);

	if (sta->node != NULL)
	{
		sta->node->destroy(sta->node);
	}
	if (sta->response != NULL)
	{
		sta->response->destroy(sta->response);
	}

	chatQuest_free(sta);
}

static void StateMachine_destroy(struct StateMachineItf *itf)
{
	struct StateMachine *imp = (struct StateMachine *)itf->imp;

	StateMachineSta_destroy(imp->sta);
	StateMachineItf_destroy(imp->StateMachineItf);
	chatQuest_free(imp);
}

	static void StateMachine_reset(struct StateMachine *imp)
	{
		struct StateMachineSta *sta = imp->sta;

		if (sta->node != NULL)
		{
			sta->node->destroy(sta->node);
			sta->node = NULL;
		}
		if (sta->response != NULL)
		{
			sta->response->destroy(sta->response);
			sta->response = NULL;
		}
	}

	static void StateMachine_setupNode(struct StateMachine *imp, struct RequestItf *request)
	{
		struct StateMachineSta *sta = imp->sta;

		struct StatusItf *status = request->getStatus(request);
		struct StringArrayItf *nodeIds = status->getNodeIds(status);

		if (nodeIds->isEmpty(nodeIds))
		{
			struct ErrorItf *error = Error_new(
				CHATQUEST_STATEMACHINEERROR_EMPTY_NODE_IDS,
				__FILE__,
				__LINE__,
				"The NodeIds is empty."
			)->ErrorItf;
			sta->errorDispatcher->dispatchAndDestroy(sta->errorDispatcher, error);
			return;
		}
		char *nodeId = nodeIds->getLastAfterIsNotEmpty(nodeIds);

		struct NodeBuilderItf *nodeBuilder = sta->nodeBuilder;
		nodeBuilder->build(nodeBuilder, nodeId);
		if (nodeBuilder->hasNode(nodeBuilder))
		{
			sta->node = nodeBuilder->ownNodeAfterHas(nodeBuilder);
			return;
		}

		struct ErrorItf *error = Error_new(
			CHATQUEST_STATEMACHINEERROR_NODE_NOT_BUILT,
			__FILE__,
			__LINE__,
			"The Node could not be built."
		)->ErrorItf;
		sta->errorDispatcher->dispatchAndDestroy(sta->errorDispatcher, error);
	}

	static void StateMachine_runNode(struct StateMachine *imp, struct RequestItf *request)
	{
		struct StateMachineSta *sta = imp->sta;

		struct NodeItf *node = sta->node;

		node->run(node, request);
		if (node->hasResponse(node))
		{
			sta->response = node->ownResponseAfterHas(node);
		}

		node->destroy(node);
		sta->node = NULL;

		if (sta->response == NULL)
		{
			struct ErrorItf *error = Error_new(
				CHATQUEST_STATEMACHINEERROR_NODE_RESPONSE_NOT_BUILT,
				__FILE__,
				__LINE__,
				"The Node did not build a Response."
			)->ErrorItf;
			sta->errorDispatcher->dispatchAndDestroy(sta->errorDispatcher, error);
		}
	}

static void StateMachine_run(struct StateMachineItf *itf, struct RequestItf *request)
{
	struct StateMachine *imp = (struct StateMachine *)itf->imp;
	struct StateMachineSta *sta = imp->sta;
	
	StateMachine_reset(imp);

	StateMachine_setupNode(imp, request);

	if (sta->node == NULL)
	{
		return;
	}

	StateMachine_runNode(imp, request);
}

static bool StateMachine_hasResponse(struct StateMachineItf *itf)
{
	struct StateMachineSta *sta = ((struct StateMachine *)itf->imp)->sta;
	return sta->response != NULL;
}

static struct ResponseItf *StateMachine_ownResponseAfterHas(struct StateMachineItf *itf)
{
	struct StateMachineSta *sta = ((struct StateMachine *)itf->imp)->sta;
	struct ResponseItf *r = sta->response;
	sta->response = NULL;
	return r;
}

struct StateMachine *StateMachine_new(
	struct NodeBuilderItf *nodeBuilder,
	struct ErrorDispatcherItf *errorDispatcher
)
{
	struct StateMachine *imp = chatQuest_malloc(sizeof(struct StateMachine));
	*imp = (struct StateMachine) {
		.sta = StateMachineSta_new(
			nodeBuilder,
			errorDispatcher
		),
		.StateMachineItf = StateMachineItf_new(
			imp,
			&StateMachine_destroy,

			&StateMachine_run,

			&StateMachine_hasResponse,
			&StateMachine_ownResponseAfterHas
		)
	};
	return imp;
}