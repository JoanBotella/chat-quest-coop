#include "chatQuest/globals/globals.h"
#include "chatQuest/stateMachine/StateMachineItf.h"
#include "chatQuest/response/ResponseItf.h"
#include "chatQuest/request/RequestItf.h"

struct StateMachineItf *StateMachineItf_new(
	void *imp,
	void (*destroy)(struct StateMachineItf *itf),

	void (*run)(struct StateMachineItf *itf, struct RequestItf *request),

	bool (*hasResponse)(struct StateMachineItf *itf),
	struct ResponseItf *(*ownResponseAfterHas)(struct StateMachineItf *itf)
)
{
	struct StateMachineItf *itf = chatQuest_malloc(sizeof(struct StateMachineItf));
	*itf = (struct StateMachineItf) {
		.imp = imp,
		.destroy = destroy,

		.run = run,

		.hasResponse = hasResponse,
		.ownResponseAfterHas = ownResponseAfterHas
	};
	return itf;
}

void StateMachineItf_destroy(struct StateMachineItf *itf)
{
	chatQuest_free(itf);
}