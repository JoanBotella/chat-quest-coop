#ifndef CHATQUEST_STATEMACHINEITF_HEADER
#define CHATQUEST_STATEMACHINEITF_HEADER

#include "chatQuest/globals/globals.h"
#include "chatQuest/response/ResponseItf.h"
#include "chatQuest/request/RequestItf.h"

struct StateMachineItf
{
	void *imp;
	void (*destroy)(struct StateMachineItf *itf);

	void (*run)(struct StateMachineItf *itf, struct RequestItf *request);

	bool (*hasResponse)(struct StateMachineItf *itf);
	struct ResponseItf *(*ownResponseAfterHas)(struct StateMachineItf *itf);
};

struct StateMachineItf *StateMachineItf_new(
	void *imp,
	void (*destroy)(struct StateMachineItf *itf),

	void (*run)(struct StateMachineItf *itf, struct RequestItf *request),

	bool (*hasResponse)(struct StateMachineItf *itf),
	struct ResponseItf *(*ownResponseAfterHas)(struct StateMachineItf *itf)
);

void StateMachineItf_destroy(struct StateMachineItf *itf);

#endif