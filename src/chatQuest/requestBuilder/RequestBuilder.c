#include "chatQuest/globals/globals.h"
#include "chatQuest/requestBuilder/RequestBuilder.h"
#include "chatQuest/startStatusBuilder/StartStatusBuilderItf.h"
#include "chatQuest/queryBuilder/QueryBuilderItf.h"
#include "chatQuest/status/StatusItf.h"
#include "chatQuest/response/ResponseAction.h"
#include "chatQuest/request/Request.h"
#include "chatQuest/request/RequestItf.h"
#include "chatQuest/query/QueryItf.h"

struct RequestBuilderSta
{
	struct StartStatusBuilderItf *startStatusBuilder;
	struct QueryBuilderItf *queryBuilder;
};

static struct RequestBuilderSta *RequestBuilderSta_new(
	struct StartStatusBuilderItf *startStatusBuilder,
	struct QueryBuilderItf *queryBuilder
)
{
	struct RequestBuilderSta *sta = chatQuest_malloc(sizeof(struct RequestBuilderSta));
	*sta = (struct RequestBuilderSta) {
		.startStatusBuilder = startStatusBuilder,
		.queryBuilder = queryBuilder
	};
	return sta;
}

static void RequestBuilderSta_destroy(struct RequestBuilderSta *sta)
{
	sta->startStatusBuilder->destroy(sta->startStatusBuilder);
	sta->queryBuilder->destroy(sta->queryBuilder);
	chatQuest_free(sta);
}

static void RequestBuilder_destroy(struct RequestBuilderItf *itf)
{
	struct RequestBuilder *imp = (struct RequestBuilder *)itf->imp;

	RequestBuilderSta_destroy(imp->sta);
	RequestBuilderItf_destroy(imp->RequestBuilderItf);
	chatQuest_free(imp);
}

		static struct QueryItf *RequestBuilder_buildQuery(struct RequestBuilder *imp, enum ResponseAction action)
		{
			struct QueryBuilderItf *queryBuilder = imp->sta->queryBuilder;
			return queryBuilder->build(queryBuilder, action);
		}

	static struct RequestItf *RequestBuilder_buildByStatusAndAction(struct RequestBuilder *imp, struct StatusItf *status, enum ResponseAction action)
	{
		return Request_new(
			RequestBuilder_buildQuery(imp, action),
			status
		)->RequestItf;
	}

static struct RequestItf *RequestBuilder_buildFirstRequest(struct RequestBuilderItf *itf)
{
	struct RequestBuilder *imp = (struct RequestBuilder *)itf->imp;
	struct RequestBuilderSta *sta = imp->sta;

	return RequestBuilder_buildByStatusAndAction(
		imp,
		sta->startStatusBuilder->build(sta->startStatusBuilder),
		CHATQUEST_RESPONSEACTION_START
	);
}

static struct RequestItf *RequestBuilder_buildByLastResponse(struct RequestBuilderItf *itf, struct ResponseItf *response)
{
	struct RequestBuilder *imp = (struct RequestBuilder *)itf->imp;

	return RequestBuilder_buildByStatusAndAction(
		imp,
		response->getStatus(response),
		response->getNextAction(response)
	);
}

struct RequestBuilder *RequestBuilder_new(
	struct StartStatusBuilderItf *startStatusBuilder,
	struct QueryBuilderItf *queryBuilder
)
{
	struct RequestBuilder *imp = chatQuest_malloc(sizeof(struct RequestBuilder));
	*imp = (struct RequestBuilder) {
		.sta = RequestBuilderSta_new(
			startStatusBuilder,
			queryBuilder
		),
		.RequestBuilderItf = RequestBuilderItf_new(
			imp,
			&RequestBuilder_destroy,

			&RequestBuilder_buildFirstRequest,
			&RequestBuilder_buildByLastResponse
		)
	};
	return imp;
}