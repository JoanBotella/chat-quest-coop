#ifndef CHATQUEST_REQUESTBUILDERITF_HEADER
#define CHATQUEST_REQUESTBUILDERITF_HEADER

#include "chatQuest/requestBuilder/RequestBuilderItf.h"
#include "chatQuest/request/RequestItf.h"
#include "chatQuest/response/ResponseItf.h"

struct RequestBuilderItf
{
	void *imp;
	void (*destroy)(struct RequestBuilderItf *itf);

	struct RequestItf *(*buildFirstRequest)(struct RequestBuilderItf *itf);
	struct RequestItf *(*buildByLastResponse)(struct RequestBuilderItf *itf, struct ResponseItf *response);
};

struct RequestBuilderItf *RequestBuilderItf_new(
	void *imp,
	void (*destroy)(struct RequestBuilderItf *itf),

	struct RequestItf *(*buildFirstRequest)(struct RequestBuilderItf *itf),
	struct RequestItf *(*buildByLastResponse)(struct RequestBuilderItf *itf, struct ResponseItf *response)
);

void RequestBuilderItf_destroy(struct RequestBuilderItf *itf);

#endif