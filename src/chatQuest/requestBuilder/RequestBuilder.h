#ifndef CHATQUEST_REQUESTBUILDER_HEADER
#define CHATQUEST_REQUESTBUILDER_HEADER

#include "chatQuest/requestBuilder/RequestBuilderItf.h"
#include "chatQuest/startStatusBuilder/StartStatusBuilderItf.h"
#include "chatQuest/queryBuilder/QueryBuilderItf.h"

struct RequestBuilderSta;

struct RequestBuilder
{
	struct RequestBuilderSta *sta;
	struct RequestBuilderItf *RequestBuilderItf;
};

struct RequestBuilder *RequestBuilder_new(
	struct StartStatusBuilderItf *startStatusBuilder,
	struct QueryBuilderItf *queryBuilder
);

#endif
