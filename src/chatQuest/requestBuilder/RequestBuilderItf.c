#include "chatQuest/globals/globals.h"
#include "chatQuest/requestBuilder/RequestBuilderItf.h"
#include "chatQuest/request/RequestItf.h"
#include "chatQuest/response/ResponseItf.h"

struct RequestBuilderItf *RequestBuilderItf_new(
	void *imp,
	void (*destroy)(struct RequestBuilderItf *itf),

	struct RequestItf *(*buildFirstRequest)(struct RequestBuilderItf *itf),
	struct RequestItf *(*buildByLastResponse)(struct RequestBuilderItf *itf, struct ResponseItf *response)
)
{
	struct RequestBuilderItf *itf = chatQuest_malloc(sizeof(struct RequestBuilderItf));
	*itf = (struct RequestBuilderItf) {
		.imp = imp,
		.destroy = destroy,

		.buildFirstRequest = buildFirstRequest,
		.buildByLastResponse = buildByLastResponse
	};
	return itf;
}

void RequestBuilderItf_destroy(struct RequestBuilderItf *itf)
{
	chatQuest_free(itf);
}