#ifndef CHATQUEST_REQUESTITF_HEADER
#define CHATQUEST_REQUESTITF_HEADER

#include "chatQuest/globals/globals.h"
#include "chatQuest/status/StatusItf.h"
#include "chatQuest/query/QueryItf.h"

struct RequestItf
{
	void *imp;
	void (*destroy)(struct RequestItf *itf);

	struct QueryItf *(*getQuery)(struct RequestItf *itf);

	struct StatusItf *(*getStatus)(struct RequestItf *itf);
};

struct RequestItf *RequestItf_new(
	void *imp,
	void (*destroy)(struct RequestItf *itf),	// Does not destroy the Status

	struct QueryItf *(*getQuery)(struct RequestItf *itf),

	struct StatusItf *(*getStatus)(struct RequestItf *itf)
);

void RequestItf_destroy(struct RequestItf *itf);

#endif