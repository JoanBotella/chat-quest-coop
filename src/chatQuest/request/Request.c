#include "chatQuest/globals/globals.h"
#include "chatQuest/request/Request.h"
#include "chatQuest/request/RequestItf.h"
#include "chatQuest/status/StatusItf.h"
#include "chatQuest/query/QueryItf.h"

struct RequestSta
{
	struct QueryItf *query;
	struct StatusItf *status;
};

static struct RequestSta *RequestSta_new(
	struct QueryItf *query,
	struct StatusItf *status
)
{
	struct RequestSta *sta = chatQuest_malloc(sizeof(struct RequestSta));
	*sta = (struct RequestSta) {
		.query = query,
		.status = status
	};
	return sta;
}

static void RequestSta_destroy(struct RequestSta *sta)
{
	sta->query->destroy(sta->query);
	// Do not destroy the Status
	chatQuest_free(sta);
}

static void Request_destroy(struct RequestItf *itf)
{
	struct Request *imp = (struct Request *)itf->imp;

	RequestSta_destroy(imp->sta);
	RequestItf_destroy(imp->RequestItf);
	chatQuest_free(imp);
}

static struct QueryItf *Request_getQuery(struct RequestItf *itf)
{
	struct RequestSta *sta = ((struct Request *)itf->imp)->sta;
	return sta->query;
}

static struct StatusItf *Request_getStatus(struct RequestItf *itf)
{
	struct RequestSta *sta = ((struct Request *)itf->imp)->sta;
	return sta->status;
}

struct Request *Request_new(
	struct QueryItf *query,
	struct StatusItf *status
)
{
	struct Request *imp = chatQuest_malloc(sizeof(struct Request));
	*imp = (struct Request) {
		.sta = RequestSta_new(
			query,
			status
		),
		.RequestItf = RequestItf_new(
			imp,
			&Request_destroy,

			&Request_getQuery,

			&Request_getStatus
		)
	};
	return imp;
}