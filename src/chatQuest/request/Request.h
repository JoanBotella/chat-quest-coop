#ifndef CHATQUEST_REQUEST_HEADER
#define CHATQUEST_REQUEST_HEADER

#include "chatQuest/request/RequestItf.h"
#include "chatQuest/status/StatusItf.h"
#include "chatQuest/query/QueryItf.h"

struct RequestSta;

struct Request
{
	struct RequestSta *sta;
	struct RequestItf *RequestItf;
};

struct Request *Request_new(
	struct QueryItf *query,
	struct StatusItf *status
);

#endif
