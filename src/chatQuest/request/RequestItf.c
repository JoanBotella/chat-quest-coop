#include "chatQuest/globals/globals.h"
#include "chatQuest/request/RequestItf.h"
#include "chatQuest/status/StatusItf.h"
#include "chatQuest/query/QueryItf.h"

struct RequestItf *RequestItf_new(
	void *imp,
	void (*destroy)(struct RequestItf *itf),

	struct QueryItf *(*getQuery)(struct RequestItf *itf),

	struct StatusItf *(*getStatus)(struct RequestItf *itf)
)
{
	struct RequestItf *itf = chatQuest_malloc(sizeof(struct RequestItf));
	*itf = (struct RequestItf) {
		.imp = imp,
		.destroy = destroy,

		.getQuery = getQuery,

		.getStatus = getStatus
	};
	return itf;
}

void RequestItf_destroy(struct RequestItf *itf)
{
	chatQuest_free(itf);
}