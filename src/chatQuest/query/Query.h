#ifndef CHATQUEST_QUERY_HEADER
#define CHATQUEST_QUERY_HEADER

#include "chatQuest/query/QueryItf.h"

struct QuerySta;

struct Query
{
	struct QuerySta *sta;
	struct QueryItf *QueryItf;
};

struct Query *Query_new();

#endif
