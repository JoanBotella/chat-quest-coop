#include "chatQuest/globals/globals.h"
#include "chatQuest/query/QueryItf.h"

struct QueryItf *QueryItf_new(
	void *imp,
	void (*destroy)(struct QueryItf *itf),

	bool (*hasVerb)(struct QueryItf *itf),
	char *(*getVerbAfterHas)(struct QueryItf *itf),
	void (*setVerb)(struct QueryItf *itf, char *verb),
	void (*unsetVerb)(struct QueryItf *itf),

	bool (*hasDirect)(struct QueryItf *itf),
	char *(*getDirectAfterHas)(struct QueryItf *itf),
	void (*setDirect)(struct QueryItf *itf, char* direct),
	void (*unsetDirect)(struct QueryItf *itf),

	bool (*hasIndirect)(struct QueryItf *itf),
	char *(*getIndirectAfterHas)(struct QueryItf *itf),
	void (*setIndirect)(struct QueryItf *itf, char* indirect),
	void (*unsetIndirect)(struct QueryItf *itf)
)
{
	struct QueryItf *itf = chatQuest_malloc(sizeof(struct QueryItf));
	*itf = (struct QueryItf) {
		.imp = imp,
		.destroy = destroy,

		.hasVerb = hasVerb,
		.getVerbAfterHas = getVerbAfterHas,
		.setVerb = setVerb,
		.unsetVerb = unsetVerb,

		.hasDirect = hasDirect,
		.getDirectAfterHas = getDirectAfterHas,
		.setDirect = setDirect,
		.unsetDirect = unsetDirect,

		.hasIndirect = hasIndirect,
		.getIndirectAfterHas = getIndirectAfterHas,
		.setIndirect = setIndirect,
		.unsetIndirect = unsetIndirect
	};
	return itf;
}

void QueryItf_destroy(struct QueryItf *itf)
{
	chatQuest_free(itf);
}