#ifndef CHATQUEST_QUERYITF_HEADER
#define CHATQUEST_QUERYITF_HEADER

#include "chatQuest/globals/globals.h"

struct QueryItf
{
	void *imp;
	void (*destroy)(struct QueryItf *itf);

	bool (*hasVerb)(struct QueryItf *itf);
	char *(*getVerbAfterHas)(struct QueryItf *itf);
	void (*setVerb)(struct QueryItf *itf, char *verb);
	void (*unsetVerb)(struct QueryItf *itf);

	bool (*hasDirect)(struct QueryItf *itf);
	char *(*getDirectAfterHas)(struct QueryItf *itf);
	void (*setDirect)(struct QueryItf *itf, char* direct);
	void (*unsetDirect)(struct QueryItf *itf);

	bool (*hasIndirect)(struct QueryItf *itf);
	char *(*getIndirectAfterHas)(struct QueryItf *itf);
	void (*setIndirect)(struct QueryItf *itf, char* indirect);
	void (*unsetIndirect)(struct QueryItf *itf);
};

struct QueryItf *QueryItf_new(
	void *imp,
	void (*destroy)(struct QueryItf *itf),

	bool (*hasVerb)(struct QueryItf *itf),
	char *(*getVerbAfterHas)(struct QueryItf *itf),
	void (*setVerb)(struct QueryItf *itf, char *verb),
	void (*unsetVerb)(struct QueryItf *itf),

	bool (*hasDirect)(struct QueryItf *itf),
	char *(*getDirectAfterHas)(struct QueryItf *itf),
	void (*setDirect)(struct QueryItf *itf, char* direct),
	void (*unsetDirect)(struct QueryItf *itf),

	bool (*hasIndirect)(struct QueryItf *itf),
	char *(*getIndirectAfterHas)(struct QueryItf *itf),
	void (*setIndirect)(struct QueryItf *itf, char* indirect),
	void (*unsetIndirect)(struct QueryItf *itf)
);

void QueryItf_destroy(struct QueryItf *itf);

#endif