#include "chatQuest/globals/globals.h"
#include "chatQuest/query/Query.h"
#include "chatQuest/query/QueryItf.h"

struct QuerySta
{
	char *verb;
	char *direct;
	char *indirect;
};

static struct QuerySta *QuerySta_new()
{
	struct QuerySta *sta = chatQuest_malloc(sizeof(struct QuerySta));
	*sta = (struct QuerySta) {
		.verb = NULL,
		.direct = NULL,
		.indirect = NULL
	};
	return sta;
}

static void QuerySta_destroy(struct QuerySta *sta)
{
	chatQuest_free(sta->verb);
	chatQuest_free(sta->direct);
	chatQuest_free(sta->indirect);
	chatQuest_free(sta);
}

static void Query_destroy(struct QueryItf *itf)
{
	struct Query *imp = (struct Query *)itf->imp;

	QuerySta_destroy(imp->sta);
	QueryItf_destroy(imp->QueryItf);
	chatQuest_free(imp);
}

static bool Query_hasVerb(struct QueryItf *itf)
{
	struct QuerySta *sta = ((struct Query *)itf->imp)->sta;
	return sta->verb != NULL;
}

static char *Query_getVerbAfterHas(struct QueryItf *itf)
{
	struct QuerySta *sta = ((struct Query *)itf->imp)->sta;
	return sta->verb;
}

static void Query_setVerb(struct QueryItf *itf, char *verb)
{
	struct QuerySta *sta = ((struct Query *)itf->imp)->sta;
	chatQuest_free(sta->verb);
	sta->verb = chatQuest_copyString(verb);
}

static void Query_unsetVerb(struct QueryItf *itf)
{
	struct QuerySta *sta = ((struct Query *)itf->imp)->sta;
	chatQuest_free(sta->verb);
	sta->verb = NULL;
}

static bool Query_hasDirect(struct QueryItf *itf)
{
	struct QuerySta *sta = ((struct Query *)itf->imp)->sta;
	return sta->direct != NULL;
}

static char *Query_getDirectAfterHas(struct QueryItf *itf)
{
	struct QuerySta *sta = ((struct Query *)itf->imp)->sta;
	return sta->direct;
}

static void Query_setDirect(struct QueryItf *itf, char* direct)
{
	struct QuerySta *sta = ((struct Query *)itf->imp)->sta;
	chatQuest_free(sta->direct);
	sta->direct = chatQuest_copyString(direct);
}

static void Query_unsetDirect(struct QueryItf *itf)
{
	struct QuerySta *sta = ((struct Query *)itf->imp)->sta;
	chatQuest_free(sta->direct);
	sta->direct = NULL;
}

static bool Query_hasIndirect(struct QueryItf *itf)
{
	struct QuerySta *sta = ((struct Query *)itf->imp)->sta;
	return sta->indirect != NULL;
}

static char *Query_getIndirectAfterHas(struct QueryItf *itf)
{
	struct QuerySta *sta = ((struct Query *)itf->imp)->sta;
	return sta->indirect;
}

static void Query_setIndirect(struct QueryItf *itf, char* indirect)
{
	struct QuerySta *sta = ((struct Query *)itf->imp)->sta;
	chatQuest_free(sta->indirect);
	sta->direct = chatQuest_copyString(indirect);
}

static void Query_unsetIndirect(struct QueryItf *itf)
{
	struct QuerySta *sta = ((struct Query *)itf->imp)->sta;
	chatQuest_free(sta->indirect);
	sta->indirect = NULL;
}

struct Query *Query_new()
{
	struct Query *imp = chatQuest_malloc(sizeof(struct Query));
	*imp = (struct Query) {
		.sta = QuerySta_new(
		),
		.QueryItf = QueryItf_new(
			imp,
			&Query_destroy,

			&Query_hasVerb,
			&Query_getVerbAfterHas,
			&Query_setVerb,
			&Query_unsetVerb,

			&Query_hasDirect,
			&Query_getDirectAfterHas,
			&Query_setDirect,
			&Query_unsetDirect,

			&Query_hasIndirect,
			&Query_getIndirectAfterHas,
			&Query_setIndirect,
			&Query_unsetIndirect
		)
	};
	return imp;
}