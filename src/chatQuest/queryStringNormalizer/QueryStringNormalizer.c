#include "chatQuest/globals/globals.h"
#include "chatQuest/queryStringNormalizer/QueryStringNormalizer.h"
#include "chatQuest/queryStringNormalizer/QueryStringNormalizerItf.h"

struct QueryStringNormalizerSta
{
};

static struct QueryStringNormalizerSta *QueryStringNormalizerSta_new()
{
	struct QueryStringNormalizerSta *sta = chatQuest_malloc(sizeof(struct QueryStringNormalizerSta));
	*sta = (struct QueryStringNormalizerSta) {
	};
	return sta;
}

static void QueryStringNormalizerSta_destroy(struct QueryStringNormalizerSta *sta)
{
	chatQuest_free(sta);
}

static void QueryStringNormalizer_destroy(struct QueryStringNormalizerItf *itf)
{
	struct QueryStringNormalizer *imp = (struct QueryStringNormalizer *)itf->imp;

	QueryStringNormalizerSta_destroy(imp->sta);
	QueryStringNormalizerItf_destroy(imp->QueryStringNormalizerItf);
	chatQuest_free(imp);
}

static char *QueryStringNormalizer_normalize(struct QueryStringNormalizerItf *itf, char *raw)
{
	char *normalized = chatQuest_copyString(raw);
	
	chatQuest_stringRightTrimInPlace(&normalized);
	chatQuest_stringLeftTrimInPlace(&normalized);
	chatQuest_replaceSubstringInPlace(&normalized, "  ", " ", 0);
	chatQuest_replaceSubstringInPlace(&normalized, "\t", " ", 0);
	chatQuest_stringToLowerCaseInPlace(&normalized);
	chatQuest_replaceSubstringInPlace(&normalized, "go to", "go", 0);

	return normalized;
}

struct QueryStringNormalizer *QueryStringNormalizer_new()
{
	struct QueryStringNormalizer *imp = chatQuest_malloc(sizeof(struct QueryStringNormalizer));
	*imp = (struct QueryStringNormalizer) {
		.sta = QueryStringNormalizerSta_new(),
		.QueryStringNormalizerItf = QueryStringNormalizerItf_new(
			imp,
			&QueryStringNormalizer_destroy,

			&QueryStringNormalizer_normalize
		)
	};
	return imp;
}