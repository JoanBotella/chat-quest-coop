#ifndef QUERYSTRINGNORMALIZERITF_HEADER
#define QUERYSTRINGNORMALIZERITF_HEADER

struct QueryStringNormalizerItf
{
	void *imp;
	void (*destroy)(struct QueryStringNormalizerItf *itf);

	char *(*normalize)(struct QueryStringNormalizerItf *itf, char* raw);
};

struct QueryStringNormalizerItf *QueryStringNormalizerItf_new(
	void *imp,
	void (*destroy)(struct QueryStringNormalizerItf *itf),

	char *(*normalize)(struct QueryStringNormalizerItf *itf, char* raw)	// chatQuest_mallocs the returned char*
);

void QueryStringNormalizerItf_destroy(struct QueryStringNormalizerItf *itf);

#endif