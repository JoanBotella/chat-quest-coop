#ifndef QUERYSTRINGNORMALIZER_HEADER
#define QUERYSTRINGNORMALIZER_HEADER

#include "chatQuest/queryStringNormalizer/QueryStringNormalizerItf.h"

struct QueryStringNormalizerSta;

struct QueryStringNormalizer
{
	struct QueryStringNormalizerSta *sta;
	struct QueryStringNormalizerItf *QueryStringNormalizerItf;
};

struct QueryStringNormalizer *QueryStringNormalizer_new();

#endif
