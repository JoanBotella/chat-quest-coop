#include "chatQuest/globals/globals.h"
#include "chatQuest/queryStringNormalizer/QueryStringNormalizerItf.h"

struct QueryStringNormalizerItf *QueryStringNormalizerItf_new(
	void *imp,
	void (*destroy)(struct QueryStringNormalizerItf *itf),

	char *(*normalize)(struct QueryStringNormalizerItf *itf, char* raw)
)
{
	struct QueryStringNormalizerItf *itf = chatQuest_malloc(sizeof(struct QueryStringNormalizerItf));
	*itf = (struct QueryStringNormalizerItf) {
		.imp = imp,
		.destroy = destroy,

		.normalize = normalize
	};
	return itf;
}

void QueryStringNormalizerItf_destroy(struct QueryStringNormalizerItf *itf)
{
	chatQuest_free(itf);
}