#include "chatQuest/globals/globals.h"
#include "chatQuest/node/NodeAbs.h"
#include "chatQuest/request/RequestItf.h"
#include "chatQuest/response/Response.h"
#include "chatQuest/response/ResponseItf.h"
#include "chatQuest/slide/Slide.h"
#include "chatQuest/slide/SlideItf.h"
#include "chatQuest/slideArray/SlideArray.h"
#include "chatQuest/slideArray/SlideArrayItf.h"
#include "chatQuest/stringArray/StringArrayItf.h"
#include "chatQuest/errorDispatcher/ErrorDispatcherItf.h"
#include "chatQuest/query/QueryItf.h"

static void NodeAbs_setResponse(struct NodeAbs *abs, struct ResponseItf *response);



struct NodeAbsSta
{
	char *id;

	struct ErrorDispatcherItf *errorDispatcher;

	void (*runByQueryAndResponse)(struct NodeAbs *abs, struct QueryItf *query, struct ResponseItf *response);

	struct ResponseItf *response;
};

static struct NodeAbsSta *NodeAbsSta_new(
	char *id,

	struct ErrorDispatcherItf *errorDispatcher,

	void (*runByQueryAndResponse)(struct NodeAbs *abs, struct QueryItf *query, struct ResponseItf *response)
)
{
	struct NodeAbsSta *sta = chatQuest_malloc(sizeof(struct NodeAbsSta));
	*sta = (struct NodeAbsSta) {
		.id = chatQuest_copyString(id),

		.errorDispatcher = errorDispatcher,

		.runByQueryAndResponse = runByQueryAndResponse,

		.response = NULL
	};
	return sta;
}

static void NodeAbsSta_destroy(struct NodeAbsSta *sta)
{
	chatQuest_free(sta->id);

	// Response is not destroyed here.

	chatQuest_free(sta);
}

static void NodeAbs_destroy(struct NodeAbs *abs)
{
	NodeAbsSta_destroy(abs->sta);
	chatQuest_free(abs);
}

static char *NodeAbs_getId(struct NodeAbs *abs)
{
	return abs->sta->id;
}

static struct ErrorDispatcherItf *NodeAbs_getErrorDispatcher(struct NodeAbs *abs)
{
	return abs->sta->errorDispatcher;
}

static void NodeAbs_reset(struct NodeAbs *abs)
{
	struct NodeAbsSta *sta = abs->sta;

	if (sta->response != NULL)
	{
		sta->response->destroy(sta->response);
		sta->response = NULL;
	}
}

static struct ResponseItf *NodeAbs_buildResponseByRequest(struct NodeAbs *abs, struct RequestItf *request)
{
	struct NodeAbsSta *sta = abs->sta;

	struct StatusItf *status = request->getStatus(request);
	struct StringArrayItf *nodeIds = status->getNodeIds(status);
	nodeIds->pop(nodeIds);
	nodeIds->append(nodeIds, sta->id);

	return Response_new(
		status
	)->ResponseItf;
}


static void NodeAbs_run(struct NodeAbs *abs, struct RequestItf *request)
{
	struct NodeAbsSta *sta = abs->sta;

	NodeAbs_reset(abs);

	struct ResponseItf *response = NodeAbs_buildResponseByRequest(abs, request);

	struct QueryItf *query = request->getQuery(request);

	sta->runByQueryAndResponse(
		abs,
		query,
		response
	);
}

static bool NodeAbs_hasResponse(struct NodeAbs *abs)
{
	return abs->sta->response != NULL;
}

static struct ResponseItf *NodeAbs_ownResponseAfterHas(struct NodeAbs *abs)
{
	struct ResponseItf *response = abs->sta->response;
	struct ResponseItf *r = response;
	response = NULL;
	return r;
}

static void NodeAbs_unsetResponse(struct NodeAbs *abs)
{
	struct ResponseItf *response = abs->sta->response;
	if (response != NULL)
	{
		response->destroy(response);
		response = NULL;
	}
}

static void NodeAbs_setResponse(struct NodeAbs *abs, struct ResponseItf *response)
{
	NodeAbs_unsetResponse(abs);
	abs->sta->response = response;
}

static struct SlideItf *NodeAbs_newSlide(struct NodeAbs *abs)
{
	return Slide_new()->SlideItf;
}

static struct SlideArrayItf *NodeAbs_newSlideArray(struct NodeAbs *abs)
{
	return SlideArray_new()->SlideArrayItf;
}

static struct ResponseItf *NodeAbs_globalRunByQueryAndResponse(struct NodeAbs *abs, struct QueryItf *query, struct ResponseItf *response)
{
	if (!query->hasVerb(query))
	{
		return response;
	}

	char *verb = query->getVerbAfterHas(query);

	if (chatQuest_areStringsEqual(verb, "quit"))
	{
		struct SlideArrayItf *slides = abs->newSlideArray(abs);

		struct SlideItf *slide = abs->newSlide(abs);
		slide->setBody(slide, "Bye!");
		slides->append(slides, slide);

		response->setSlides(response, slides);

		response->setNextAction(response, CHATQUEST_RESPONSEACTION_END);
	}
	else if (chatQuest_areStringsEqual(verb, "go"))
	{
		if (query->hasDirect(query))
		{
			struct SlideArrayItf *slides = abs->newSlideArray(abs);

			struct SlideItf *slide = abs->newSlide(abs);
			slide->setBody(slide, "I can't go there.");
			slides->append(slides, slide);

			response->setSlides(response, slides);
		}
		else
		{
			struct SlideArrayItf *slides = abs->newSlideArray(abs);

			struct SlideItf *slide = abs->newSlide(abs);
			slide->setBody(slide, "Go where?");
			slides->append(slides, slide);

			response->setSlides(response, slides);
		}
	}
	else
	{
		struct SlideArrayItf *slides = abs->newSlideArray(abs);

		struct SlideItf *slide = abs->newSlide(abs);
		slide->setTransitionIn(slide, CHATQUEST_TRANSITION_IN_SLEEP);
		slide->setBody(slide, "What?");
		slides->append(slides, slide);

		response->setSlides(response, slides);
	}

	return response;
}

struct NodeAbs *NodeAbs_new(
	void *imp,
	char *id,

	struct ErrorDispatcherItf *errorDispatcher,

	void (*runByQueryAndResponse)(struct NodeAbs *abs, struct QueryItf *query, struct ResponseItf *response)
)
{
	struct NodeAbs *abs = chatQuest_malloc(sizeof(struct NodeAbs));
	*abs = (struct NodeAbs) {
		.imp = imp,
		.sta = NodeAbsSta_new(
			id,

			errorDispatcher,

			runByQueryAndResponse
		),
		.destroy = &NodeAbs_destroy,

		.getId = &NodeAbs_getId,

		.run = &NodeAbs_run,

		.getErrorDispatcher = &NodeAbs_getErrorDispatcher,

		.hasResponse = &NodeAbs_hasResponse,
		.ownResponseAfterHas = &NodeAbs_ownResponseAfterHas,
		.setResponse = &NodeAbs_setResponse,
		.unsetResponse = &NodeAbs_unsetResponse,

		.newSlide = &NodeAbs_newSlide,
		.newSlideArray = &NodeAbs_newSlideArray,

		.globalRunByQueryAndResponse = &NodeAbs_globalRunByQueryAndResponse
	};
	return abs;
}