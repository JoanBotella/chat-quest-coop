#ifndef CHATQUEST_NODEITF_HEADER
#define CHATQUEST_NODEITF_HEADER

#include "chatQuest/globals/globals.h"
#include "chatQuest/request/RequestItf.h"
#include "chatQuest/response/ResponseItf.h"

struct NodeItf
{
	void *imp;
	void (*destroy)(struct NodeItf *itf);

	char *(*getId)(struct NodeItf *itf);

	void (*run)(struct NodeItf *itf, struct RequestItf *request);

	bool (*hasResponse)(struct NodeItf *itf);
	struct ResponseItf *(*ownResponseAfterHas)(struct NodeItf *itf);
};

struct NodeItf *NodeItf_new(
	void *imp,
	void (*destroy)(struct NodeItf *itf),

	char *(*getId)(struct NodeItf *itf),

	void (*run)(struct NodeItf *itf, struct RequestItf *request),

	bool (*hasResponse)(struct NodeItf *itf),
	struct ResponseItf *(*ownResponseAfterHas)(struct NodeItf *itf)
);

void NodeItf_destroy(struct NodeItf *itf);

#endif