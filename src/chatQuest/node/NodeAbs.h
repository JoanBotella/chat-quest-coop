#ifndef CHATQUEST_NODEABS_HEADER
#define CHATQUEST_NODEABS_HEADER

#include "chatQuest/globals/globals.h"
#include "chatQuest/request/RequestItf.h"
#include "chatQuest/status/StatusItf.h"
#include "chatQuest/response/ResponseItf.h"
#include "chatQuest/slide/SlideItf.h"
#include "chatQuest/slideArray/SlideArrayItf.h"
#include "chatQuest/errorDispatcher/ErrorDispatcherItf.h"
#include "chatQuest/query/QueryItf.h"

struct NodeAbsSta;

struct NodeAbs
{
	void *imp;
	struct NodeAbsSta *sta;
	void (*destroy)(struct NodeAbs *abs);

	char *(*getId)(struct NodeAbs *abs);

	void (*run)(struct NodeAbs *abs, struct RequestItf *request);

	struct ErrorDispatcherItf *(*getErrorDispatcher)(struct NodeAbs *abs);

	bool (*hasResponse)(struct NodeAbs *abs);
	struct ResponseItf *(*ownResponseAfterHas)(struct NodeAbs *abs);
	void (*setResponse)(struct NodeAbs *abs, struct ResponseItf *response);
	void (*unsetResponse)(struct NodeAbs *abs);

	struct SlideItf *(*newSlide)(struct NodeAbs *abs);
	struct SlideArrayItf *(*newSlideArray)(struct NodeAbs *abs);

	struct ResponseItf *(*globalRunByQueryAndResponse)(struct NodeAbs *abs, struct QueryItf *query, struct ResponseItf *response);
};

struct NodeAbs *NodeAbs_new(
	void *imp,
	char *id,

	struct ErrorDispatcherItf *errorDispatcher,

	void (*runByQueryAndResponse)(struct NodeAbs *abs, struct QueryItf *query, struct ResponseItf *response)
);

#endif