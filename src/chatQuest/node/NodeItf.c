#include "chatQuest/globals/globals.h"
#include "chatQuest/node/NodeItf.h"
#include "chatQuest/request/RequestItf.h"
#include "chatQuest/response/ResponseItf.h"

struct NodeItf *NodeItf_new(
	void *imp,
	void (*destroy)(struct NodeItf *itf),

	char *(*getId)(struct NodeItf *itf),

	void (*run)(struct NodeItf *itf, struct RequestItf *request),

	bool (*hasResponse)(struct NodeItf *itf),
	struct ResponseItf *(*ownResponseAfterHas)(struct NodeItf *itf)
)
{
	struct NodeItf *itf = chatQuest_malloc(sizeof(struct NodeItf));
	*itf = (struct NodeItf) {
		.imp = imp,
		.destroy = destroy,

		.getId = getId,

		.run = run,

		.hasResponse = hasResponse,
		.ownResponseAfterHas = ownResponseAfterHas
	};
	return itf;
}

void NodeItf_destroy(struct NodeItf *itf)
{
	chatQuest_free(itf);
}