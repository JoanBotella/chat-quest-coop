#include "chatQuest/globals/globals.h"
#include "chatQuest/error/Error.h"
#include "chatQuest/error/ErrorItf.h"

struct ErrorSta
{
	int code;
	char *file;
	int line;
	char *message;
};

static struct ErrorSta *ErrorSta_new(
	int code,
	char *file,
	int line,
	char *message
)
{
	struct ErrorSta *sta = chatQuest_malloc(sizeof(struct ErrorSta));
	*sta = (struct ErrorSta) {
		.code = code,
		.file = chatQuest_copyString(file),
		.line = line,
		.message = chatQuest_copyString(message)
	};
	return sta;
}

static void ErrorSta_destroy(struct ErrorSta *sta)
{
	chatQuest_free(sta->file);
	chatQuest_free(sta->message);
	chatQuest_free(sta);
}

static void Error_destroy(struct ErrorItf *itf)
{
	struct Error *imp = (struct Error *)itf->imp;

	ErrorSta_destroy(imp->sta);
	ErrorItf_destroy(imp->ErrorItf);
	chatQuest_free(imp);
}

static int Error_getCode(struct ErrorItf *itf)
{
	struct ErrorSta *sta = ((struct Error *) itf->imp)->sta;
	return sta->code;
}

static char *Error_getFile(struct ErrorItf *itf)
{
	struct ErrorSta *sta = ((struct Error *) itf->imp)->sta;
	return sta->file;
}

static int Error_getLine(struct ErrorItf *itf)
{
	struct ErrorSta *sta = ((struct Error *) itf->imp)->sta;
	return sta->line;
}

static char *Error_getMessage(struct ErrorItf *itf)
{
	struct ErrorSta *sta = ((struct Error *) itf->imp)->sta;
	return sta->message;
}

struct Error *Error_new(
	int code,
	char *file,
	int line,
	char *message
)
{
	struct Error *imp = chatQuest_malloc(sizeof(struct Error));
	*imp = (struct Error) {
		.sta = ErrorSta_new(
			code,
			file,
			line,
			message
		),
		.ErrorItf = ErrorItf_new(
			imp,
			&Error_destroy,

			&Error_getCode,
			&Error_getFile,
			&Error_getLine,
			&Error_getMessage
		)
	};
	return imp;
}