#include "chatQuest/globals/globals.h"
#include "chatQuest/error/ErrorItf.h"

struct ErrorItf *ErrorItf_new(
	void *imp,
	void (*destroy)(struct ErrorItf *itf),

	int (*getCode)(struct ErrorItf *itf),
	char *(*getFile)(struct ErrorItf *itf),
	int (*getLine)(struct ErrorItf *itf),
	char *(*getMessage)(struct ErrorItf *itf)
)
{
	struct ErrorItf *itf = chatQuest_malloc(sizeof(struct ErrorItf));
	*itf = (struct ErrorItf) {
		.imp = imp,
		.destroy = destroy,

		.getCode = getCode,
		.getFile = getFile,
		.getLine = getLine,
		.getMessage = getMessage
	};
	return itf;
}

void ErrorItf_destroy(struct ErrorItf *itf)
{
	chatQuest_free(itf);
}