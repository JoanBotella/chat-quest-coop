#ifndef CHATQUEST_ERROR_HEADER
#define CHATQUEST_ERROR_HEADER

#include "chatQuest/error/ErrorItf.h"

struct ErrorSta;

struct Error
{
	struct ErrorSta *sta;
	struct ErrorItf *ErrorItf;
};

struct Error *Error_new(
	int code,
	char *file,
	int line,
	char *message
);

#endif
