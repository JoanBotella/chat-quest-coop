#ifndef CHATQUEST_ERRORITF_HEADER
#define CHATQUEST_ERRORITF_HEADER

struct ErrorItf
{
	void *imp;
	void (*destroy)(struct ErrorItf *itf);

	int (*getCode)(struct ErrorItf *itf);
	char *(*getFile)(struct ErrorItf *itf);
	int (*getLine)(struct ErrorItf *itf);
	char *(*getMessage)(struct ErrorItf *itf);
};

struct ErrorItf *ErrorItf_new(
	void *imp,
	void (*destroy)(struct ErrorItf *itf),

	int (*getCode)(struct ErrorItf *itf),
	char *(*getFile)(struct ErrorItf *itf),
	int (*getLine)(struct ErrorItf *itf),
	char *(*getMessage)(struct ErrorItf *itf)
);

void ErrorItf_destroy(struct ErrorItf *itf);

#endif