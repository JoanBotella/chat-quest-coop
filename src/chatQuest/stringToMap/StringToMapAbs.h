#ifndef CHATQUEST_STRINGTOMAPABS_HEADER
#define CHATQUEST_STRINGTOMAPABS_HEADER

struct StringToMapAbsSta;

struct StringToMapAbs
{
	void *imp;
	struct StringToMapAbsSta *sta;
	void (*destroy)(struct StringToMapAbs *abs);

	bool (*has)(struct StringToMapAbs *abs, void *key);
	void *(*getAfterHas)(struct StringToMapAbs *abs, void *key);
	void (*set)(struct StringToMapAbs *abs, void *key, void *value);
	void (*unset)(struct StringToMapAbs *abs, void *key);
};

struct StringToMapAbs *StringToMapAbs_new(
	void *imp,
	void (*destroyItem)(struct StringToMapAbs *abs, void *key, void *value)
);

#endif