#include <glib.h>
#include "chatQuest/globals/globals.h"
#include "chatQuest/stringToMap/StringToMapAbs.h"

struct StringToMapAbsSta
{
	GHashTable *gHashTable;
	void (*destroyItem)(struct StringToMapAbs *abs, void *key, void *value);
};

static struct StringToMapAbsSta *StringToMapAbsSta_new(
	void (*destroyItem)(struct StringToMapAbs *abs, void *key, void *value)
)
{
	struct StringToMapAbsSta *sta = chatQuest_malloc(sizeof(struct StringToMapAbsSta));
	*sta = (struct StringToMapAbsSta) {
		.gHashTable = g_hash_table_new(g_str_hash, g_str_equal),
		.destroyItem = destroyItem
	};
	return sta;
}

static void StringToMapAbsSta_destroy(struct StringToMapAbsSta *sta)
{
	g_hash_table_destroy(sta->gHashTable);
	chatQuest_free(sta);
}

		static void StringToMapAbs_destroyItemsForeachCallback(void *key, void *value, void *absVoid)
		{
			struct StringToMapAbs *abs = (struct StringToMapAbs *)absVoid;
			abs->sta->destroyItem(abs, key, value);
		}

	static void StringToMapAbs_destroyItems(struct StringToMapAbs *abs)
	{
		g_hash_table_foreach(
			abs->sta->gHashTable,
			StringToMapAbs_destroyItemsForeachCallback,
			abs
		);
	}

static void StringToMapAbs_destroy(struct StringToMapAbs *abs)
{
	StringToMapAbs_destroyItems(abs);
	StringToMapAbsSta_destroy(abs->sta);
	chatQuest_free(abs);
}

static bool StringToMapAbs_has(struct StringToMapAbs *abs, void *key)
{
	return g_hash_table_contains(abs->sta->gHashTable, key);
}

static void *StringToMapAbs_getAfterHas(struct StringToMapAbs *abs, void *key)
{
	return g_hash_table_lookup(abs->sta->gHashTable, key);
}

static void StringToMapAbs_set(struct StringToMapAbs *abs, void *key, void *value)
{
	g_hash_table_insert(abs->sta->gHashTable, key, value);
}

static void StringToMapAbs_unset(struct StringToMapAbs *abs, void *key)
{
	g_hash_table_remove(abs->sta->gHashTable, key);
}

struct StringToMapAbs *StringToMapAbs_new(
	void *imp,
	void (*destroyItem)(struct StringToMapAbs *abs, void *key, void *value)
)
{
	struct StringToMapAbs *abs = chatQuest_malloc(sizeof(struct StringToMapAbs));
	*abs = (struct StringToMapAbs) {
		.imp = imp,
		.sta = StringToMapAbsSta_new(
			destroyItem
		),
		.destroy = &StringToMapAbs_destroy,

		.has = &StringToMapAbs_has,
		.getAfterHas = &StringToMapAbs_getAfterHas,
		.set = &StringToMapAbs_set,
		.unset = &StringToMapAbs_unset
	};
	return abs;
}