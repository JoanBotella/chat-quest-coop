#include "chatQuest/globals/globals.h"
#include "chatQuest/slide/SlideItf.h"
#include "chatQuest/slide/TransitionIn.h"
#include "chatQuest/slide/TransitionOut.h"

struct SlideItf *SlideItf_new(
	void *imp,
	void (*destroy)(struct SlideItf *itf),

	bool (*hasTitle)(struct SlideItf *itf),
	char *(*getTitleAfterHas)(struct SlideItf *itf),
	void (*setTitle)(struct SlideItf *itf, char *title),
	void (*unsetTitle)(struct SlideItf *itf),

	bool (*hasBody)(struct SlideItf *itf),
	char *(*getBodyAfterHas)(struct SlideItf *itf),
	void (*setBody)(struct SlideItf *itf, char *body),
	void (*unsetBody)(struct SlideItf *itf),

	enum TransitionIn (*getTransitionIn)(struct SlideItf *itf),
	void (*setTransitionIn)(struct SlideItf *itf, enum TransitionIn transitionIn),

	enum TransitionOut (*getTransitionOut)(struct SlideItf *itf),
	void (*setTransitionOut)(struct SlideItf *itf, enum TransitionOut transitionOut)
)
{
	struct SlideItf *itf = chatQuest_malloc(sizeof(struct SlideItf));
	*itf = (struct SlideItf) {
		.imp = imp,
		.destroy = destroy,

		.hasTitle = hasTitle,
		.getTitleAfterHas = getTitleAfterHas,
		.setTitle = setTitle,
		.unsetTitle = unsetTitle,

		.hasBody = hasBody,
		.getBodyAfterHas = getBodyAfterHas,
		.setBody = setBody,
		.unsetBody = unsetBody,

		.getTransitionIn = getTransitionIn,
		.setTransitionIn = setTransitionIn,

		.getTransitionOut = getTransitionOut,
		.setTransitionOut = setTransitionOut
	};
	return itf;
}

void SlideItf_destroy(struct SlideItf *itf)
{
	chatQuest_free(itf);
}