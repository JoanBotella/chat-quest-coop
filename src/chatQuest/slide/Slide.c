#include "chatQuest/globals/globals.h"
#include "chatQuest/slide/Slide.h"
#include "chatQuest/slide/SlideItf.h"
#include "chatQuest/slide/TransitionIn.h"
#include "chatQuest/slide/TransitionOut.h"

struct SlideSta
{
	char *title;
	char *body;
	enum TransitionIn transitionIn;
	enum TransitionOut transitionOut;
};

static struct SlideSta *SlideSta_new()
{
	struct SlideSta *sta = chatQuest_malloc(sizeof(struct SlideSta));
	*sta = (struct SlideSta) {
		.title = NULL,
		.body = NULL,
		.transitionIn = CHATQUEST_TRANSITION_IN_NONE,
		.transitionOut = CHATQUEST_TRANSITION_OUT_NONE
	};
	return sta;
}

static void SlideSta_destroy(struct SlideSta *sta)
{
	chatQuest_free(sta->title);
	chatQuest_free(sta->body);
	chatQuest_free(sta);
}

static void Slide_destroy(struct SlideItf *itf)
{
	struct Slide *imp = (struct Slide *)itf->imp;

	SlideSta_destroy(imp->sta);
	SlideItf_destroy(imp->SlideItf);
	chatQuest_free(imp);
}

static bool Slide_hasTitle(struct SlideItf *itf)
{
	struct SlideSta *sta = ((struct Slide *)itf->imp)->sta;
	return sta->title != NULL;
}

static char *Slide_getTitleAfterHas(struct SlideItf *itf)
{
	struct SlideSta *sta = ((struct Slide *)itf->imp)->sta;
	return sta->title;
}

static void Slide_setTitle(struct SlideItf *itf, char *title)
{
	struct SlideSta *sta = ((struct Slide *)itf->imp)->sta;
	chatQuest_free(sta->title);
	sta->title = chatQuest_copyString(title);
}

static void Slide_unsetTitle(struct SlideItf *itf)
{
	struct SlideSta *sta = ((struct Slide *)itf->imp)->sta;
	chatQuest_free(sta->title);
	sta->title = NULL;
}

static bool Slide_hasBody(struct SlideItf *itf)
{
	struct SlideSta *sta = ((struct Slide *)itf->imp)->sta;
	return sta->body != NULL;
}

static char *Slide_getBodyAfterHas(struct SlideItf *itf)
{
	struct SlideSta *sta = ((struct Slide *)itf->imp)->sta;
	return sta->body;
}

static void Slide_setBody(struct SlideItf *itf, char *body)
{
	struct SlideSta *sta = ((struct Slide *)itf->imp)->sta;
	chatQuest_free(sta->body);
	sta->body = chatQuest_copyString(body);
}

static void Slide_unsetBody(struct SlideItf *itf)
{
	struct SlideSta *sta = ((struct Slide *)itf->imp)->sta;
	chatQuest_free(sta->body);
	sta->body = NULL;
}

static enum TransitionIn Slide_getTransitionIn(struct SlideItf *itf)
{
	struct SlideSta *sta = ((struct Slide *)itf->imp)->sta;
	return sta->transitionIn;
}

static void Slide_setTransitionIn(struct SlideItf *itf, enum TransitionIn transitionIn)
{
	struct SlideSta *sta = ((struct Slide *)itf->imp)->sta;
	sta->transitionIn = transitionIn;
}

static enum TransitionOut Slide_getTransitionOut(struct SlideItf *itf)
{
	struct SlideSta *sta = ((struct Slide *)itf->imp)->sta;
	return sta->transitionOut;
}

static void Slide_setTransitionOut(struct SlideItf *itf, enum TransitionOut transitionOut)
{
	struct SlideSta *sta = ((struct Slide *)itf->imp)->sta;
	sta->transitionOut = transitionOut;
}

struct Slide *Slide_new()
{
	struct Slide *imp = chatQuest_malloc(sizeof(struct Slide));
	*imp = (struct Slide) {
		.sta = SlideSta_new(),
		.SlideItf = SlideItf_new(
			imp,
			&Slide_destroy,

			&Slide_hasTitle,
			&Slide_getTitleAfterHas,
			&Slide_setTitle,
			&Slide_unsetTitle,

			&Slide_hasBody,
			&Slide_getBodyAfterHas,
			&Slide_setBody,
			&Slide_unsetBody,

			&Slide_getTransitionIn,
			&Slide_setTransitionIn,

			&Slide_getTransitionOut,
			&Slide_setTransitionOut
		)
	};
	return imp;
}