#ifndef CHATQUEST_SLIDE_HEADER
#define CHATQUEST_SLIDE_HEADER

#include "chatQuest/slide/SlideItf.h"

struct SlideSta;

struct Slide
{
	struct SlideSta *sta;
	struct SlideItf *SlideItf;
};

struct Slide *Slide_new();

#endif
