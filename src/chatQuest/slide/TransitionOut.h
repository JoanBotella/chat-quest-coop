#ifndef CHATQUEST_SLIDE_TRANSITIONOUT_HEADER
#define CHATQUEST_SLIDE_TRANSITIONOUT_HEADER

enum TransitionOut
{
	CHATQUEST_TRANSITION_OUT_NONE = 0,
	CHATQUEST_TRANSITION_OUT_PROMPT = 1,
	CHATQUEST_TRANSITION_OUT_SLEEP = 2
};

#endif