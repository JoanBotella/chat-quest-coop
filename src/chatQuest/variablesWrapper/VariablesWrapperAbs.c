#include "chatQuest/globals/globals.h"
#include "chatQuest/variablesWrapper/VariablesWrapperAbs.h"
#include "chatQuest/stringToStringMap/StringToStringMapItf.h"

struct VariablesWrapperAbsSta
{
	struct StringToStringMapItf *variables;
};

static struct VariablesWrapperAbsSta *VariablesWrapperAbsSta_new(
	struct StringToStringMapItf *variables
)
{
	struct VariablesWrapperAbsSta *sta = chatQuest_malloc(sizeof(struct VariablesWrapperAbsSta));
	*sta = (struct VariablesWrapperAbsSta) {
		.variables = variables
	};
	return sta;
}

static void VariablesWrapperAbsSta_destroy(struct VariablesWrapperAbsSta *sta)
{
	// Do not destroy variables
	chatQuest_free(sta);
}

static void VariablesWrapperAbs_destroy(struct VariablesWrapperAbs *abs)
{
	VariablesWrapperAbsSta_destroy(abs->sta);
	chatQuest_free(abs);
}

static bool VariablesWrapperAbs_hasVariable(struct VariablesWrapperAbs *abs, char *name)
{
	struct StringToStringMapItf *variables = abs->sta->variables;
	return variables->has(variables, name);
}

static void VariablesWrapperAbs_unsetVariable(struct VariablesWrapperAbs *abs, char *name)
{
	struct StringToStringMapItf *variables = abs->sta->variables;
	variables->unset(variables, name);
}

static int VariablesWrapperAbs_getIntVariableAfterHas(struct VariablesWrapperAbs *abs, char *name)
{
	struct StringToStringMapItf *variables = abs->sta->variables;
	char *serializedValue = variables->getAfterHas(variables, name);
	return chatQuest_unserializeInt(serializedValue);
}

static void VariablesWrapperAbs_setIntVariable(struct VariablesWrapperAbs *abs, char *name, int value)
{
	struct StringToStringMapItf *variables = abs->sta->variables;
	char *serializedValue = chatQuest_serializeInt(value);
	variables->set(variables, name, serializedValue);
	chatQuest_free(serializedValue);
}

static float VariablesWrapperAbs_getFloatVariableAfterHas(struct VariablesWrapperAbs *abs, char *name)
{
	struct StringToStringMapItf *variables = abs->sta->variables;
	char *serializedValue = variables->getAfterHas(variables, name);
	return chatQuest_unserializeFloat(serializedValue);
}

static void VariablesWrapperAbs_setFloatVariable(struct VariablesWrapperAbs *abs, char *name, float value)
{
	struct StringToStringMapItf *variables = abs->sta->variables;
	char *serializedValue = chatQuest_serializeFloat(value);
	variables->set(variables, name, serializedValue);
	chatQuest_free(serializedValue);
}

static bool VariablesWrapperAbs_getBoolVariableAfterHas(struct VariablesWrapperAbs *abs, char *name)
{
	struct StringToStringMapItf *variables = abs->sta->variables;
	char *serializedValue = variables->getAfterHas(variables, name);
	return chatQuest_unserializeBool(serializedValue);
}

static void VariablesWrapperAbs_setBoolVariable(struct VariablesWrapperAbs *abs, char *name, bool value)
{
	struct StringToStringMapItf *variables = abs->sta->variables;
	char *serializedValue = chatQuest_serializeBool(value);
	variables->set(variables, name, serializedValue);
	chatQuest_free(serializedValue);
}

static char *VariablesWrapperAbs_getStringVariableAfterHas(struct VariablesWrapperAbs *abs, char *name)
{
	struct StringToStringMapItf *variables = abs->sta->variables;
	return variables->getAfterHas(variables, name);
}

static void VariablesWrapperAbs_setStringVariable(struct VariablesWrapperAbs *abs, char *name, char *value)
{
	struct StringToStringMapItf *variables = abs->sta->variables;
	variables->set(variables, name, value);
}

struct VariablesWrapperAbs *VariablesWrapperAbs_new(
	void *imp,

	struct StringToStringMapItf *variables
)
{
	struct VariablesWrapperAbs *abs = chatQuest_malloc(sizeof(struct VariablesWrapperAbs));
	*abs = (struct VariablesWrapperAbs) {
		.imp = imp,
		.sta = VariablesWrapperAbsSta_new(
			variables
		),
		.destroy = &VariablesWrapperAbs_destroy,

		.hasVariable = &VariablesWrapperAbs_hasVariable,
		.unsetVariable = &VariablesWrapperAbs_unsetVariable,

		.getIntVariableAfterHas = &VariablesWrapperAbs_getIntVariableAfterHas,
		.setIntVariable = &VariablesWrapperAbs_setIntVariable,

		.getFloatVariableAfterHas = &VariablesWrapperAbs_getFloatVariableAfterHas,
		.setFloatVariable = &VariablesWrapperAbs_setFloatVariable,

		.getBoolVariableAfterHas = &VariablesWrapperAbs_getBoolVariableAfterHas,
		.setBoolVariable = &VariablesWrapperAbs_setBoolVariable,

		.getStringVariableAfterHas = &VariablesWrapperAbs_getStringVariableAfterHas,
		.setStringVariable = &VariablesWrapperAbs_setStringVariable
	};
	return abs;
}