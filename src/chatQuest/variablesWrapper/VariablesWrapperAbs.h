#ifndef CHATQUEST_VARIABLESWRAPPERABS_HEADER
#define CHATQUEST_VARIABLESWRAPPERABS_HEADER

#include "chatQuest/stringToStringMap/StringToStringMapItf.h"

struct VariablesWrapperAbsSta;

struct VariablesWrapperAbs
{
	void *imp;
	struct VariablesWrapperAbsSta *sta;
	void (*destroy)(struct VariablesWrapperAbs *abs);

	bool (*hasVariable)(struct VariablesWrapperAbs *abs, char *name);
	void (*unsetVariable)(struct VariablesWrapperAbs *abs, char *name);

	int (*getIntVariableAfterHas)(struct VariablesWrapperAbs *abs, char *name);
	void (*setIntVariable)(struct VariablesWrapperAbs *abs, char *name, int value);

	float (*getFloatVariableAfterHas)(struct VariablesWrapperAbs *abs, char *name);
	void (*setFloatVariable)(struct VariablesWrapperAbs *abs, char *name, float value);

	bool (*getBoolVariableAfterHas)(struct VariablesWrapperAbs *abs, char *name);
	void (*setBoolVariable)(struct VariablesWrapperAbs *abs, char *name, bool value);

	char *(*getStringVariableAfterHas)(struct VariablesWrapperAbs *abs, char *name);
	void (*setStringVariable)(struct VariablesWrapperAbs *abs, char *name, char *value);
};

struct VariablesWrapperAbs *VariablesWrapperAbs_new(
	void *imp,

	struct StringToStringMapItf *variables
);

#endif