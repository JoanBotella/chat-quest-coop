#ifndef CHATQUEST_STRINGARRAY_HEADER
#define CHATQUEST_STRINGARRAY_HEADER

#include "chatQuest/stringArray/StringArrayItf.h"
#include "chatQuest/array/ArrayAbs.h"

struct StringArraySta;

struct StringArray
{
	struct StringArraySta *sta;
	struct StringArrayItf *StringArrayItf;
	struct ArrayAbs *ArrayAbs;
};

struct StringArray *StringArray_new();

#endif
