#include "chatQuest/globals/globals.h"
#include "chatQuest/stringArray/StringArrayItf.h"

struct StringArrayItf *StringArrayItf_new(
	void *imp,
	void (*destroy)(struct StringArrayItf *itf),
	
	void (*append)(struct StringArrayItf *itf, char *item),
	void (*prepend)(struct StringArrayItf *itf, char *item),
	int (*getLength)(struct StringArrayItf *itf),
	bool (*hasAt)(struct StringArrayItf *itf, int index),
	char *(*getAtAfterHas)(struct StringArrayItf *itf, int index),
	bool (*isEmpty)(struct StringArrayItf *itf),
	char *(*getLastAfterIsNotEmpty)(struct StringArrayItf *itf),
	void (*removeAtAfterHas)(struct StringArrayItf *itf, int index),
	void (*pop)(struct StringArrayItf *itf),
	void (*shift)(struct StringArrayItf *itf)
)
{
	struct StringArrayItf *itf = chatQuest_malloc(sizeof(struct StringArrayItf));
	*itf = (struct StringArrayItf) {
		.imp = imp,
		.destroy = destroy,
		
		.append = append,
		.prepend = prepend,
		.getLength = getLength,
		.hasAt = hasAt,
		.getAtAfterHas = getAtAfterHas,
		.isEmpty = isEmpty,
		.getLastAfterIsNotEmpty = getLastAfterIsNotEmpty,
		.removeAtAfterHas = removeAtAfterHas,
		.pop = pop,
		.shift = shift
	};
	return itf;
}

void StringArrayItf_destroy(struct StringArrayItf *itf)
{
	chatQuest_free(itf);
}
