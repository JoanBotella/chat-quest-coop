#include "chatQuest/globals/globals.h"
#include "chatQuest/stringArray/StringArray.h"
#include "chatQuest/stringArray/StringArrayItf.h"
#include "chatQuest/array/ArrayAbs.h"

struct StringArraySta
{
};

static struct StringArraySta *StringArraySta_new()
{
	struct StringArraySta *sta = chatQuest_malloc(sizeof(struct StringArraySta));
	*sta = (struct StringArraySta) {
	};
	return sta;
}

static void StringArraySta_destroy(struct StringArraySta *sta)
{
	chatQuest_free(sta);
}

static void StringArray_destroyItem(struct ArrayAbs *abs, void *item)
{
	char *string = (char *)item;
	chatQuest_free(string);
}

static void StringArray_destroy(struct StringArrayItf *itf)
{
	struct StringArray *imp = (struct StringArray *)itf->imp;
	struct ArrayAbs *abs = imp->ArrayAbs;

	StringArraySta_destroy(imp->sta);
	StringArrayItf_destroy(imp->StringArrayItf);
	abs->destroy(abs);
	chatQuest_free(imp);
}

static void StringArray_append(struct StringArrayItf *itf, char *item)
{
	struct ArrayAbs *abs = ((struct StringArray *)itf->imp)->ArrayAbs;
	abs->append(abs, chatQuest_copyString(item));
}

static void StringArray_prepend(struct StringArrayItf *itf, char *item)
{
	struct ArrayAbs *abs = ((struct StringArray *)itf->imp)->ArrayAbs;
	abs->prepend(abs, chatQuest_copyString(item));
}

static int StringArray_getLength(struct StringArrayItf *itf)
{
	struct ArrayAbs *abs = ((struct StringArray *)itf->imp)->ArrayAbs;
	return abs->getLength(abs);
}

static bool StringArray_hasAt(struct StringArrayItf *itf, int index)
{
	struct ArrayAbs *abs = ((struct StringArray *)itf->imp)->ArrayAbs;
	return abs->hasAt(abs, index);
}

static char *StringArray_getAtAfterHas(struct StringArrayItf *itf, int index)
{
	struct ArrayAbs *abs = ((struct StringArray *)itf->imp)->ArrayAbs;
	return (char *)abs->getAtAfterHas(abs, index);
}

static bool StringArray_isEmpty(struct StringArrayItf *itf)
{
	struct ArrayAbs *abs = ((struct StringArray *)itf->imp)->ArrayAbs;
	return abs->isEmpty(abs);
}

static char *StringArray_getLastAfterIsEmpty(struct StringArrayItf *itf)
{
	struct ArrayAbs *abs = ((struct StringArray *)itf->imp)->ArrayAbs;
	return (char *)abs->getLastAfterIsNotEmpty(abs);
}

static void StringArray_removeAtAfterHas(struct StringArrayItf *itf, int index)
{
	struct ArrayAbs *abs = ((struct StringArray *)itf->imp)->ArrayAbs;
	abs->removeAtAfterHas(abs, index);
}

static void StringArray_pop(struct StringArrayItf *itf)
{
	struct ArrayAbs *abs = ((struct StringArray *)itf->imp)->ArrayAbs;
	abs->pop(abs);
}

static void StringArray_shift(struct StringArrayItf *itf)
{
	struct ArrayAbs *abs = ((struct StringArray *)itf->imp)->ArrayAbs;
	abs->shift(abs);
}

struct StringArray *StringArray_new()
{
	struct StringArray *imp = chatQuest_malloc(sizeof(struct StringArray));
	*imp = (struct StringArray) {
		.sta = StringArraySta_new(
		),
		.StringArrayItf = StringArrayItf_new(
			imp,
			&StringArray_destroy,

			&StringArray_append,
			&StringArray_prepend,
			&StringArray_getLength,
			&StringArray_hasAt,
			&StringArray_getAtAfterHas,
			&StringArray_isEmpty,
			&StringArray_getLastAfterIsEmpty,
			&StringArray_removeAtAfterHas,
			&StringArray_pop,
			&StringArray_shift
		),
		.ArrayAbs = ArrayAbs_new(
			imp,
			&StringArray_destroyItem
		)
	};
	return imp;
}
