#ifndef CHATQUEST_STRINGARRAYITF_HEADER
#define CHATQUEST_STRINGARRAYITF_HEADER

#include "chatQuest/globals/globals.h"

struct StringArrayItf
{
	void *imp;
	void (*destroy)(struct StringArrayItf *itf);

	void (*append)(struct StringArrayItf *itf, char *item);
	void (*prepend)(struct StringArrayItf *itf, char *item);
	int (*getLength)(struct StringArrayItf *itf);
	bool (*hasAt)(struct StringArrayItf *itf, int index);
	char *(*getAtAfterHas)(struct StringArrayItf *itf, int index);
	bool (*isEmpty)(struct StringArrayItf *itf);
	char *(*getLastAfterIsNotEmpty)(struct StringArrayItf *itf);
	void (*removeAtAfterHas)(struct StringArrayItf *itf, int index);
	void (*pop)(struct StringArrayItf *itf);
	void (*shift)(struct StringArrayItf *itf);
};

struct StringArrayItf *StringArrayItf_new(
	void *imp,
	void (*destroy)(struct StringArrayItf *itf),
	
	void (*append)(struct StringArrayItf *itf, char *item),
	void (*prepend)(struct StringArrayItf *itf, char *item),
	int (*getLength)(struct StringArrayItf *itf),
	bool (*hasAt)(struct StringArrayItf *itf, int index),
	char *(*getAtAfterHas)(struct StringArrayItf *itf, int index),
	bool (*isEmpty)(struct StringArrayItf *itf),
	char *(*getLastAfterIsNotEmpty)(struct StringArrayItf *itf),
	void (*removeAtAfterHas)(struct StringArrayItf *itf, int index),
	void (*pop)(struct StringArrayItf *itf),
	void (*shift)(struct StringArrayItf *itf)
);

void StringArrayItf_destroy(struct StringArrayItf *itf);

#endif
