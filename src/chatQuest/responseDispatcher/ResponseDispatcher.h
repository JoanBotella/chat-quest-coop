#ifndef CHATQUEST_RESPONSEDISPATCHER_HEADER
#define CHATQUEST_RESPONSEDISPATCHER_HEADER

#include "chatQuest/responseDispatcher/ResponseDispatcherItf.h"

struct ResponseDispatcherSta;

struct ResponseDispatcher
{
	struct ResponseDispatcherSta *sta;
	struct ResponseDispatcherItf *ResponseDispatcherItf;
};

struct ResponseDispatcher *ResponseDispatcher_new();

#endif
