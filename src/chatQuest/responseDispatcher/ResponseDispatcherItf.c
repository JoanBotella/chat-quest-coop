#include "chatQuest/globals/globals.h"
#include "chatQuest/responseDispatcher/ResponseDispatcherItf.h"
#include "chatQuest/response/ResponseItf.h"

struct ResponseDispatcherItf *ResponseDispatcherItf_new(
	void *imp,
	void (*destroy)(struct ResponseDispatcherItf *itf),

	void (*dispatch)(struct ResponseDispatcherItf *itf, struct ResponseItf *response)
)
{
	struct ResponseDispatcherItf *itf = chatQuest_malloc(sizeof(struct ResponseDispatcherItf));
	*itf = (struct ResponseDispatcherItf) {
		.imp = imp,
		.destroy = destroy,

		.dispatch = dispatch
	};
	return itf;
}

void ResponseDispatcherItf_destroy(struct ResponseDispatcherItf *itf)
{
	chatQuest_free(itf);
}