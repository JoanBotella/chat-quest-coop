#include "chatQuest/globals/globals.h"
#include "chatQuest/responseDispatcher/ResponseDispatcher.h"
#include "chatQuest/responseDispatcher/ResponseDispatcherItf.h"
#include "chatQuest/response/ResponseItf.h"
#include "chatQuest/slide/SlideItf.h"
#include "chatQuest/slideArray/SlideArrayItf.h"

static const char *STYLE_RESET = "\e[0m";

static const char *STYLE_REGULAR_BLACK = "\e[0;30m";
static const char *STYLE_REGULAR_RED = "\e[0;31m";
static const char *STYLE_REGULAR_GREEN = "\e[0;32m";
static const char *STYLE_REGULAR_YELLOW = "\e[0;33m";
static const char *STYLE_REGULAR_BLUE = "\e[0;34m";
static const char *STYLE_REGULAR_PURPLE = "\e[0;35m";
static const char *STYLE_REGULAR_CYAN = "\e[0;36m";
static const char *STYLE_REGULAR_WHITE = "\e[0;37m";

static const char *STYLE_BOLD_BLACK = "\e[1;30m";
static const char *STYLE_BOLD_RED = "\e[1;31m";
static const char *STYLE_BOLD_GREEN = "\e[1;32m";
static const char *STYLE_BOLD_YELLOW = "\e[1;33m";
static const char *STYLE_BOLD_BLUE = "\e[1;34m";
static const char *STYLE_BOLD_PURPLE = "\e[1;35m";
static const char *STYLE_BOLD_CYAN = "\e[1;36m";
static const char *STYLE_BOLD_WHITE = "\e[1;37m";

static const char *STYLE_UNDERLINE_BLACK = "\e[4;30m";
static const char *STYLE_UNDERLINE_RED = "\e[4;31m";
static const char *STYLE_UNDERLINE_GREEN = "\e[4;32m";
static const char *STYLE_UNDERLINE_YELLOW = "\e[4;33m";
static const char *STYLE_UNDERLINE_BLUE = "\e[4;34m";
static const char *STYLE_UNDERLINE_PURPLE = "\e[4;35m";
static const char *STYLE_UNDERLINE_CYAN = "\e[4;36m";
static const char *STYLE_UNDERLINE_WHITE = "\e[4;37m";

static const char *STYLE_REGULARHIGH_BLACK = "\e[0;90m";
static const char *STYLE_REGULARHIGH_RED = "\e[0;91m";
static const char *STYLE_REGULARHIGH_GREEN = "\e[0;92m";
static const char *STYLE_REGULARHIGH_YELLOW = "\e[0;93m";
static const char *STYLE_REGULARHIGH_BLUE = "\e[0;94m";
static const char *STYLE_REGULARHIGH_PURPLE = "\e[0;95m";
static const char *STYLE_REGULARHIGH_CYAN = "\e[0;96m";
static const char *STYLE_REGULARHIGH_WHITE = "\e[0;97m";

static const char *STYLE_BOLDHIGH_BLACK = "\e[1;90m";
static const char *STYLE_BOLDHIGH_RED = "\e[1;91m";
static const char *STYLE_BOLDHIGH_GREEN = "\e[1;92m";
static const char *STYLE_BOLDHIGH_YELLOW = "\e[1;93m";
static const char *STYLE_BOLDHIGH_BLUE = "\e[1;94m";
static const char *STYLE_BOLDHIGH_PURPLE = "\e[1;95m";
static const char *STYLE_BOLDHIGH_CYAN = "\e[1;96m";
static const char *STYLE_BOLDHIGH_WHITE = "\e[1;97m";

static const char *STYLE_BACKGROUND_BLACK = "\e[40m";
static const char *STYLE_BACKGROUND_RED = "\e[41m";
static const char *STYLE_BACKGROUND_GREEN = "\e[42m";
static const char *STYLE_BACKGROUND_YELLOW = "\e[43m";
static const char *STYLE_BACKGROUND_BLUE = "\e[44m";
static const char *STYLE_BACKGROUND_PURPLE = "\e[45m";
static const char *STYLE_BACKGROUND_CYAN = "\e[46m";
static const char *STYLE_BACKGROUND_WHITE = "\e[47m";

static const char *STYLE_BACKGROUNDHIGH_BLACK = "\e[100m";
static const char *STYLE_BACKGROUNDHIGH_RED = "\e[101m";
static const char *STYLE_BACKGROUNDHIGH_GREEN = "\e[102m";
static const char *STYLE_BACKGROUNDHIGH_YELLOW = "\e[103m";
static const char *STYLE_BACKGROUNDHIGH_BLUE = "\e[104m";
static const char *STYLE_BACKGROUNDHIGH_PURPLE = "\e[105m";
static const char *STYLE_BACKGROUNDHIGH_CYAN = "\e[106m";
static const char *STYLE_BACKGROUNDHIGH_WHITE = "\e[107m";

struct ResponseDispatcherSta
{
};

static struct ResponseDispatcherSta *ResponseDispatcherSta_new()
{
	struct ResponseDispatcherSta *sta = chatQuest_malloc(sizeof(struct ResponseDispatcherSta));
	*sta = (struct ResponseDispatcherSta) {
	};
	return sta;
}

static void ResponseDispatcherSta_destroy(struct ResponseDispatcherSta *sta)
{
	chatQuest_free(sta);
}

static void ResponseDispatcher_destroy(struct ResponseDispatcherItf *itf)
{
	struct ResponseDispatcher *imp = (struct ResponseDispatcher *)itf->imp;

	ResponseDispatcherSta_destroy(imp->sta);
	ResponseDispatcherItf_destroy(imp->ResponseDispatcherItf);
	chatQuest_free(imp);
}

		static void ResponseDispatcher_dispatchPromptTransition(struct ResponseDispatcher *imp)
		{
			chatQuest_print("[...]");
			chatQuest_waitForIntroKeyPress();
		}

		static void ResponseDispatcher_dispatchSleepTransition(struct ResponseDispatcher *imp)
		{
			chatQuest_waitSeconds(1);
		}

	static void ResponseDispatcher_dispatchTransitionIn(struct ResponseDispatcher *imp, struct SlideItf *slide)
	{
		switch (slide->getTransitionIn(slide))
		{
			case CHATQUEST_TRANSITION_IN_NONE:
				return;
			case CHATQUEST_TRANSITION_IN_PROMPT:
				ResponseDispatcher_dispatchPromptTransition(imp);
				return;
			case CHATQUEST_TRANSITION_IN_SLEEP:
				ResponseDispatcher_dispatchSleepTransition(imp);
				return;
		}
	}

	static void ResponseDispatcher_dispatchTitle(struct ResponseDispatcher *imp, struct SlideItf *slide)
	{
		if (slide->hasTitle(slide))
		{
			chatQuest_print("\n%s%s%s\n", STYLE_BOLD_WHITE, slide->getTitleAfterHas(slide), STYLE_RESET);
		}
	}

		static char *ResponseDispatcher_stylize(struct ResponseDispatcher *imp, char *body)
		{
			chatQuest_replaceSubstringInPlace(&body, "<exit>", STYLE_REGULAR_RED, 0);
			chatQuest_replaceSubstringInPlace(&body, "</exit>", STYLE_RESET, 0);
			return body;
		}

	static void ResponseDispatcher_dispatchBody(struct ResponseDispatcher *imp, struct SlideItf *slide)
	{
		if (slide->hasBody(slide))
		{
			char *body = slide->getBodyAfterHas(slide);
			body = ResponseDispatcher_stylize(imp, body);
			chatQuest_print("\n%s\n\n", body);
		}
	}

	static void ResponseDispatcher_dispatchTransitionOut(struct ResponseDispatcher *imp, struct SlideItf *slide)
	{
		switch (slide->getTransitionOut(slide))
		{
			case CHATQUEST_TRANSITION_OUT_NONE:
				return;
			case CHATQUEST_TRANSITION_OUT_PROMPT:
				ResponseDispatcher_dispatchPromptTransition(imp);
				return;
			case CHATQUEST_TRANSITION_OUT_SLEEP:
				ResponseDispatcher_dispatchSleepTransition(imp);
				return;
		}
	}

static void ResponseDispatcher_dispatch(struct ResponseDispatcherItf *itf, struct ResponseItf *response)
{
	struct ResponseDispatcher *imp = (struct ResponseDispatcher *)itf->imp;

	if (response->hasSlides(response))
	{
		struct SlideArrayItf *slides = response->getSlidesAfterHas(response);
		struct SlideItf *slide;

		for (int i = 0; slides->hasAt(slides, i); i++)
		{
			slide = slides->getAtAfterHas(slides, i);

			ResponseDispatcher_dispatchTransitionIn(imp, slide);

			ResponseDispatcher_dispatchTitle(imp, slide);

			ResponseDispatcher_dispatchBody(imp, slide);

			ResponseDispatcher_dispatchTransitionOut(imp, slide);
		}
	}
}

struct ResponseDispatcher *ResponseDispatcher_new()
{
	struct ResponseDispatcher *imp = chatQuest_malloc(sizeof(struct ResponseDispatcher));
	*imp = (struct ResponseDispatcher) {
		.sta = ResponseDispatcherSta_new(),
		.ResponseDispatcherItf = ResponseDispatcherItf_new(
			imp,
			&ResponseDispatcher_destroy,

			&ResponseDispatcher_dispatch
		)
	};
	return imp;
}