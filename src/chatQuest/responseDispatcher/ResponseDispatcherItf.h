#ifndef CHATQUEST_RESPONSEDISPATCHERITF_HEADER
#define CHATQUEST_RESPONSEDISPATCHERITF_HEADER

#include "chatQuest/response/ResponseItf.h"

struct ResponseDispatcherItf
{
	void *imp;
	void (*destroy)(struct ResponseDispatcherItf *itf);

	void (*dispatch)(struct ResponseDispatcherItf *itf, struct ResponseItf *response);
};

struct ResponseDispatcherItf *ResponseDispatcherItf_new(
	void *imp,
	void (*destroy)(struct ResponseDispatcherItf *itf),

	void (*dispatch)(struct ResponseDispatcherItf *itf, struct ResponseItf *response)
);

void ResponseDispatcherItf_destroy(struct ResponseDispatcherItf *itf);

#endif