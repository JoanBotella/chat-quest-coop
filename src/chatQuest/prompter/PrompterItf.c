#include "chatQuest/globals/globals.h"
#include "chatQuest/prompter/PrompterItf.h"

struct PrompterItf *PrompterItf_new(
	void *imp,
	void (*destroy)(struct PrompterItf *itf),

	char* (*prompt)(struct PrompterItf *itf, char *message)
)
{
	struct PrompterItf *itf = chatQuest_malloc(sizeof(struct PrompterItf));
	*itf = (struct PrompterItf) {
		.imp = imp,
		.destroy = destroy,

		.prompt = prompt
	};
	return itf;
}

void PrompterItf_destroy(struct PrompterItf *itf)
{
	chatQuest_free(itf);
}