#ifndef CHATQUEST_PROMPTERIF_HEADER
#define CHATQUEST_PROMPTERIF_HEADER

struct PrompterItf
{
	void *imp;
	void (*destroy)(struct PrompterItf *itf);

	char* (*prompt)(struct PrompterItf *itf, char *message);
};

struct PrompterItf *PrompterItf_new(
	void *imp,
	void (*destroy)(struct PrompterItf *itf),

	char* (*prompt)(struct PrompterItf *itf, char *message)	// chatQuest_mallocs the returned char*
);

void PrompterItf_destroy(struct PrompterItf *itf);

#endif