#include "chatQuest/globals/globals.h"
#include "chatQuest/prompter/Prompter.h"
#include "chatQuest/prompter/PrompterItf.h"

#define CHATQUEST_PROMPTER_BUFFER_SIZE 1000

struct PrompterSta
{
};

static struct PrompterSta *PrompterSta_new()
{
	struct PrompterSta *sta = chatQuest_malloc(sizeof(struct PrompterSta));
	*sta = (struct PrompterSta) {
	};
	return sta;
}

static void PrompterSta_destroy(struct PrompterSta *sta)
{
	chatQuest_free(sta);
}

static void Prompter_destroy(struct PrompterItf *itf)
{
	struct Prompter *imp = (struct Prompter *)itf->imp;

	PrompterSta_destroy(imp->sta);
	PrompterItf_destroy(imp->PrompterItf);
	chatQuest_free(imp);
}

static char* Prompter_prompt(struct PrompterItf *itf, char *message)
{
	chatQuest_print(message);
	return chatQuest_readLineFromStdin(CHATQUEST_PROMPTER_BUFFER_SIZE);
}

struct Prompter *Prompter_new()
{
	struct Prompter *imp = chatQuest_malloc(sizeof(struct Prompter));
	*imp = (struct Prompter) {
		.sta = PrompterSta_new(),
		.PrompterItf = PrompterItf_new(
			imp,
			&Prompter_destroy,

			&Prompter_prompt
		)
	};
	return imp;
}