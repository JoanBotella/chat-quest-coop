#ifndef CHATQUEST_PROMPTER_HEADER
#define CHATQUEST_PROMPTER_HEADER

#include "chatQuest/prompter/PrompterItf.h"

struct PrompterSta;

struct Prompter
{
	struct PrompterSta *sta;
	struct PrompterItf *PrompterItf;
};

struct Prompter *Prompter_new();

#endif
