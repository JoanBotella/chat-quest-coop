#ifndef CHATQUEST_SLIDEARRAY_HEADER
#define CHATQUEST_SLIDEARRAY_HEADER

#include "chatQuest/slideArray/SlideArrayItf.h"
#include "chatQuest/slide/SlideItf.h"
#include "chatQuest/array/ArrayAbs.h"

struct SlideArraySta;

struct SlideArray
{
	struct SlideArraySta *sta;
	struct SlideArrayItf *SlideArrayItf;
	struct ArrayAbs *ArrayAbs;
};

struct SlideArray *SlideArray_new();

#endif
