#ifndef CHATQUEST_SLIDEARRAYITF_HEADER
#define CHATQUEST_SLIDEARRAYITF_HEADER

#include "chatQuest/globals/globals.h"
#include "chatQuest/slide/SlideItf.h"

struct SlideArrayItf
{
	void *imp;
	void (*destroy)(struct SlideArrayItf *itf);

	void (*append)(struct SlideArrayItf *itf, struct SlideItf *item);
	void (*prepend)(struct SlideArrayItf *itf, struct SlideItf *item);
	int (*getLength)(struct SlideArrayItf *itf);
	bool (*hasAt)(struct SlideArrayItf *itf, int index);
	struct SlideItf *(*getAtAfterHas)(struct SlideArrayItf *itf, int index);
	bool (*isEmpty)(struct SlideArrayItf *itf);
	struct SlideItf *(*getLastAfterIsNotEmpty)(struct SlideArrayItf *itf);
	void (*removeAtAfterHas)(struct SlideArrayItf *itf, int index);
	void (*pop)(struct SlideArrayItf *itf);
	void (*shift)(struct SlideArrayItf *itf);
};

struct SlideArrayItf *SlideArrayItf_new(
	void *imp,
	void (*destroy)(struct SlideArrayItf *itf),
	
	void (*append)(struct SlideArrayItf *itf, struct SlideItf *item),
	void (*prepend)(struct SlideArrayItf *itf, struct SlideItf *item),
	int (*getLength)(struct SlideArrayItf *itf),
	bool (*hasAt)(struct SlideArrayItf *itf, int index),
	struct SlideItf *(*getAtAfterHas)(struct SlideArrayItf *itf, int index),
	bool (*isEmpty)(struct SlideArrayItf *itf),
	struct SlideItf *(*getLastAfterIsNotEmpty)(struct SlideArrayItf *itf),
	void (*removeAtAfterHas)(struct SlideArrayItf *itf, int index),
	void (*pop)(struct SlideArrayItf *itf),
	void (*shift)(struct SlideArrayItf *itf)
);

void SlideArrayItf_destroy(struct SlideArrayItf *itf);

#endif
