#include "chatQuest/globals/globals.h"
#include "chatQuest/slideArray/SlideArrayItf.h"
#include "chatQuest/slide/SlideItf.h"

struct SlideArrayItf *SlideArrayItf_new(
	void *imp,
	void (*destroy)(struct SlideArrayItf *itf),
	
	void (*append)(struct SlideArrayItf *itf, struct SlideItf *item),
	void (*prepend)(struct SlideArrayItf *itf, struct SlideItf *item),
	int (*getLength)(struct SlideArrayItf *itf),
	bool (*hasAt)(struct SlideArrayItf *itf, int index),
	struct SlideItf *(*getAtAfterHas)(struct SlideArrayItf *itf, int index),
	bool (*isEmpty)(struct SlideArrayItf *itf),
	struct SlideItf *(*getLastAfterIsNotEmpty)(struct SlideArrayItf *itf),
	void (*removeAtAfterHas)(struct SlideArrayItf *itf, int index),
	void (*pop)(struct SlideArrayItf *itf),
	void (*shift)(struct SlideArrayItf *itf)
)
{
	struct SlideArrayItf *itf = chatQuest_malloc(sizeof(struct SlideArrayItf));
	*itf = (struct SlideArrayItf) {
		.imp = imp,
		.destroy = destroy,
		
		.append = append,
		.prepend = prepend,
		.getLength = getLength,
		.hasAt = hasAt,
		.getAtAfterHas = getAtAfterHas,
		.isEmpty = isEmpty,
		.getLastAfterIsNotEmpty = getLastAfterIsNotEmpty,
		.removeAtAfterHas = removeAtAfterHas,
		.pop = pop,
		.shift = shift
	};
	return itf;
}

void SlideArrayItf_destroy(struct SlideArrayItf *itf)
{
	chatQuest_free(itf);
}
