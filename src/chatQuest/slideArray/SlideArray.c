#include "chatQuest/globals/globals.h"
#include "chatQuest/slideArray/SlideArray.h"
#include "chatQuest/slideArray/SlideArrayItf.h"
#include "chatQuest/slide/SlideItf.h"
#include "chatQuest/array/ArrayAbs.h"

struct SlideArraySta
{
};

static struct SlideArraySta *SlideArraySta_new()
{
	struct SlideArraySta *sta = chatQuest_malloc(sizeof(struct SlideArraySta));
	*sta = (struct SlideArraySta) {
	};
	return sta;
}

static void SlideArray_destroyItem(struct ArrayAbs *abs, void *item)
{
	struct SlideItf *slide = (struct SlideItf *)item;
	slide->destroy(slide);
}

static void SlideArraySta_destroy(struct SlideArraySta *sta)
{
	chatQuest_free(sta);
}

static void SlideArray_destroy(struct SlideArrayItf *itf)
{
	struct SlideArray *imp = (struct SlideArray *)itf->imp;
	struct ArrayAbs *abs = imp->ArrayAbs;

	SlideArraySta_destroy(imp->sta);
	SlideArrayItf_destroy(imp->SlideArrayItf);
	abs->destroy(abs);
	chatQuest_free(imp);
}

static void SlideArray_append(struct SlideArrayItf *itf, struct SlideItf *item)
{
	struct ArrayAbs *abs = ((struct SlideArray *)itf->imp)->ArrayAbs;
	abs->append(abs, item);
}

static void SlideArray_prepend(struct SlideArrayItf *itf, struct SlideItf *item)
{
	struct ArrayAbs *abs = ((struct SlideArray *)itf->imp)->ArrayAbs;
	abs->prepend(abs, item);
}

static int SlideArray_getLength(struct SlideArrayItf *itf)
{
	struct ArrayAbs *abs = ((struct SlideArray *)itf->imp)->ArrayAbs;
	return abs->getLength(abs);
}

static bool SlideArray_hasAt(struct SlideArrayItf *itf, int index)
{
	struct ArrayAbs *abs = ((struct SlideArray *)itf->imp)->ArrayAbs;
	return abs->hasAt(abs, index);
}

static struct SlideItf *SlideArray_getAtAfterHas(struct SlideArrayItf *itf, int index)
{
	struct ArrayAbs *abs = ((struct SlideArray *)itf->imp)->ArrayAbs;
	return (struct SlideItf *)abs->getAtAfterHas(abs, index);
}

static bool SlideArray_isEmpty(struct SlideArrayItf *itf)
{
	struct ArrayAbs *abs = ((struct SlideArray *)itf->imp)->ArrayAbs;
	return abs->isEmpty(abs);
}

static struct SlideItf *SlideArray_getLastAfterIsNotEmpty(struct SlideArrayItf *itf)
{
	struct ArrayAbs *abs = ((struct SlideArray *)itf->imp)->ArrayAbs;
	return (struct SlideItf *)abs->getLastAfterIsNotEmpty(abs);
}

static void SlideArray_removeAtAfterHas(struct SlideArrayItf *itf, int index)
{
	struct ArrayAbs *abs = ((struct SlideArray *)itf->imp)->ArrayAbs;
	abs->removeAtAfterHas(abs, index);
}

static void SlideArray_pop(struct SlideArrayItf *itf)
{
	struct ArrayAbs *abs = ((struct SlideArray *)itf->imp)->ArrayAbs;
	abs->pop(abs);
}

static void SlideArray_shift(struct SlideArrayItf *itf)
{
	struct ArrayAbs *abs = ((struct SlideArray *)itf->imp)->ArrayAbs;
	abs->shift(abs);
}

struct SlideArray *SlideArray_new()
{
	struct SlideArray *imp = chatQuest_malloc(sizeof(struct SlideArray));
	*imp = (struct SlideArray) {
		.sta = SlideArraySta_new(
		),
		.SlideArrayItf = SlideArrayItf_new(
			imp,
			&SlideArray_destroy,

			&SlideArray_append,
			&SlideArray_prepend,
			&SlideArray_getLength,
			&SlideArray_hasAt,
			&SlideArray_getAtAfterHas,
			&SlideArray_isEmpty,
			&SlideArray_getLastAfterIsNotEmpty,
			&SlideArray_removeAtAfterHas,
			&SlideArray_pop,
			&SlideArray_shift
		),
		.ArrayAbs = ArrayAbs_new(
			imp,
			&SlideArray_destroyItem
		)
	};
	return imp;
}
