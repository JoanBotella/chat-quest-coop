#include "chatQuest/globals/globals.h"
#include "chatQuest/app/AppItf.h"

struct AppItf *AppItf_new(
	void *imp,
	void (*destroy)(struct AppItf *itf),
	void (*run)(struct AppItf *itf)
)
{
	struct AppItf *itf = chatQuest_malloc(sizeof(struct AppItf));
	*itf = (struct AppItf) {
		.imp = imp,
		.destroy = destroy,

		.run = run
	};
	return itf;
}

void AppItf_destroy(struct AppItf *itf)
{
	chatQuest_free(itf);
}