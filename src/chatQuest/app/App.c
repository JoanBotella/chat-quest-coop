#include "chatQuest/globals/globals.h"
#include "chatQuest/app/App.h"
#include "chatQuest/app/AppItf.h"
#include "chatQuest/requestBuilder/RequestBuilderItf.h"
#include "chatQuest/request/RequestItf.h"
#include "chatQuest/status/StatusItf.h"
#include "chatQuest/response/ResponseItf.h"
#include "chatQuest/response/ResponseAction.h"
#include "chatQuest/stateMachine/StateMachineItf.h"
#include "chatQuest/responseDispatcher/ResponseDispatcherItf.h"
#include "chatQuest/errorDispatcher/ErrorDispatcherItf.h"
#include "chatQuest/app/AppError.h"
#include "chatQuest/error/Error.h"
#include "chatQuest/error/ErrorItf.h"

struct AppSta
{
	struct RequestBuilderItf *requestBuilder;
	struct StateMachineItf *stateMachine;
	struct ResponseDispatcherItf *responseDispatcher;
	struct ErrorDispatcherItf *errorDispatcher;

	struct RequestItf *request;
	struct ResponseItf *response;
};

static struct AppSta *AppSta_new(
	struct RequestBuilderItf *requestBuilder,
	struct StateMachineItf *stateMachine,
	struct ResponseDispatcherItf *responseDispatcher,
	struct ErrorDispatcherItf *errorDispatcher
)
{
	struct AppSta *sta = chatQuest_malloc(sizeof(struct AppSta));
	*sta = (struct AppSta) {
		.requestBuilder = requestBuilder,
		.stateMachine = stateMachine,
		.responseDispatcher = responseDispatcher,
		.errorDispatcher = errorDispatcher,

		.request = NULL,
		.response = NULL
	};
	return sta;
}

static void AppSta_destroy(struct AppSta *sta)
{
	sta->requestBuilder->destroy(sta->requestBuilder);
	sta->stateMachine->destroy(sta->stateMachine);
	sta->responseDispatcher->destroy(sta->responseDispatcher);
	sta->errorDispatcher->destroy(sta->errorDispatcher);

	// Do not destroy Request here
	// Do not destroy Response here

	chatQuest_free(sta);
}

static void App_destroy(struct AppItf *itf)
{
	struct App *imp = (struct App *)itf->imp;

	AppSta_destroy(imp->sta);
	AppItf_destroy(imp->AppItf);
	chatQuest_free(imp);
}

	static void App_setupRequest(struct App *imp)
	{
		struct AppSta *sta = imp->sta;

		if (sta->response == NULL)
		{
			sta->request = sta->requestBuilder->buildFirstRequest(sta->requestBuilder);
			return;
		}

		sta->request = sta->requestBuilder->buildByLastResponse(sta->requestBuilder, sta->response);
	}

	static void App_tryToSetupResponse(struct App *imp)
	{
		struct AppSta *sta = imp->sta;
		struct StateMachineItf *stateMachine = sta->stateMachine;
		stateMachine->run(stateMachine, sta->request);
		sta->response =
			stateMachine->hasResponse(stateMachine)
				? stateMachine->ownResponseAfterHas(stateMachine)
				: NULL
		;
	}

	static void App_dispatchResponse(struct App *imp)
	{
		struct AppSta *sta = imp->sta;
		sta->responseDispatcher->dispatch(sta->responseDispatcher, sta->response);
	}

	static bool App_mustKeepRunning(struct App *imp)
	{
		struct AppSta *sta = imp->sta;
		return sta->response->getNextAction(sta->response) != CHATQUEST_RESPONSEACTION_END;
	}

static void App_run(struct AppItf *itf)
{
	struct App *imp = (struct App *)itf->imp;
	struct AppSta *sta = imp->sta;

	do
	{
		App_setupRequest(imp);

		if (sta->request == NULL)
		{
			struct ErrorItf *error = Error_new(
				CHATQUEST_APPERROR_REQUEST_NOT_BUILT,
				__FILE__,
				__LINE__,
				"The Request could not be built."
			)->ErrorItf;
			sta->errorDispatcher->dispatchAndDestroy(sta->errorDispatcher, error);
			break;
		}
		if (sta->response != NULL)
		{
			sta->response->destroy(sta->response);
			sta->response = NULL;
		}

		App_tryToSetupResponse(imp);

		if (sta->response == NULL)
		{
			struct ErrorItf *error = Error_new(
				CHATQUEST_APPERROR_RESPONSE_NOT_BUILT,
				__FILE__,
				__LINE__,
				"The Response could not be built."
			)->ErrorItf;
			sta->errorDispatcher->dispatchAndDestroy(sta->errorDispatcher, error);
			break;
		}

		sta->request->destroy(sta->request);

		App_dispatchResponse(imp);
	}
	while (App_mustKeepRunning(imp));

	if (sta->response != NULL)
	{
		sta->response->destroy(sta->response);
	}
}

struct App *App_new(
	struct RequestBuilderItf *requestBuilder,
	struct StateMachineItf *stateMachine,
	struct ResponseDispatcherItf *responseDispatcher,
	struct ErrorDispatcherItf *errorDispatcher
)
{
	struct App *imp = chatQuest_malloc(sizeof(struct App));
	*imp = (struct App) {
		.sta = AppSta_new(
			requestBuilder,
			stateMachine,
			responseDispatcher,
			errorDispatcher
		),
		.AppItf = AppItf_new(
			imp,
			&App_destroy,

			&App_run
		)
	};
	return imp;
}