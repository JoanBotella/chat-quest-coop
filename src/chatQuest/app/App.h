#ifndef CHATQUEST_APP_HEADER
#define CHATQUEST_APP_HEADER

#include "chatQuest/app/AppItf.h"
#include "chatQuest/requestBuilder/RequestBuilderItf.h"
#include "chatQuest/stateMachine/StateMachineItf.h"
#include "chatQuest/responseDispatcher/ResponseDispatcherItf.h"
#include "chatQuest/errorDispatcher/ErrorDispatcherItf.h"

struct AppSta;

struct App
{
	struct AppSta *sta;
	struct AppItf *AppItf;
};

struct App *App_new(
	struct RequestBuilderItf *requestBuilder,
	struct StateMachineItf *stateMachine,
	struct ResponseDispatcherItf *responseDispatcher,
	struct ErrorDispatcherItf *errorDispatcher
);

#endif