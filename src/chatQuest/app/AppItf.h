#ifndef CHATQUEST_APPITF_HEADER
#define CHATQUEST_APPITF_HEADER

struct AppItf
{
	void *imp;
	void (*destroy)(struct AppItf *itf);

	void (*run)(struct AppItf *itf);
};

struct AppItf *AppItf_new(
	void *imp,
	void (*destroy)(struct AppItf *itf),

	void (*run)(struct AppItf *itf)
);

void AppItf_destroy(struct AppItf *itf);

#endif