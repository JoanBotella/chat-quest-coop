#include "chatQuest/globals/globals.h"
#include "chatQuest/stringToStringMap/StringToStringMapItf.h"

struct StringToStringMapItf *StringToStringMapItf_new(
	void *imp,
	void (*destroy)(struct StringToStringMapItf *itf),

	bool (*has)(struct StringToStringMapItf *itf, char *key),
	char *(*getAfterHas)(struct StringToStringMapItf *itf, char *key),
	void (*set)(struct StringToStringMapItf *itf, char *key, char *value),
	void (*unset)(struct StringToStringMapItf *itf, char *key)
)
{
	struct StringToStringMapItf *itf = chatQuest_malloc(sizeof(struct StringToStringMapItf));
	*itf = (struct StringToStringMapItf) {
		.imp = imp,
		.destroy = destroy,

		.has = has,
		.getAfterHas = getAfterHas,
		.set = set,
		.unset = unset
	};
	return itf;
}

void StringToStringMapItf_destroy(struct StringToStringMapItf *itf)
{
	chatQuest_free(itf);
}