#ifndef CHATQUEST_STRINGTOSTRINGMAPITF_HEADER
#define CHATQUEST_STRINGTOSTRINGMAPITF_HEADER

struct StringToStringMapItf
{
	void *imp;
	void (*destroy)(struct StringToStringMapItf *itf);

	bool (*has)(struct StringToStringMapItf *itf, char *key);
	char *(*getAfterHas)(struct StringToStringMapItf *itf, char *key);
	void (*set)(struct StringToStringMapItf *itf, char *key, char *value);
	void (*unset)(struct StringToStringMapItf *itf, char *key);
};

struct StringToStringMapItf *StringToStringMapItf_new(
	void *imp,
	void (*destroy)(struct StringToStringMapItf *itf),

	bool (*has)(struct StringToStringMapItf *itf, char *key),
	char *(*getAfterHas)(struct StringToStringMapItf *itf, char *key),
	void (*set)(struct StringToStringMapItf *itf, char *key, char *value),
	void (*unset)(struct StringToStringMapItf *itf, char *key)
);

void StringToStringMapItf_destroy(struct StringToStringMapItf *itf);

#endif