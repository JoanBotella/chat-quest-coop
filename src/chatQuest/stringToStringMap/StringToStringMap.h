#ifndef CHATQUEST_STRINGTOSTRINGMAP_HEADER
#define CHATQUEST_STRINGTOSTRINGMAP_HEADER

#include "chatQuest/stringToStringMap/StringToStringMapItf.h"
#include "chatQuest/stringToMap/StringToMapAbs.h"

struct StringToStringMapSta;

struct StringToStringMap
{
	struct StringToStringMapSta *sta;
	struct StringToStringMapItf *StringToStringMapItf;
	struct StringToMapAbs *StringToMapAbs;
};

struct StringToStringMap *StringToStringMap_new();

#endif
