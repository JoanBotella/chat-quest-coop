#include "chatQuest/globals/globals.h"
#include "chatQuest/stringToStringMap/StringToStringMap.h"
#include "chatQuest/stringToStringMap/StringToStringMapItf.h"
#include "chatQuest/stringToMap/StringToMapAbs.h"

struct StringToStringMapSta
{
};

static struct StringToStringMapSta *StringToStringMapSta_new()
{
	struct StringToStringMapSta *sta = chatQuest_malloc(sizeof(struct StringToStringMapSta));
	*sta = (struct StringToStringMapSta) {
	};
	return sta;
}

static void StringToStringMapSta_destroy(struct StringToStringMapSta *sta)
{
	chatQuest_free(sta);
}

static void StringToStringMap_destroyItem(struct StringToMapAbs *abs, void *key, void *value)
{
	char *string = (char *)key;
	chatQuest_free(string);

	string = (char *)value;
	chatQuest_free(string);
}

static void StringToStringMap_destroy(struct StringToStringMapItf *itf)
{
	struct StringToStringMap *imp = (struct StringToStringMap *)itf->imp;
	struct StringToMapAbs *abs = imp->StringToMapAbs;

	StringToStringMapSta_destroy(imp->sta);
	StringToStringMapItf_destroy(imp->StringToStringMapItf);
	abs->destroy(abs);
	chatQuest_free(imp);
}

static bool StringToStringMap_has(struct StringToStringMapItf *itf, char *key)
{
	struct StringToMapAbs *abs = ((struct StringToStringMap *)itf->imp)->StringToMapAbs;
	return abs->has(abs, key);
}

static char *StringToStringMap_getAfterHas(struct StringToStringMapItf *itf, char *key)
{
	struct StringToMapAbs *abs = ((struct StringToStringMap *)itf->imp)->StringToMapAbs;
	return (char *)abs->getAfterHas(abs, key);
}

static void StringToStringMap_set(struct StringToStringMapItf *itf, char *key, char *value)
{
	struct StringToMapAbs *abs = ((struct StringToStringMap *)itf->imp)->StringToMapAbs;
	abs->set(abs, chatQuest_copyString(key), chatQuest_copyString(value));
}

static void StringToStringMap_unset(struct StringToStringMapItf *itf, char *key)
{
	struct StringToMapAbs *abs = ((struct StringToStringMap *)itf->imp)->StringToMapAbs;
	abs->unset(abs, key);
}

struct StringToStringMap *StringToStringMap_new()
{
	struct StringToStringMap *imp = chatQuest_malloc(sizeof(struct StringToStringMap));
	*imp = (struct StringToStringMap) {
		.sta = StringToStringMapSta_new(
		),
		.StringToStringMapItf = StringToStringMapItf_new(
			imp,
			&StringToStringMap_destroy,

			&StringToStringMap_has,
			&StringToStringMap_getAfterHas,
			&StringToStringMap_set,
			&StringToStringMap_unset
		),
		.StringToMapAbs = StringToMapAbs_new(
			imp,
			&StringToStringMap_destroyItem
		)
	};
	return imp;
}
