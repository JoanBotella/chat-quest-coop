#ifndef CHATQUEST_STATUS_HEADER
#define CHATQUEST_STATUS_HEADER

#include "chatQuest/status/StatusItf.h"
#include "chatQuest/stringArray/StringArrayItf.h"
#include "chatQuest/stringToStringMap/StringToStringMapItf.h"

struct StatusSta;

struct Status
{
	struct StatusSta *sta;
	struct StatusItf *StatusItf;
};

struct Status *Status_new(
	struct StringArrayItf *nodeIds,
	struct StringToStringMapItf *variables
);

#endif
