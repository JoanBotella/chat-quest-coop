#ifndef CHATQUEST_STATUSITF_HEADER
#define CHATQUEST_STATUSITF_HEADER

#include "chatQuest/stringArray/StringArrayItf.h"
#include "chatQuest/stringToStringMap/StringToStringMapItf.h"

struct StatusItf
{
	void *imp;
	void (*destroy)(struct StatusItf *itf);

	struct StringArrayItf *(*getNodeIds)(struct StatusItf *itf);
	void (*setNodeIds)(struct StatusItf *itf, struct StringArrayItf *nodeIds);

	struct StringToStringMapItf *(*getVariables)(struct StatusItf *itf);
	void (*setVariables)(struct StatusItf *itf, struct StringToStringMapItf *variables);
};

struct StatusItf *StatusItf_new(
	void *imp,
	void (*destroy)(struct StatusItf *itf),

	struct StringArrayItf *(*getNodeIds)(struct StatusItf *itf),
	void (*setNodeIds)(struct StatusItf *itf, struct StringArrayItf *nodeIds),

	struct StringToStringMapItf *(*getVariables)(struct StatusItf *itf),
	void (*setVariables)(struct StatusItf *itf, struct StringToStringMapItf *variables)
);

void StatusItf_destroy(struct StatusItf *itf);

#endif