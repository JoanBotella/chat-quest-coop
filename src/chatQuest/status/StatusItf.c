#include "chatQuest/globals/globals.h"
#include "chatQuest/status/StatusItf.h"
#include "chatQuest/stringArray/StringArrayItf.h"
#include "chatQuest/stringToStringMap/StringToStringMapItf.h"

struct StatusItf *StatusItf_new(
	void *imp,
	void (*destroy)(struct StatusItf *itf),

	struct StringArrayItf *(*getNodeIds)(struct StatusItf *itf),
	void (*setNodeIds)(struct StatusItf *itf, struct StringArrayItf *nodeIds),

	struct StringToStringMapItf *(*getVariables)(struct StatusItf *itf),
	void (*setVariables)(struct StatusItf *itf, struct StringToStringMapItf *variables)
)
{
	struct StatusItf *itf = chatQuest_malloc(sizeof(struct StatusItf));
	*itf = (struct StatusItf) {
		.imp = imp,
		.destroy = destroy,

		.getNodeIds = getNodeIds,
		.setNodeIds = setNodeIds,

		.getVariables = getVariables,
		.setVariables = setVariables
	};
	return itf;
}

void StatusItf_destroy(struct StatusItf *itf)
{
	chatQuest_free(itf);
}