#include "chatQuest/globals/globals.h"
#include "chatQuest/status/Status.h"
#include "chatQuest/status/StatusItf.h"
#include "chatQuest/stringArray/StringArrayItf.h"
#include "chatQuest/stringToStringMap/StringToStringMapItf.h"

struct StatusSta
{
	struct StringArrayItf *nodeIds;
	struct StringToStringMapItf *variables;
};

static struct StatusSta *StatusSta_new(
	struct StringArrayItf *nodeIds,
	struct StringToStringMapItf *variables
)
{
	struct StatusSta *sta = chatQuest_malloc(sizeof(struct StatusSta));
	*sta = (struct StatusSta) {
		.nodeIds = nodeIds,
		.variables = variables
	};
	return sta;
}

static void StatusSta_destroy(struct StatusSta *sta)
{
	sta->nodeIds->destroy(sta->nodeIds);
	chatQuest_free(sta);
}

void Status_destroy(struct StatusItf *itf)
{
	struct Status *imp = (struct Status *)itf->imp;

	StatusSta_destroy(imp->sta);
	StatusItf_destroy(imp->StatusItf);
	chatQuest_free(imp);
}

static struct StringArrayItf *Status_getNodeIds(struct StatusItf *itf)
{
	struct StatusSta *sta = ((struct Status *)itf->imp)->sta;
	return sta->nodeIds;
}

static void Status_setNodeIds(struct StatusItf *itf, struct StringArrayItf *nodeIds)
{
	struct StatusSta *sta = ((struct Status *)itf->imp)->sta;
	sta->nodeIds->destroy(sta->nodeIds);
	sta->nodeIds = nodeIds;
}

static struct StringToStringMapItf *Status_getVariables(struct StatusItf *itf)
{
	struct StatusSta *sta = ((struct Status *)itf->imp)->sta;
	return sta->variables;
}

static void Status_setVariables(struct StatusItf *itf, struct StringToStringMapItf *variables)
{
	struct StatusSta *sta = ((struct Status *)itf->imp)->sta;
	sta->variables->destroy(sta->variables);
	sta->variables = variables;
}

struct Status *Status_new(
	struct StringArrayItf *nodeIds,
	struct StringToStringMapItf *variables
)
{
	struct Status *imp = chatQuest_malloc(sizeof(struct Status));
	*imp = (struct Status) {
		.sta = StatusSta_new(
			nodeIds,
			variables
		),
		.StatusItf = StatusItf_new(
			imp,
			&Status_destroy,

			&Status_getNodeIds,
			&Status_setNodeIds,

			&Status_getVariables,
			&Status_setVariables
		)
	};
	return imp;
}