#ifndef CHATQUEST_GLOBALS_HEADER
#define CHATQUEST_GLOBALS_HEADER

#include <stdlib.h>
#include <stdbool.h>

void *chatQuest_malloc(size_t size);

void chatQuest_free(void *pointer);

void chatQuest_print(const char *format, ...);

void chatQuest_printError(const char *format, ...);

char *chatQuest_buildString(const char *format, ...);

char *chatQuest_copyString(const char *string);

char *chatQuest_copyStringBySize(const char *string, int size);

int chatQuest_calculateStringSize(const char *string);

void chatQuest_stringToLowerCaseInPlace(char **stringPointerAddress);

bool chatQuest_areStringsEqual(const char *a, const char* b);

void chatQuest_waitSeconds(int seconds);

void chatQuest_waitForIntroKeyPress();

char *chatQuest_readLineFromStdin(int bufferSize);

int chatQuest_replaceSubstringInPlace(char **stringPointerAddress, const char *find, const char *replace, int limit);

char **chatQuest_splitString(const char *string, const char *delimiter, int maxTokens);

void chatQuest_freeStringArray(char **stringArray);

void chatQuest_stringRightTrimInPlace(char **stringPointerAddress);

void chatQuest_stringLeftTrimInPlace(char **stringPointerAddress);

char *chatQuest_serializeInt(int value);

int chatQuest_unserializeInt(char *value);

char *chatQuest_serializeBool(bool value);

bool chatQuest_unserializeBool(char *value);

char *chatQuest_serializeFloat(float value);

float chatQuest_unserializeFloat(char *value);

#endif
