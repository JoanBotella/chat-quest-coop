#include "chatQuest/globals/globals.h"
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <glib.h>

void *chatQuest_malloc(size_t size)
{
	void *pointer = malloc(size);
	if (pointer == NULL)
	{
		exit(EXIT_FAILURE);
	}
	return pointer;
}

void chatQuest_free(void *pointer)
{
	free(pointer);
}

void chatQuest_print(const char *format, ...)
{
	va_list args;
	va_start(args, format);
	vprintf(format, args);
	va_end(args);
}

void chatQuest_printError(const char *format, ...)
{
	va_list args;
	va_start(args, format);
	vfprintf(stderr, format, args);
	va_end(args);
}

char *chatQuest_buildString(const char *format, ...)
{
	va_list args;
	va_start(args, format);
	char *string = g_strdup_vprintf(format, args);
	va_end(args);
	return string;
}

char *chatQuest_copyString(const char *string)
{
	int size = chatQuest_calculateStringSize(string);
	return chatQuest_copyStringBySize(string, size);
}

char *chatQuest_copyStringBySize(const char *string, int size)
{
	char *copy = chatQuest_malloc(size + 1);
	for (int index = 0; index < size; index++)
	{
		copy[index] = string[index];
	}
	copy[size] = '\0';
	return copy;
}

int chatQuest_calculateStringSize(const char *string)
{
	int index;
	for(index = 0; string[index] != '\0'; index++);
	return index;
}

void chatQuest_stringToLowerCaseInPlace(char **stringPointerAddress)
{
	char *string = *stringPointerAddress;
	for(int index = 0; string[index] != '\0'; index++){
		string[index] = tolower(string[index]);
	}
}

bool chatQuest_areStringsEqual(const char *a, const char* b)
{
	int index = 0;
	do
	{
		if (a[index] != b[index])
		{
			return false;
		}
		if (a[index] == '\0')
		{
			break;
		}
		index++;
	}
	while (true);
	return true;
}

void chatQuest_waitSeconds(int seconds)
{
	sleep(seconds);
}

void chatQuest_waitForIntroKeyPress()
{
	getchar();
}

char *chatQuest_readLineFromStdin(int bufferSize)
{
	char *buffer = chatQuest_malloc(bufferSize);
	fgets(buffer, bufferSize, stdin);

	int lineSize = chatQuest_calculateStringSize(buffer) - 1;
	char *line = chatQuest_copyStringBySize(buffer, lineSize);

	chatQuest_free(buffer);

	return line;
}

int chatQuest_replaceSubstringInPlace(char **stringPointerAddress, const char *find, const char *replace, int limit)
{
	GString *gstring = g_string_new(*stringPointerAddress);
	int r = g_string_replace(
		gstring,
		find,
		replace,
		limit
	);
	*stringPointerAddress = gstring->str;
	return r;
}

char **chatQuest_splitString(const char *string, const char *delimiter, int maxTokens)
{
	return g_strsplit (string, delimiter, maxTokens);
}

void chatQuest_freeStringArray(char **stringArray)
{
	g_strfreev(stringArray);
}

void chatQuest_stringRightTrimInPlace(char **stringPointerAddress)
{
	g_strchomp(*stringPointerAddress);
}

void chatQuest_stringLeftTrimInPlace(char **stringPointerAddress)
{
	g_strchug(*stringPointerAddress);
}

char *chatQuest_serializeInt(int value)
{
	return g_strdup_printf("%d", value);
}

int chatQuest_unserializeInt(char *value)
{
	return g_ascii_strtoll(value, NULL, 10);
}

char *chatQuest_serializeBool(bool value)
{
	char *string =
		value
			? "true"
			: "false"
	;
	return chatQuest_copyString(string);
}

bool chatQuest_unserializeBool(char *value)
{
	if (chatQuest_areStringsEqual(value, "true"))
	{
		return true;
	}
	return false;
}

char *chatQuest_serializeFloat(float value)
{
	return g_strdup_printf("%f", value);
}

float chatQuest_unserializeFloat(char *value)
{
	return g_ascii_strtod(value, NULL);
}
