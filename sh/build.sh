#! /bin/bash

if [ $# -eq 0 ]
then
	ENVIRONMENT=dev
else
	ENVIRONMENT=$1
fi

ENVIRONMENT_FILE="env/_$ENVIRONMENT.sh"

if [ ! -f "$ENVIRONMENT_FILE" ]
then
	echo "Environment file $ENVIRONMENT_FILE does not exist."
	exit 3
fi

echo "Building for $ENVIRONMENT environment."
source "$ENVIRONMENT_FILE"



echo 'Cleaning building directories'

mkdir -p "$OBJ_DIR" > /dev/null
mkdir -p "$DST_DIR" > /dev/null



if [ "$DEPENDENCIES" != '' ]
then
	DEPENDENCIES_CFLAGS=`pkg-config --cflags $DEPENDENCIES`
	DEPENDENCIES_LIBS=`pkg-config --libs $DEPENDENCIES`
else
	DEPENDENCIES_CFLAGS=''
	DEPENDENCIES_LIBS=''
fi



cd "$OBJ_DIR"

OBJECTS=()

for FILE in `find "$SRC_DIR" -name *.c`
do
	if [ "$FILE" != "$SRC_MAIN_FILE" ]
	then
		echo "Compiling $FILE"
		gcc $COMPILING_EXTRA_PARAMS $DEPENDENCIES_CFLAGS -iquote "$SRC_DIR" -c "$FILE" $DEPENDENCIES_LIBS

		if [ $? -gt 0 ]
		then
			echo 'COMPILING ERROR.'
			exit 1
		fi

		OBJECTS+=("$OBJ_DIR/`basename $FILE .c`.o")
	fi
done

cd - > /dev/null



echo 'Linking'

gcc $LINKING_EXTRA_PARAMS $DEPENDENCIES_CFLAGS -iquote "$SRC_DIR" -o "$DST_MAIN_FILE" "$SRC_MAIN_FILE" "${OBJECTS[@]}" $DEPENDENCIES_LIBS



if [ $? -gt 0 ]
then
	echo 'LINKING ERROR.'
	exit 2
fi

DST_MAIN_FILE_SIZE=`du -h "$DST_MAIN_FILE" | awk '{print $1}'`
echo "SUCCESS ($DST_MAIN_FILE_SIZE)"